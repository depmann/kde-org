---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE에서 KDE 프로그램 18.04.3 출시
layout: application
title: KDE에서 KDE 프로그램 18.04.3 출시
version: 18.04.3
---
July 12, 2018. Today KDE released the third stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Ark, Cantor, Dolphin, Gwenview, KMag 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- 기능 정보를 제공하지 않는 IMAP 서버와의 호환성이 복원됨
- Ark에서 올바른 폴더 항목이 없는 ZIP 압축 파일을 풀 수 있음
- KNotes에서 화면상 메모를 움직일 때 마우스 포인터를 따라감
