---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE, KDE Uygulamaları 17.12.2'yi Gönderdi
layout: application
title: KDE, KDE Uygulamaları 17.12.2'yi Gönderdi
version: 17.12.2
---
February 8, 2018. Today KDE released the second stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen yaklaşık 20 hata düzeltmesi, diğerleri arasında Kontak, Dolphin, Gwenview, K İndir, Okular için iyileştirmeler içerir.
