---
aliases:
- ../announce-applications-15.08-beta
date: 2015-07-28
description: O KDE Lança as Aplicações do KDE 15.08 Beta.
layout: application
release: applications-15.07.80
title: O KDE Lança a Primeira Versão 15.08 Beta das Aplicações
---
28 de Julho de 2015. Hoje o KDE lançou a primeira das versões beta das novas Aplicações do KDE. Com as dependências e as funcionalidades estabilizadas, o foco da equipa do KDE é agora a correcção de erros e mais algumas rectificações.

Com as diversas aplicações a basearem-se nas Plataformas do KDE 5, as versões 15.08 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do utilizador. Os utilizadores actuais são críticos para manter a alta qualidade do KDE, dado que os programadores não podem simplesmente testar todas as configurações possíveis. Contamos consigo para nos ajudar a encontrar erros antecipadamente, para que possam ser rectificados antes da versão final. Por favor, pense em juntar-se à equipa, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
