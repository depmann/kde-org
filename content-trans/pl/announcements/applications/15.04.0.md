---
aliases:
- ../announce-applications-15.04.0
changelog: true
date: '2015-04-15'
description: KDE wydało Aplikacje 15.04.
layout: application
title: KDE wydało Aplikacje KDE 15.04.0
version: 15.04.0
---
15 kwietnia 2015. Dzisiaj KDE wydało Aplikacje KDE 15.04. Z tym wydaniem, ponad 72 aplikacje zostały przeniesione na <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Szkielety KDE 5</a>. Zespół stara się zapewniać najlepszą jakość na twoim pulpicie dla tych aplikacji, więc liczymy na twoją informację zwrotną.

W tym wydaniu dodano kilka nowych aplikacji opartych na Szkieletach KDE 5, takich jak <a href='https://www.kde.org/applications/education/khangman/'>KWisielec</a>, <a href='https://www.kde.org/applications/education/rocs/'>Rocs</a>, <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, <a href='https://www.kde.org/applications/development/kompare'>Kompare</a>, <a href='https://kdenlive.org/'>Kdenlive</a>, <a href='https://userbase.kde.org/Telepathy'>KDE Telepathy</a> oraz <a href='https://games.kde.org/'>kilka gier KDE</a>.

Kdenlive jest jednym z najlepszych dostępnych nie-liniowych edytorów wideo. Ostatnio zakończył swój <a href='https://community.kde.org/Incubator'>okres inkubacyjny</a>, aby stać się oficjalnym projektem KDE i został przeniesiony na Szkielety KDE 5. Zespół odpowiedzialny za to dzieło postanowił, że Kdenlive ma być wydany razem z Aplikacjami KDE. Nowymi funkcjami jest samozapisywanie nowych projektów i stabilizacja stałych wycinków.

KDE Telepathy jest narzędziem do komunikowania się. Zostało przeniesione na Szkielety KDE 5 oraz Qt5 i stało się częścią wydań Aplikacji KDE. Program został właściwie ukończony, lecz nadal brakuje w nim interfejsów do obsługi rozmów głosowych i wideo.

Gdzie to możliwe, KDE używa istniejącej technologi, tak jak zostało to zrobione z modułem Konta, który jest także używany w SailfishOS oraz Unity Canonicala. Wewnątrz Aplikacji KDE, jest on używany obecnie tylko przez KDE Telepathy. W przyszłości planowane jest jego szersze użycie m.in. przez Kontact oraz Akonadi.

W <a href='https://edu.kde.org/'>module edukacyjnym KDE</a>, Cantor zyskał kilka nowych możliwości dotyczących obsługi Pythona, tj. nowy silnik Python 3 oraz nowe kategorie Get Hot New Stuff. Poprzewracaliśmy Rocs: jądro teorii grafów zostało przepisane, podział struktury danych został usunięty, wprowadzono bardziej ogólny dokument grafów jako centralna jednostka grafów, przejrzano także API skryptów dla algorytmów grafów, które teraz dostarcza tylko jedno jednolite API. KHangMan został przeniesiony na QtQuick, a zarazem przemalowany na świeżo. Kanagram dostał nowy tryb gry na  2 graczy, a w litery  od teraz można klikać lub wpisywać tak jak poprzednio.

Poza zwykłym poprawkami błędów <a href='https://www.kde.org/applications/development/umbrello/'>Umbrello</a> jest także bardziej stabilny i użyteczny. Dodatkowo w funkcji znajdywania można teraz ograniczyć do kategorii: klasa, interfejs, pakiet, operacje lub atrybuty.
