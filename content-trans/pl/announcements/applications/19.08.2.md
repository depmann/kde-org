---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE wydało Aplikacje 19.08.2.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE wydało Aplikacje 19.08.2
version: 19.08.2
---
{{% i18n_date %}}

Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../19.08.0'>Aplikacji KDE 19.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize, Spectacle, among others.

Wśród ulepszeń znajdują się:

- High-DPI support got improved in Konsole and other applications
- Switching between different searches in Dolphin now correctly updates search parameters
- KMail can again save messages directly to remote folders
