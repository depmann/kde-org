---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### In vele frameworks

- Hernoem privé klassen om ze per ongeluk te exporteren te vermijden

### Baloo

- org.kde.baloo-interface toevoegen aan root-object voor achterwaartse compatibiliteit
- Installeer een fake org.kde.baloo.file.indexer.xml om compilatie van plasma-desktop 5.4 te repareren
- Reorganiseer D-Bus-interfaces
- json metadata in kded plug-in gebruiken en plug-in-naam repareren
- Eén database exemplaar per proces aanmaken (bug 350247)
- Voorkom dat baloo_file_extractor beëindigd wordt tijdens een commit
- Xml-interface-bestand genereren met qt5_generate_dbus_interface
- Reparaties aan Baloo-monitor
- Exporteren van URL van bestand verplaatsen naar de hoofd-thread
- Zorg er voor dat met gecascadeerde configuraties rekening wordt gehouden
- Geen namelink voor privé bibliotheek installeren
- Vertalingen installeren, gezien door Hrvoje Senjan.

### BluezQt

- Stuur signaal deviceChanged niet door nadat het apparaat was verwijderd (bug 351051)
- Houd rekening met -DBUILD_TESTING=OFF

### Extra CMake-modules

- Voeg macro toe om logging-categorie-declaraties voor Qt5 te genereren.
- ecm_generate_headers: voeg optie COMMON_HEADER en multiple-header-functionaliteit toe
- Voeg -pedantic voor KF5 code toe (bij gebruik van gcc of clang)
- KDEFrameworkCompilerSettings: schakel alleen "strict iterators" in in debugmodus
- Stel ook de standaard zichtbaarheid voor C-code in op verborgen.

### Frameworkintegratie

- Propageer venstertitels ook voor bestandsdialogen met alleen mappen.

### KActivities

- Spawn slechts één actielader (thread) wanneer de acties van de FileItemLinkingPlugin niet zijn geïnitialiseerd (bug 351585)
- De bouwproblemen repareren geïntroduceerd door hernoemen van de "Private classes" (11030ffc0)
- Voeg ontbrekend include path voor boost toe om op OS X te kunnen bouwen
- Instellen van de sneltoetsen verplaatst naar instellingen voor activiteiten
- Instellen dat de modus privé activiteit werkt
- Opnieuw maken van de instellingen-UI
- Basis activiteitsmethodes zijn functioneel
- UI voor de activiteitennconfiguratie en pop-ups voor verwijderen
- Basis-UI voor de activiteitensectie voor aanmaken/verwijderen/instellen in KCM
- Verhoogt de chunk-grootte voor laden van de resultaten
- Ontbrekende include voor std::set toegevoegd

### KDE Doxygen hulpmiddelen

- Windows reparatie: verwijder bestaande bestanden alvorens we ze vervangen door os.rename.
- Gebruik eigen paden bij aanroepen van python om bouwen van Windows te repareren

### KCompletion

- Repareer slecht gedrag / uitvoeren van OOM onder Windows (bug 345860)

### KConfig

- Optimaliseer readEntryGui
- QString::fromLatin1() in gegenereerde code vermijden
- Minimaliseer aanroepen naar dure QStandardPaths::locateAll()
- Voltooi het overbrengen naar QCommandLineParser (het bevat nu addPositionalArgument)

### Ondersteuning van KDELibs 4

- Breng solid-networkstatus kded-plug-in over naar json-metadata
- KPixmapCache: maak dir als deze niet bestaat

### KDocTools

- Synchroniseer Catalan user.entities met Engelse (en) versie.
- Voeg entities toe voor sebas en plasma-pa

### KEmoticons

- Prestatie: cache hier een exemplaar van KEmoticons, geen KEmoticonsTheme.

### KFileMetaData

- PlainTextExtractor: schakel O_NOATIME branch in op GNU libc-platformen
- PlainTextExtractor: maak de Linux-branch werkend ook zonder O_NOATIME
- PlainTextExtractor: repareer foutencontrole bij open(O_NOATIME) mislukking

### KGlobalAccel

- Start kglobalaccel5 alleen indien nodig.

### KI18n

- Behandel geen nieuwe-regel aan het eind van pmap-bestand netjes

### KIconThemes

- KIconLoader: repareer reconfigure() en vergeet geërfde thema's en app-mappen
- Hou beter vast aan de spec voor laden van pictogrammen

### KImageFormats

- eps: repareer includes gerelateerd aan Qt gecategoriseerde logging

### KIO

- Gebruik Q_OS_WIN in plaats van Q_OS_WINDOWS
- Laat KDE_FORK_SLAVES werken onder Windows
- Schakel installatie van desktop-bestand voor ProxyScout kded module uit
- Bied deterministische sorteervolgorde voor KDirSortFilterProxyModelPrivate::compare
- Eigen pictogrammen voor mappen opnieuw tonen (bug 350612)
- Verplaats kpasswdserver van kded naar kiod
- Repareer bugs gemaakt bij overzetten in kpasswdserver
- Verwijder verouderde code voor praten uit zeer oude versies van kpasswdserver.
- KDirListerTest: QTRY_COMPARE gebruiken in beide statements, om race getoond door CI te repareren
- KFilePlacesModel: implementeer oude TODO over gebruik van trashrc in plaats van een volledige KDirLister.

### KItemModels

- Nieuw proxymodel: KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: repareer behandeling van layoutChanged.
- Meer controle bij de selectie na sortering.
- KExtraColumnsProxyModel: repareer bug in sibling() die bijv. selecties brak

### Pakket Framework

- kpackagetool kan een pakket uit een pakketbestand deïnstalleren
- kpackagetool is nu beter in het vinden van het juiste servicetype

### KService

- KSycoca: controleer tijdstippen en voer kbuildsycoca uit indien nodig. Geen afhankelijkheid van kded nog.
- Sluit ksycoca niet direct nadat het is geopend.
- KPluginInfo behandelt nu correct FormFactor-metagegevens

### KTextEditor

- Meng toekenning van TextLineData en aantal ref-blokken.
- Wijzig standaard sneltoets voor "ga naar vorige te bewerken regel"
- Reparaties aan accentuering van syntaxis Haskell-commentaar
- Versnellen verschijnen op-up van code-aanvulling
- minimap: poging om het uiterlijk en gedrag te verbeteren (bug 309553)
- genest commentaar in syntaxisaccentuering van Haskell
- Reparatie van probleem met verkeerd inspringen ongedaan maken voor python (bug 351190)

### KWidgetsAddons

- KPasswordDialog: laat de gebruiker de zichtbaarheid van het wachtwoord wijzigen (bug 224686)

### KXMLGUI

- KSwitchLanguageDialog repareren bij niet tonen van de meeste talen

### KXmlRpcClient

- Vermijd QLatin1String overal waar het heap-geheugen toekent

### ModemManagerQt

- Repareer conflict met metatype in de laatste nm-qt wijziging

### NetworkManagerQt

- Nieuwe eigenschappen uit de laatste NM snapshot/uitgaven toegevoegd

### Plasma Framework

- reparent naar flickable indien mogelijk
- repareer lijst met pakketten
- plasma: repareer appletacties die nullptr kunnen zijn (bug 351777)
- Het signaal onClicked van PlasmaComponents.ModelContextMenu werkt nu op de juiste manier
- PlasmaComponents ModelContextMenu kan nu Menu-secties aanmaken
- Overgezet platformstatus kded plug-in naar json metagegevens...
- Behandel een ongeldig metagegeven in PluginLoader
- Laat de RowLayout de grootte van het label uitvogelen
- toon altijd het bewerkingsmenu wanneer de cursor zichtbaar is
- Repareer loop in ButtonStyle
- Wijzig de vlakheid van een button niet bij indrukken
- bij aanraakscherm en mobiel zijn schuifbalken voorbijgaand
- pas flick velocity&amp;deceleration aan aan dpi
- eigen cursordelegatie alleen indien mobiel
- aanraakvriendelijke tekstcursor
- beleid voor ouderprocessen en pop-up repareren
- declare __editMenu
- voeg ontbrekende cursor die delegaties behandeld
- herschrijf de implementatie van het EditMenu 
- gebruik het mobielmenu alleen onder voorwaarden
- reparent het menu naar root

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
