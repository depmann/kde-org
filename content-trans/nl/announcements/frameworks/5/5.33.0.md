---
aliases:
- ../../kde-frameworks-5.33.0
date: 2017-04-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Toegevoegde beschrijving voor commando's (balooctl)
- Ook in symbolisch gekoppelde mappen zoeken (bug 333678)

### BluezQt

- Lever apparaattype voor laag energie apparaten

### Extra CMake-modules

- Specificeer qml-root-path als de gedeelde map in de prefix
- ecm_generate_pkgconfig_file compatibiliteit met nieuwe cmake repareren
- Registeer alleen APPLE_* opties indien(APPLE)

### KActivitiesStats

- Toegevoegde voorinstellingen aan de toepassingen voor testen
- Items op de juiste manier naar de gewenste positie verplaatsen
- Opnieuw ordenen naar andere modelexemplaren synchroniseren
- Als de volgorde niet is gedefinieerd, sorteer de items op het id

### KDE Doxygen hulpmiddelen

- [Meta] onderhouder wijzigen in setup.py

### KAuth

- Backend voor Mac
- Ondersteuning toevoegen voor afbreken van een KAuth::ExecuteJob

### KConfig

- Afsteeklijst bij lezen/schrijven uit kdeglobals opschonen
- Onnodig opnieuw toekennen vermijden door squeeze call bij tijdelijke buffer te verwijderen

### KDBusAddons

- KDBusService: voeg toe wie toegang heeft tot de dbus-servicenaam die we registreren

### KDeclarative

- Met Qt &gt;= 5.8 de nieuwe API gebruiken om scene-graph-backend in te stellen
- acceptHoverEvents in DragArea niet instellen omdat we deze niet gebruiken

### KDocTools

- meinproc5: maak koppeling naar de bestanden, niet naar de bibliotheek (bug 377406)

### KFileMetaData

- Laat PlainTextExtractor opnieuw overeenkomen met "text/plain"

### KHTML

- Foutpagina, laad de afbeelding op de juiste manier (met een echte URL)

### KIO

- Laat remote file:// URL wijzend naar smb:// opnieuw werken
- Codering van query behouden wanneer HTTP Proxy wordt gebruikt
- Bijgewerkte gebruikersagents (Firefox 52 ESR, Chromium 57)
- Behandel/kort af getoonde tekenreeks van URL toegekend aan taakbeschrijving. voorkomt dat grote gegevens: urls ingevoegd worden in UI meldingen
- KFileWidget::setSelectedUrl() toevoegen (bug 376365)
- KUrlRequester opslagmodus repareren door setAcceptMode toe te voegen

### KItemModels

- De nieuwe QSFPM::setRecursiveFiltering(true) noemen die KRecursiveFilterProxyModel verouderd maakt

### KNotification

- Geen in de rij geplaatste meldingen verwijderen wanneer de fd.o service start
- Aanpassingen aan het Mac platform

### KParts

- API dox: ontbrekende notitie om setXMLFile aan te rpeoen met KParts::MainWindow repareren

### KService

- 'Niet gevonden: ""' berichten naar de terminal repareren

### KTextEditor

- Extra interne functionaliteit van View naar de publieke API exposeren
- Veel toekenningen voor setPen opslaan
- ConfigInterface van KTextEditor::Document repareren
- Lettertype en opties on-the-fly-spellingcontrole in ConfigInterface toevoegen

### KWayland

- Ondersteuning voor wl_shell_surface::set_popup en popup_done toevoegen

### KWidgetsAddons

- Bouwen tegen een qt zonder a11es ingeschakeld ondersteunen
- Verkeerde grootte hint wanneer animatedShow wordt aangeroepen met verborgen ouder repareren (bug 377676)
- Tekens in KCharSelectTable die worden afgekapt repareren
- Alle vlakken in de testdialoog van kcharselect inschakelen

### NetworkManagerQt

- WiredSetting: autonegotiate teruggeven zelfs wanneer het is uitgeschakeld
- Voorkomen dat signals in glib2 gedefinieerd worden door Qt
- WiredSetting: snelheid en duplex kunnen alleen ingesteld worden wanneer auto-negotiation is uitgezet (bug 376018)
- Waarde van auto-negotiate voor wired instelling zou false moeten zijn

### Plasma Framework

- [ModelContextMenu] Instantiator gebruiken in plaats van Repeater-and-reparent-hack
- [Calendar] Verklein en kap weeknamen af zoals is gedaan met dagdelegatie (bug 378020)
- [Icon Item] Zorg dat eigenschap "soepeler" echt iets doet
- Stel impliciete grootte uit brongrootte voor afbeelding/SVG URL-bronnen in
- voeg een nieuwe eigenschap toe in container, voor een bewerkingsmodus
- corrigeer maskRequestedPrefix wanneer geen prefix wordt gebruikt (bug 377893)
- [Menu] openRelative plaatsing harmoniseren
- De meeste (context)menu's hebben nu versnellers (Alt+letter sneltoetsen) (bug 361915)
- Plasma besturing gebaseerd op QtQuickControls2
- applyPrefixes laten handelen met een lege tekenreeks (bug 377441)
- oude themacaches echt verwijderen
- [Containment Interface] contextmenu's starten bij indrukken van toets "Menu"
- [Breeze Plasma Thema] actie-overlay pictogrammen verbeteren (bug 376321)

### Accentuering van syntaxis

- TOML: accentuering van escape-sequences in tekenreeksen repareren
- Syntaxis accentuering van Clojure bijwerken
- Een paar dingen om OCaml syntaxis bi te werken
- *.sbt bestanden als scala-code accentueren
- Ook de QML accentueerder voor .qmltypes bestanden gebruiken

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
