---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE stelt KDE Applicaties 17.04.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.04.3 beschikbaar
version: 17.04.3
---
13 juli 2017. Vandaag heeft KDE de derde update voor stabiliteit vrijgegeven voor <a href='../17.04.0'>KDE Applicaties 17.04</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 25 aangegeven reparaties van bugs, inclusief verbeteringen aan kdepim, dolphin, dragonplayer, kdenlive, umbrello, naast andere.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.34.
