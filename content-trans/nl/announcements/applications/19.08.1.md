---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE stelt Applicaties 19.08.1 beschikbaar.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE stelt Applicaties 19.08.1 beschikbaar
version: 19.08.1
---
{{% i18n_date %}}

Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../19.08.0'>KDE Applicaties 19.08</a> Deze uitgave bevat alleen reparaties van bugs en bijgewerkte vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan twintig aangegeven reparaties van bugs, die verbeteringen leveren aan Kontact, Dolphin, Kdenlive, Konsole, Step, naast andere.

Verbeteringen bevatten:

- Een aantal regressies in de behandeling van tabbladen van Konsole zijn gerepareerd
- Dolphin start opnieuw op de juiste manier wanneer in modus gesplitst beeld
- Een zacht gedeelte in de fysicasimulator Step veroorzaakt niet langer een crash
