---
aliases:
- ../../kde-frameworks-5.25.0
date: 2016-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Xeral

- Agora requírese Qt ≥ 5.5

### Attica

- Seguir redireccións de HTTP

### Iconas de Breeze

- actualizar as iconas de 16 px de correo para recoñecer mellor as iconas
- actualizar as iconas de estado de micrófono e son para ter a mesma disposición e tamaño
- Nova icona de aplicación da configuración do sistema
- engadir iconas de GNOME de estado simbólico
- engadir compatibilidade coas iconas simbólicas de GNOME 3
- Engadíronse iconas para Diaspora e Vector, consulte phabricator.kde.org/M59
- Novas iconas para Dolphin e Gwenview
- as iconas meteorolóxicas son iconas de estado, non de aplicación
- engadir algunhas ligazóns a xliff grazas a gnastyle
- engadir unha icona para Kig
- engadir iconas de tipos MIME, unha icona de KRDC e outras iconas de aplicación de gnastyle
- engadir unha icona de tipo MIME de certificado (fallo 365094)
- actualizar as iconas de Gimp grazas a gnastyle (fallo 354370)
- a icona de acción de globo xa non é un ficheiro ligado, úsese en Digikam
- actualizar as iconas de Labplot segundo a mensaxe de correo de 13.07. de Alexander Semke
- engadir iconas de aplicación de gnastyle
- engadir unha icona para KRuler de Yuri Fabirovsky
- corrixir ficheiros SVG rotos, grazas fuchs (fallo 365035)

### Módulos adicionais de CMake

- Corrixir unha inclusión cando falta Qt 5
- Engadir un método de reserva para query_qmake() cando non haxa instalación de Qt 5
- Asegurarse de que ECMGeneratePriFile.cmake se comporta como o resto dos ECM
- Cambiou o lugar preferido dos datos de Appstream

### KActivities

- [KActivities-CLI] ordes para iniciar ou deter unha actividade
- [KActivities-CLI] Definir e obter o nome, icona e descrición de actividade
- Engadir unha aplicación de interface da liña de ordes para controlar actividades
- Engadir scripts para cambiar a actividades anteriores ou seguintes
- O método para inserir en QFlatSet agora devolve o índice xunto co iterador (fallo 365610)
- Engadir funcións de ZSH para deter e eliminar actividades distintas da actual
- Engadiuse a propiedade isCurrent a KActivities::Info
- Usar iteradores constantes ao buscar unha actividade

### Ferramentas de Doxygen de KDE

- Moitas melloras no formato de saída
- Agora Mainpage.dox ten maior prioridade que README.md

### KArchive

- Xestionar varios fluxos de gzip (fallo 232843)
- Asumir que un directorio é un directorio aínda que o bit de permisos estea mal definido (fallo 364071)

### KBookmarks

- KBookmarkGroup::moveBookmark: corrixir o valor que se devolve cando o elemento xa está na posición correcta

### KConfig

- Engadir un atallo estándar a DeleteFile e RenameFile

### KConfigWidgets

- Engadíronse as accións estándar DeleteFile e RenameFile
- Agora a páxina de configuración ten barras de desprazamento cando se necesitan (fallo 362234)

### KCoreAddons

- Instalar licenzas coñecidas e atopalas en tempo de execución (corrección dunha regresión) (fallo 353939)

### KDeclarative

- Emitir valueChanged de verdade

### KFileMetaData

- Comprobar se xattr existe durante o paso de configuración, senón a construción podería fallar (se falta xattr.h)

### KGlobalAccel

- Usar o D-Bus de KLauncher en vez de KRun (fallo 366415)
- Iniciar as accións da lista de salto mediante KGlobalAccel
- KGlobalAccel: Corrixir un bloqueo indefinido en Windows ao saír

### KHTML

- Permitir porcentaxe como unidade do raio dos bordos
- Retirouse a versión prefixada das propiedades de fondo e de raio de bordo
- Correccións na función de construtor de atallo de 4 valores
- Só crear obxectos de cadea se se van usar

### KIconThemes

- Mellorar de maneira considerábel o rendemento de makeCacheKey, xa que é unha ruta de código crítica na consulta de iconas
- KIconLoader: reducir o número de buscas ao recorrer á reserva
- KIconLoader: mellora masiva de velocidade na carga de iconas non dispoñíbeis
- Non baleirar a liña de busca ao cambiar de categoría
- KIconEngine: corrixir que QIcon::hasThemeIcon sempre devolvese verdadeiro (fallo 365130)

### KInit

- Adaptar KInit a Mac OS X

### KIO

- Corrixir KIO::linkAs() para que funcione como se describe, é dicir, fallar se o destino xa existe
- Corrixir KIO::put("file:///ruta") para respectar os permisos de novos ficheiros (fallo 359581)
- Corrixir KIO::pasteActionText para elementos de destino nulos e para URL baleiros
- Engadir a posibilidade de desfacer a creación de ligazóns
- Opción de interface gráfica para configurar o MarkPartial global para escravos de KIO
- Corrixir MaxCacheSize limitado a 99 KiB
- Engadir botóns de portapapeis ao separador das sumas de comprobación
- KNewFileMenu: corrixir copiar un ficheiro de modelo dun recurso incrustado (fallo 359581)
- KNewFileMenu: Corrixir a creación dunha ligazóns á aplicación (fallo 363673)
- KNewFileMenu: Corrixir unha suxestión dun novo nome de ficheiro cando o ficheiro xa existe no escritorio
- KNewFileMenu: asegurarse de que se emite fileCreated() tamén para ficheiros de escritorio de aplicación
- KNewFileMenu: corrixir a creación de ligazóns simbólicas cun destino relativo
- KPropertiesDialog: simplificar o uso de caixa de botón, corrixir o comportamento ante Esc
- KProtocolInfo: encher de novo a caché para atopar protocolos instalador recentemente
- KIO::CopyJob: migrar a qCDebug (cunha zona de seu, dado que pode ser moi detallado)
- KPropertiesDialog: engadir un separador de sumas de comprobación
- Limpar o URL da ruta antes de inicializar KUrlNavigator

### KItemModels

- KRearrangeColumnsProxyModel: corrixir a aserción de index(0, 0) nun modelo baleiro
- Corrixir que KDescendantsProxyModel::setSourceModel() non baleirase as cachés internas
- KRecursiveFilterProxyModel: corrixir a corrupción de QSFPM por excluír o sinal rowsRemoved (fallo 349789)
- KExtraColumnsProxyModel: codificar hasChildren()

### KNotification

- Non definir o pai da disposición subordinada manualmente, silencia un aviso

### Infraestrutura de paquetes

- Inferir o ParentApp do complemento PackageStructure
- Permitir que kpackagetool5 xere información de AppStream para compoñentes de kpackage
- Permitir cargar o ficheiro metadata.json desde kpackagetool5

### Kross

- Retirar dependencias innecesarias de KF5

### KService

- applications.menu: retirar referencias a categorías sen usar
- Actualizar sempre o analizador de Trader de fontes de yacc e lex

### KTextEditor

- Non preguntar dúas veces se sobrescribir un ficheiro con diálogos nativos
- engadiuse a sintaxe FASTQ

### KWayland

- [cliente] Usar un QPointer para o enteredSurface de Pointer
- Expoñer Geometry en PlasmaWindowModel
- Engadir o evento geometry a PlasmaWindow
- [src/server] Verificar que a superficie ten o recurso antes de enviar a entrada do punteiro
- Engadir compatibilidade con xdg-shell
- [servidor] Enviar correctamente un baleirado de selección antes de entrar o foco do teclado
- [servidor] Xestionar mellor a situación de falta de XDG_RUNTIME_DIR

### KWidgetsAddons

- [KCharSelect] Corrixir a quebra ao buscar sen un ficheiro de datos presente (fallo 300521)
- [KCharSelect] Xestionar caracteres fóra de BMP (fallo 142625)
- [KCharSelect] Actualizar kcharselect-data a Unicode 9.0.0 (fallo 336360)
- KCollapsibleGroupBox: deter a animación no destrutor se continúa
- Actualizar a paleta de Breeze (sincronizar desde KColorScheme)

### KWindowSystem

- [xcb] Asegurarse de que se emita o sinal compositingChanged se NETEventFilter se crea de novo (fallo 362531)
- Engadir unha API de comodidade para consultar o sistema de xanelas ou a plataforma que Qt usa

### KXMLGUI

- Corrixir o consello de tamaño mínimo (texto recortado) (fallo 312667)
- [KToggleToolBarAction] Respectar a restrición action/options_show_toolbar

### NetworkManagerQt

- Usar WPA2-PSK e WPA2-EAP de maneira predeterminada ao obter o tipo de seguranza da configuración da conexión

### Iconas de Oxygen

- engadir application-menu a Oxygen (fallo 365629)

### Infraestrutura de Plasma

- Manter a rañura createApplet compatíbel coa versión 5.24 das infraestruturas
- Non eliminar a textura de gl dúas veces na miniatura (fallo 365946)
- Engadir un dominio de tradución ao obxecto wallpaper de QML
- Non eliminar miniaplicativos manualmente
- Engadir un kapptemplate para fondo de escritorio de Plasma
- Modelos: rexistrar os modelos na propia categoría raíz «Plasma/»
- Modelos: actualizar as ligazóns ao wiki de TechBase nos README
- Define que packagestructures de Plasma estenden plasmashell
- permitir un tamaño ao engadir miniaplicativos
- Definir os PackageStructure de Plasma como complementos PackageStructure normais de KPackage
- Corrección: actualizar a QtQuick.Controls o config.qml do exemplo de fondo de escritorio Outono
- Usar KPackage para instalar paquetes de Plasma
- Se pasamos un QIcon como argumento a IconItem::Source, usalo
- Engadir a funcionalidade de capa ao IconItem de Plasma
- Engadir Qt::Dialog ás marcas predeterminadas para contentar a QXcbWindow::isTransient() (fallo 366278)
- [Tema Breeze de Plasma] Engadir as iconas network-flightmode-on e network-flightmode-off
- Emitir contextualActionsAboutToShow antes de mostrar o menú contextualActions do miniaplicativo (fallo 366294)
- [TextField] Asociar coa lonxitude de TextField en vez da de text
- [Estilos dos botóns] Centrar horizontalmente no modo de só iconas (fallo 365947)
- [Containment] Tratar HiddenStatus como un estado baixo
- Engadir unha icona da área de notificacións para KRuler de Yuri Fabirovsky
- Corrixir o famoso fallo de «os diálogos aparecen no xestor de tarefas» unha vez máis
- corrixir a icona de dispoñibilidade de rede sen fíos cun emblema ? (fallo 355490)
- IconItem: Usar un xeito mellor de desactivar a animación ao ir de invisíbel a visíbel
- Definir o tipo de xanela de consello en ToolTipDialog a través da API de KWindowSystem

### Solid

- Actualizar sempre o analizador de Predicate de fontes de yacc e lex

### Sonnet

- hunspell: limpar o código de busca de dicionarios, engadir os directorios de XDG (fallo 361409)
- Intentar corrixir un pouco o uso de detección de idioma do filtro de idioma

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
