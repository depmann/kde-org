---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE rilascia Applications 15.04.1.
layout: application
title: KDE rilascia KDE Applications 15.04.1
version: 15.04.1
---
12 maggio 2015. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../15.04.0'>KDE Applications 15.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 50 errori corretti includono miglioramenti a kdelibs, kdepim, Kdenlive, Okular, Marble e Umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine (Long Term Support) di Plasma Workspaces 4.11.19, KDE Development Platform 4.14.8 e della suite Kontact 4.14.8.
