---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE rilascia Applications 19.04.1.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE rilascia KDE Applications 19.04.1
version: 19.04.1
---
{{% i18n_date %}}

Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../19.04.0'>KDE Applications 19.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Più di venti errori corretti includono, tra gli altri, miglioramenti a Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle e Umbrello.

I miglioramenti includono:

- L'attribuzione di etichette sul desktop non tronca più il nome dell'etichetta
- È stato risolto un blocco nell'estensione di condivisione del testo di KMail
- Sono state corrette diverse regressioni nell'editor video Kdenlive
