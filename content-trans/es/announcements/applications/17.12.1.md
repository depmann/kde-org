---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE lanza las Aplicaciones de KDE 17.12.1
layout: application
title: KDE lanza las Aplicaciones de KDE 17.12.1
version: 17.12.1
---
Hoy, 11 de enero de 2018. KDE ha lanzado la primera actualización de estabilización para las <a href='../17.12.0'>Aplicaciones de KDE 17.12</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- Se ha corregido el envío de mensajes en Kontact para ciertos servidores SMTP.
- Se han mejorado la escala temporal de Gwenview y la búsqueda por etiquetas.
- Se ha corregido la importación de Java en Umbrello, la herramientas de diagramas UML.
