---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE lanza las Aplicaciones de KDE 16.04.0
layout: application
title: KDE lanza las Aplicaciones de KDE 16.04.0
version: 16.04.0
---
Hoy, 20 de abril de 2016, KDE presenta las Aplicaciones 16.04 con un impresionante conjunto de actualizaciones relacionadas con una mayor facilidad de acceso, la introducción de funcionalidades de gran utilidad y la solución de problemas menores. Todo ello contribuye a que ahora las Aplicaciones de KDE estén un paso más cerca de ofrecer la configuración perfecta para su dispositivo.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> y <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> se han migrado a KDE Frameworks 5 y esperamos con ilusión sus comentarios sobre las funcionalidades Más recientes introducidas en esta versión. También le recomendamos fervientemente que apoye a <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a>, la nueva incorporación a las Aplicaciones de KDE y esperamos sus comentarios sobre qué más le gustaría ver.

### La más reciente incorporación de KDE

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

Se ha añadido una nueva incorporación al conjunto de aplicaciones educativas de KDE. <a href='https://minuet.kde.org'>Minuet</a> es una aplicación para educación musical que incorpora la completa implementación de MIDI con control de ritmo, tono y volumen, lo que la hace adecuada tanto para principiantes como para músicos experimentados.

Minuet incluye 44 ejercicios para entrenar el oído musical en escalas, acordes, intervalos y ritmo. Permite la visualización de contenido musical en el teclado del piano y permite la integración completa de sus ejercicios.

### Más ayuda para el usuario

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

KHelpCenter, que antes se distribuía con Plasma, ahora se distribuye con las Aplicaciones de KDE.

El equipo de KHelpCenter ha llevado a cabo una enorme campaña de limpieza y eliminación de errores que ha finalizado con la solución de 49 fallos, muchos de los cuales estaban relacionados con la mejora y el restablecimiento de funcionalidad de búsqueda que antes no funcionaba correctamente.

La búsqueda interna en documentos, que dependía de la aplicación obsoleta ht::/dig, se ha sustituido por un nuevo sistema de búsqueda e indexación basado en Xapian, el cual aporta el restablecimiento de funcionalidades como la búsqueda en páginas man, en páginas de información y en la documentación proporcionada por KDE, con un conjunto añadido de marcadores para las páginas de documentación.

Con la eliminación del mantenimiento para KDELibs4 y una limpieza adicional del código y de otros errores menores, el mantenimiento del código también se ha reforzado para presentar KHelpCenter con un nuevo y reluciente aspecto.

### Agresivo control de plagas

Se han solucionado 55 errores en el conjunto de aplicaciones Kontact, alguno de los cuales estaban relacionados con problemas al establecer alarmas, la importación de mensajes de Thunderbird y minimizar los iconos de conversación de Skype y Google en la vista del panel de «Contactos». También había errores en soluciones alternativas relacionadas con KMail, como la importación de carpetas, en la importación de vCards, en la apertura de adjuntos con formato ODF, al insertar una URL desde Chromium, en las diferencias en el menú de herramientas con la aplicación que empezaron como parte de Kontact, a diferencia de su uso independiente, en la falta de la opción de menú «Enviar», entre otras cosas. Se ha añadido el idioma Portugués de Brasil en los canales RSS y se ha solucionado el problema con las direcciones para los canales españoles y húngaros.

Las nuevas funcionalidades incluyen un nuevo diseño del editor de contactos KAddressbook, un nuevo tema predeterminado para la cabecera de KMail, mejoras para el exportador de preferencias y la solución de la implementación de Favicon en Akregator. La interfaz de redacción de KMail se ha remodelado con la introducción de un nuevo tema predeterminado para la cabecera de KMail y se usa el tema Grantlee para la página «Acerca de» en KMail, además de en Kontact y en Akregator. Ahora, Akregator usa QtWebKit, uno de los motores más importantes para representar páginas web y ejecutar código Javascript como motor de representación de páginas. Ya ha empezado el proceso de implementación de QtWebEngine en Akregator y en otras aplicaciones de Kontact. Mientras que varias funcionalidades se han convertido en complementos en kdepim-addons, las bibliotecas pim se han dividido en varios paquetes.

### Compresión en profundidad

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, el gestor de archivos comprimidos, ha experimentado dos importantes soluciones de errores de manera que ahora, Ark advierte al usuario si falla la extracción debido a que no hay suficiente espacio en la carpeta de destino y no llena la RAM durante la extracción de grandes archivos a través de arrastrar y soltar.

Ahora, Ark también incluye un diálogo de propiedades que muestra información como el tipo de archivo, tamaño comprimido y descomprimido y resúmenes criptográficos MD5/SHA-1/SHA-256 sobre el archivo abierto en ese momento.

Ahora Ark también puede abrir y extraer archivos RAR sin utilizar las utilidades RAR no libres y puede abrir, extraer y crear archivos TAR comprimidos con los formatos lzop/lzip/lzrip.

La interfaz de usuario de Ark se ha remodelado mediante la reorganización de la barra de menú y la barra de herramientas para mejorar la usabilidad y para eliminar ambigüedades, así como para ahorrar espacio vertical gracias a la barra de estado, ahora oculta de manera predeterminada.

### Mayor precisión en la edición de vídeo

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

Se han implementado muchas nuevas funcionalidades en <a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, el editor de vídeo no lineal. Ahora el titulador tiene funcionalidad de rejilla, gradientes, adición de sombreado de texto y ajuste del espacio de letras y líneas.

La visualización integrada de los niveles de audio permite la monitorización del audio del proyecto de manera sencilla. Además, en el nuevo monitor de vídeo aparece la frecuencia de fotogramas sobreimpresionada durante la reproducción. También incluye zonas seguras, ondas de audio, una barra de herramientas para buscar marcadores y un monitor de escala.

También hay una nueva funcionalidad en la biblioteca que permite copiar y pegar secuencias entre proyectos y una vista dividida en la línea del tiempo para comparar y visualizar los efectos aplicados al clip con el otro sin efectos.

El diálogo de representación se ha rehecho y se ha añadido una nueva opción para obteber una codificación más rápida, con lo que se ha conseguido que la producción de archivos grandes y el efecto de velocidad funcionen también con audio.

Se han introducido curvas en los fotogramas clave para conseguir algunos efectos y ahora el usuario puede acceder rápidamente a sus efectos preferidos mediante el elemento gráfico de efectos preferidos. Cuando se utiliza la herramienta de guillotina, ahora la línea vertical en la línea del tiempo mostrará el fotograma preciso en el que se realizará el corte y los nuevos generadores de clips que se han añadido permitirán crear clips de barras de color y contadores. Además, el contador de uso de clip se ha vuelto a introducir en el panel de Proyecto y las miniaturas de audio también se han rehecho para hacerlas mucho más rápidas.

Las principales soluciones de errores incluyen el fallo cuando se usaban títulos (que requiere la última versión de MLT), corregir corrupciones que se producían cuando la frecuencia de fotogramas por segundo es diferente a 25, los fallos en el borrado de pistas, el problemático modo de sobrescritura en la línea del tiempo y la corrupción o pérdida de efectos cuando se usaba un formato local que usaba coma como separador. Aparte de estos errores, el equipo ha estado trabajando de manera constante en realizar importantes mejoras en la estabilidad el flujo de trabajo y en pequeñas características de usabilidad.

### ¡Y aún más!

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, el visor de documentos trae incorpora nuevas funcionalidades en forma de anotaciones en línea con borde personalizables, posibilidad de abrir directamente los archivos integrados en lugar de solo guardarlos y también la visualización de marcadores de la tabla de contenidos cuando los enlaces hijos están contraídos.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> incorpora una mejor implementación del GnuPG 2.1 con la solución de errores de prueba automática y los incómodos refrescos de teclas causados por la nueva distribución del directorio GnuPG (GNU Privacy Guard). Se ha añadido La generación de claves ECC con la visualización de información de Curve para los certificados ECC. Ahora, Kleopatra se lanza como paquete aparte y ahora admite archivos .pfx y .crt. Para resolver la diferencia de comportamiento de las claves secretas importadas y las claves generadas, ahora, Kleopatra permite establecer la confianza del propietario para las claves secretas importadas.
