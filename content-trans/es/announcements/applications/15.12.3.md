---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE lanza las Aplicaciones de KDE 15.12.3
layout: application
title: KDE lanza las Aplicaciones de KDE 15.12.3
version: 15.12.3
---
Hoy, 15 de marzo de 2016, KDE ha lanzado la tercera actualización de estabilización para las <a href='../15.12.0'>Aplicaciones KDE 15.12</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 15 correcciones de errores registradas, se incluyen mejoras en Kdepim, Akonadi, Ark, KBlocks, KCalc, KTouch y Umbrello, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.18 que contará con asistencia a largo plazo.
