---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE lanza las Aplicaciones de KDE 17.12.2
layout: application
title: KDE lanza las Aplicaciones de KDE 17.12.2
version: 17.12.2
---
Hoy, 8 de febrero de 2018, KDE ha lanzado la segunda actualización de estabilización para las <a href='../17.12.0'>Aplicaciones de KDE 17.12</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en Kontact, Dolphin, Gwenview, KGet y Okular, entre otras aplicaciones.
