---
aliases:
- ../announce-applications-17.08-beta
date: 2017-07-21
description: KDE publie la version 17.08 « bêta » des applications de KDE.
layout: application
release: applications-17.07.80
title: KDE publie la version « bêta » des applications de KDE version 17.08.
---
21 Juillet 2017. Aujourd'hui, KDE a publié la version « bêta » des nouvelles versions des applications de KDE. Les dépendances et les fonctionnalités sont stabilisées et l'équipe KDE se concentre à présent sur la correction des bogues et sur les dernières finitions.

Veuillez vérifier les <a href='https://community.kde.org/Applications/17.08_Release_Notes'>notes de mises à jour de la communauté</a> pour plus d'informations sur les nouveaux fichiers compressés, maintenant bâtis pour KDE 5, et les problèmes connus. Une annonce plus complète sera disponible avec la mise à jour finale.

Les versions des applications de KDE 17.08 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version « bêta » et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.
