---
aliases:
- ../../kde-frameworks-5.36.0
date: 2017-07-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Tots els Frameworks: opció per construir i instal·lar el fitxer QCH amb la «dox» de l'API pública

### Baloo

- Usa FindInotify.cmake per decidir si «inotify» és disponible

### Icones Brisa

- No dependre innecessàriament del «bash», i no validar les icones predeterminades

### Mòduls extres del CMake

- FindQHelpGenerator: evita la selecció de la versió Qt4
- ECMAddQch: falla amb notificació si les eines necessàries no són presents, per evitar sorpreses
- Descarta el Perl com a dependència per a «ecm_add_qch», no cal/s'usa
- Explora tota la carpeta d'instal·lació per a les dependències del QML
- Nou: ECMAddQch, per generar fitxers QCH i d'etiquetes del «doxygen»
- Esmena KDEInstallDirsTest.relative_or_absolute_usr, evitant que s'usin camins Qt

### KAuth

- Comprova l'estat d'error després de cada ús del PolKitAuthority

### KBookmarks

- Emet errors quan manca el «keditbookmarks» (error 303830)

### KConfig

- Esmena per a CMake 3.9

### KCoreAddons

- Usa FindInotify.cmake per decidir si «inotify» és disponible

### KDeclarative

- KKeySequenceItem: fa possible enregistrar Ctrl+Núm+1 com a drecera
- Comença l'arrossegament amb prémer i aguantar en esdeveniments tàctils (error 368698)
- No confia en el fet que QQuickWindow ofereixi QEvent::Ungrab com a «mouseUngrabEvent» (com ja no ho fa a les Qt 5.8+) (error 380354)

### Compatibilitat amb les KDELibs 4

- Carca de KEmoticons, que és una dependència del «config.cmake.in» del CMake (error 381839)

### KFileMetaData

- Afegeix un extractor usant «qtmultimedia»

### KI18n

- Assegura que es genera l'objectiu dels fitxers TS

### KIconThemes

- Més detalls quant al desplegament dels temes d'icones al Mac i MSWin
- Canvia la mida predeterminada de les icones del plafó a 48

### KIO

- [KNewFileMenu] Oculta el menú «Enllaç a dispositiu» si estarà buit (error 381479)
- Usa KIO::rename en lloc de KIO::moveAs a «setData» (error 380898)
- Esmena la posició del menú desplegable en el Wayland
- KUrlRequester: Defineix el senyal NOTIFY al «textChanged()» a les propietats del text
- [KOpenWithDialog] Nom de fitxer amb HTML-escapat
- KCoreDirLister::cachedItemForUrl: no crea la memòria intermèdia si no existeix
- Usa «data» com a nom de fitxer en copiar URL de dades (error 379093)

### KNewStuff

- Esmena una detecció incorrecta d'error de fitxers «knsrc» que manquen
- Exposa i usa la variable de mida de pàgina de l'Engine
- Fa possible usar QXmlStreamReader per llegir un fitxer de registre del KNS

### Framework del KPackage

- Afegeix «kpackage-genericqml.desktop»

### KTextEditor

- Soluciona els pics d'ús de la CPU després de mostrar la barra d'ordres del «vi» (error 376504)
- Esmena la barra de desplaçament arrossegada que salta quan està habilitat el mapa en miniatura
- Salta a la barra de desplaçament clicada quan el mapa en miniatura està habilitat (error 368589)

### KWidgetsAddons

- Actualitza les «kcharselect-data» a Unicode 10.0

### KXMLGUI

- KKeySequenceWidget: fa possible enregistrar Ctrl+Núm+1 com a drecera (error 183458)
- Reverteix «En construir jerarquies de menús, situa els menús pares en els seus contenidors»
- Reverteix «Usa directament "transientparent"»

### NetworkManagerQt

- WiredSetting: les propietats de «wake on lan» s'han adaptat al NM 1.0.6
- WiredSetting: la propietat «metered» s'ha adaptat al NM 1.0.6
- Afegeix propietats noves a diverses classes de paràmetres
- Dispositiu: afegeix estadístiques del dispositiu
- Afegeix un dispositiu «IpTunnel»
- WiredDevice: afegeix informació quant a la versió requerida del NM per a la propietat s390SubChannels
- TeamDevice: afegeix una propietat nova de configuració (a partir de NM 1.4.0)
- Dispositiu amb fil: afegeix la propietat s390SubChannels
- Actualitza les introspeccions (NM 1.8.0)

### Frameworks del Plasma

- Assegura que la mida és la final després de «showEvent»
- Esmena els marges i l'esquema de colors de la icona VLC de la safata
- Estableix que els contenidors tinguin el focus a la vista (error 381124)
- Genera la clau antiga abans d'actualitzar «enabledborders» (error 378508)
- Mostra el botó de contrasenya encara que el text estigui buit (error 378277)
- Emet «usedPrefixChanged» quan el prefix està buit

### Solid

- CMake: construeix el dorsal «udisks2» al FreeBSD només quan s'ha activat

### Ressaltat de la sintaxi

- Ressalta els fitxers «.julius» com a JavaScript
- Haskell: Afegeix totes les «pragmas» del llenguatge com a paraules clau
- CMake: OR/AND no ressaltat després d'una expressió entre () (error 360656)
- Makefile: Elimina les entrades de paraules clau no vàlides en el «makefile.xml»
- Indexador: Millora la informació dels errors
- Actualitza la versió del fitxer de sintaxi HTML
- Afegeix els modificadors angulars en els atributs HTML
- Actualitza les dades de referència de proves seguint els canvis del commit anterior
- Error 376979 - les claus angulars en els comentaris del «doxygen» trenquen el ressaltat de sintaxi

### ThreadWeaver

- Solució alternativa de l'error del compilador MSVC2017

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
