---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE distribueix les aplicacions 17.08.2 del KDE
layout: application
title: KDE distribueix les aplicacions 17.08.2 del KDE
version: 17.08.2
---
12 d'octubre de 2017. Avui KDE distribueix la segona actualització d'estabilització per a les <a href='../17.08.0'>aplicacions 17.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 25 esmenes registrades d'errors que inclouen millores al Kontact, Dolphin, Gwenview, Kdenlive, Marble, i Okular entre d'altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.37.

Les millores inclouen:

- S'ha esmenat una fuita de memòria i una fallada a la configuració del connector d'esdeveniments del Plasma
- Els missatges llegits ja no s'eliminen immediatament del filtre de no llegits a l'Akregator
- L'importador del Gwenview ara utilitza la data i hora de l'EXIF
