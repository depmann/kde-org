---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: Es distribueixen les aplicacions 19.08.2 del KDE.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: Es distribueixen les aplicacions 19.08.2 del KDE
version: 19.08.2
---
{{% i18n_date %}}

Avui, KDE distribueix la segona actualització d'estabilització per a les <a href='../19.08.0'>Aplicacions 19.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, i proporciona una actualització segura i millor per a tothom.

Hi ha més de vint esmenes registrades d'errors que inclouen millores al Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize i Spectacle, entre d'altres.

Les millores inclouen:

- S'ha millorat el funcionament de l'alta densitat de PPP al Konsole i a altres aplicacions
- En commutar entre cerques diferents al Dolphin ara els paràmetres de les cerques s'actualitzen correctament
- El KMail pot tornar a desar missatges directament en carpetes remotes
