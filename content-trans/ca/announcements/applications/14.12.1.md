---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE distribueix les aplicacions 14.12.1 del KDE.
layout: application
title: KDE distribueix les aplicacions 14.12.1 del KDE
version: 14.12.1
---
13 de gener de 2015. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../14.12.0'>aplicacions 14.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 50 esmenes registrades d'errors que inclouen millores a l'eina d'arxivament Ark, el modelador UML Umbrello, el visualitzador de documents Okular, l'aplicació d'aprenentatge de pronunciació Artikulate i el client d'escriptori remot KRDC.

Aquest llançament també inclou les versions de suport a llarg termini dels espais de treball Plasma 4.11.15, la plataforma de desenvolupament KDE 4.14.4 i el paquet Kontact 4.14.4.
