---
aliases:
- ../announce-4.11.3
date: 2013-11-05
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.3.
title: KDE toimittaa marraskuun päivitykset Plasma-työtiloihin, sovelluksiin ja ohjelmistoalustaan
---
5. marraskuuta 2013. Tänään KDE julkaisi päivitykset sen työtiloihin, sovelluksiin ja kehitysalustaan. Nämä päivitykset ovat kolmas 4.11-sarjan kuukausittaisten vakauspäivitysten sarjassa. Kuten 4.11:n julkaisun yhteydessä tiedotettiin, työtiloille tarjotaan päivityksiä seuraavien kahden vuoden ajan. Koska tässä julkaisussa on pelkästään virhekorjauksia ja käännöspäivityksiä, päivitys on turvallinen ja miellyttävä jokaiselle.

Ainakin 120 virhekorjaukseen kuuluu parannuksia ikkunointiohjelmaan KWiniin, tiedostonhallintaan Dolphiniin, henkilökohtaisten tietojen hallintapakettiin Kontactiin, UML-työkaluun Umbrelloon sekä muualle. Julkaisussa on useita vakauskorjauksia sekä tavalliset käännöstäydennykset.

A more complete <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.3&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>list</a> of changes can be found in KDE's issue tracker. For a detailed list of changes that went into 4.11.3, you can also browse the Git logs.

Lataa lähdekoodi tai asennettavat paketit siirtymällä <a href='/info/4/4.11.3'>4.11.3-infosivulle</a>. Jos haluat tietää lisää KDE:n työtilojen, sovellusten ja kehitysalustan 4.11-versioista, lue <a href='/announcements/4.11/'>4.11:n julkaisutiedote</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Kontactin uusi tapa sähköpostien lähettämiseen myöhemmin` width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href='http://download.kde.org/stable/4.11.3/'>download.kde.org</a> or from any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.
