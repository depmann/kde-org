---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE veröffentlicht die KDE-Anwendungen 17.12.1
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.12.1
version: 17.12.1
---
11. Januar 2018. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../17.12.0'>KDE-Anwendungen 17.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta und Umbrello.

Verbesserungen umfassen:

- Sending mails in Kontact has been fixed for certain SMTP servers
- Gwenview's timeline and tag searches have been improved
- JAVA import has been fixed in Umbrello UML diagram tool
