---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Rätta gräns- och positionshantering i SearchStore::exec
- Återskapa index i Baloo
- balooctl config: Lägg till väljare för att ställa in och visa onlyBasicIndexing
- Konvertera balooctl check för att fungera med ny arkitektur (fel 353011)
- FileContentIndexer: Rätta att filePath skickas två gånger
- UnindexedFileIterator: mtime är quint32 inte quint64
- Transaction: Rätta ytterligare ett stavfel av Dbi
- Transaction: Rätta att documentMTime() och documentCTime() använder fel Dbi:er.
- Transaction::checkPostingDbInTermsDb: Optimera kod
- Rätta D-Bus varningar
- Balooctl: Lägg till checkDb kommando
- balooctl config: Lägg till "exclude filter"
- KF5Baloo: Säkerställ att D-Bus gränssnitt skapas innan de används (fel 353308).
- Undvik användning av QByteArray::fromRawData
- Ta bort baloo-monitor från Baloo
- TagListJob: Skicka fel när databasen inte kunde öppnas
- Ignorera inte delbegrepp om inte hittade
- Renare kod för misslyckad Baloo::File::load() när databasen inte kunde öppnas.
- Låt balooctl använda IndexerConfig istället för att direkt manipulera baloofilerc
- Förbättra i18n för balooshow
- Låt balooshow misslyckas på ett snyggt sätt om databasen inte kan öppnas.
- Låt Baloo::File::load() misslyckas om databasen inte är öppen (fel 353049).
- IndexerConfig: Lägg till metoden refresh()
- inotify: Simulera inte en closedWrite händelse efter förflyttning utan kaka
- ExtractorProcess: Ta bort extra n i slutet av filePath
- baloo_file_extractor: Anropa QProcess::close innan QProcess förstörs
- baloomonitorplugin/balooctl: Lägg till i18 för indexerarens tillstånd.
- BalooCtl: Lägg till en 'config' väljare
- Gör ballosearch mer presentabel
- Ta bort tomma EventMonitor-filer
- BalooShow: Visa mer information när identifierare inte matchar
- BalooShow: Vid anrop med en identifierare kontrollera om den är korrekt
- Lägg till klassen FileInfo
- Lägg till felkontroll på diverse ställen så att Baloo inte kraschar när det inaktiveras (fel 352454).
- Rätta att Baloo inte respekterar inställningsalternativet "basic indexing only"
- Övervakning: Hämta återstående tid vid start
- Använd verkliga metodanrop i MainAdaptor istället för QMetaObject::invokeMethod
- Lägg till org.kde.baloo gränssnitt för rotobjekt för bakåtkompatibilitet
- Rätta datumsträng visad i adressraden på grund av konvertering till QDate
- Lägg till fördröjning efter varje fil istället för varje omgång
- Ta bort beroende på Qt::Widgets från baloo_file
- Ta bort oanvänd kod från baloo_file_extractor
- Lägg till Baloo-övervakare eller experimentellt QML-insticksprogram
- Gör "kontroll av återstående tid" trådsäker
- kioslaves: Lägg till saknad överskridning av virtuella funktioner
- Extractor: Ställ in applicationData efter att programmet har konstruerats
- Query: Implementera stöd för 'offset'
- Balooctl: Lägg till --version och --help (fel 351645)
- Ta bort stöd för KAuth för att öka maximalt antal inotify-övervakare om antalet är för litet (fel 351602)

### BluezQt

- Rätta krasch av fakebluez i obexmanagertest med ASAN
- Framåtdeklarera alla exporterade klasser i types.h
- ObexTransfer: Ange fel när överföringssession tas bort
- Utils: Behåll pekare till hanterares instanser
- ObexTransfer: Ange fel när org.bluez.obex kraschar

### Extra CMake-moduler

- Uppdatera GTK-ikoncache när ikoner installeras.
- Ta bort provisorisk lösning för att fördröja körning på Android
- ECMEnableSanitizers: Den odefinierade saneraren stöds av gcc 4.9
- Inaktivera detektering av X11,XCB etc. på OS X
- Titta efter filerna på installationsprefix istället för prefixsökvägen
- Använd Qt5 för att ange vad installationsprefix för Qt5 är
- Lägg till definitionen ANDROID där det behövs i qsystemdetection.h.

### Integrering med ramverk

- Rätta problem med att fildialogrutan slumpmässigt inte dyker upp (fel 350758).

### KActivities

- Använd en egen matchningsfunktion istället för glob i Sqlite (fel 352574).
- Rättade problem med tillägg av en ny resurs i modellen

### KCodecs

- Rätta krasch i UnicodeGroupProber::HandleData med korta strängar

### KConfig

- Markera kconfig-compiler som ett icke grafiskt användarverktyg

### KCoreAddons

- KShell::splitArgs: Enbart ASCII mellanslag är skiljetecken, inte Unicode mellanslag U+3000 (fel 345140)
- KDirWatch: Rätta krasch när en global statisk destruktor använder KDirWatch::self() (fel 353080)
- Rätta krasch när KDirWatch används i Q_GLOBAL_STATIC.
- KDirWatch: Rätta trådsäkerhet
- Klargör hur KAboutData konstruktorargument anges.

### KCrash

- KCrash: Skicka aktuell arbetskatalog till kdeinit när programmet startas om automatiskt via kdeinit (fel 337760).
- Lägg till KCrash::initialize() så att program och plattformsinsticksprogrammet explicit kan aktivera KCrash.
- Inaktivera ASAN om aktiverad

### KDeclarative

- Mindre förbättringar i ColumnProxyModel
- Gör det möjligt för program att veta sökvägen till hemkatalogen
- Flytta EventForge från skrivbordsomgivningen
- Tillhandahåll aktiveringsegenskap för QIconItem.

### KDED

- kded: Förenkla logik i närheten av sycoca, anropa bara ensureCacheValid.

### Stöd för KDELibs 4

- Anropa newInstance från underliggande process vid första anropande
- Använd definitioner från kdewin.
- Försök inte hitta X11 på WIN32
- cmake: Rätta versionscheck för taglib i FindTaglib.cmake.

### KDesignerPlugin

- Qt moc kan inte hantera makron (QT_VERSION_CHECK)

### KDESU

- kWarning -&gt; qWarning

### KFileMetaData

- Implementera Windows usermetadata

### KDE GUI Addons

- Att inte leta efter X11/XCB är också vettigt för WIN32

### KHTML

- Ersätt std::auto_ptr med std::unique_ptr
- khtml-filter: Förkasta regler som innehåller särskilda reklamblockeringsfunktioner som vi inte ännu hanterar.
- khtml-filter: Omordning av kod, inga funktionella ändringar.
- khtml-filter: Ignorera reguljära uttryck med alternativ, eftersom vi inte stöder dem.
- khtml-filter: Rätta detektering av avgränsare för alternativ i reklamblockering.
- khtml-filter: Rensa bort efterföljande blanktecken.
- khtml-filter: Förkasta inte rader som börjar med '&amp;' eftersom den inte är ett särskilt reklamblockeringstecken.

### KI18n

- Ta bort strikta itereringar för msvc för att få ki18n att byggas

### KIO

- KFileWidget: Överliggande argument ska ha normalvärdet 0 som i alla grafiska komponenter.
- Säkerställ att storleken på byte-fältet som just lagrades in struct:en är stor nog innan targetInfo beräknas, annars kommer vi åt minne som inte hör till oss
- Rätta användning av Qurl vid anrop av QFileDialog::getExistingDirectory()
- Uppdatera enhetslistan i Solid innan förfrågan i kio_trash
- Tillåt trash: förutom trash:/ som webbadress för listDir (anropar listRoot) (fel 353181)
- KProtocolManager: Rätta låsning när EnvVarProxy används (fel 350890)
- Försök inte hitta X11 på WIN32
- KBuildSycocaProgressDialog: Använd Qt:s inbyggda upptagetindikering (fel 158672).
- KBuildSycocaProgressDialog: Kör kbuildsycoca5 med QProcess.
- KPropertiesDialog: Rätta att ~/.local kan vara symbolisk länk, jämför kanoniska sökvägar
- Lägg till stöd för delade nätverksplatser i kio_trash (fel 177023)
- Anslut till signalerna i QDialogButtonBox, inte QDialog (fel 352770)
- Cookies KCM: Uppdatera DBus-namn för kded5
- Använd JSON-filer direkt istället för kcoreaddons_desktop_to_json()

### KNotification

- Skicka inte signal om uppdatering av underrättelse två gånger
- Tolka bara om underrättelseinställning när den ändats
- Försök inte hitta X11 på WIN32

### KNotifyConfig

- Ändra metod för att läsa in förval
- Skicka namn på programmet vars inställning uppdaterades tillsammans med DBus-signalen
- Lägg till metod för att få kconfigwidget att återgå till förvalda värden
- Synkronisera inte inställningen n gånger när man sparar

### KService

- Använd senaste tidsstämpeln i underkatalog som tidsstämpel för resurskatalog.
- KSycoca: Lagra mtime för varje källkatalog, för att detektera ändringar (fel 353036).
- KServiceTypeProfile: Ta bort onödigt skapande av tillverkare (fel 353360)
- Förenkla och snabba upp KServiceTest::initTestCase.
- Gör installationsnamn på filen applications.menu en cachevariabel i cmake
- KSycoca: ensureCacheValid() ska skapa databasen om den inte finns
- KSycoca: Få global databas att fungera efter den senaste kontrollkoden för tidsstämplar
- KSycoca: Ändra databasens filnamn för att inkludera språk och sha1 för katalogerna den skapas från.
- KSycoca: Gör ensureCacheValid() en del av det öppna programmeringsgränssnittet.
- KSycoca: Lägg till en q-pekare för att ta bort mer singleton-användning
- KSycoca: Ta bort alla self()-metoder för tillverkare, lagra dem istället i KSycoca.
- KBuildSycoca: Ta bort skrivning av filen ksycoca5stamp.
- KBuildSycoca: Använd qCWarning istället för fprintf(stderr, ...) eller qWarning
- KSycoca: Bygg om ksycoca i processen istället för att köra kbuildsycoca5
- KSycoca: Flytta alla kod för kbuildsycoca till biblioteket, utom main().
- KSycoca optimering: Bevaka bara filen om programmet ansluter till databaseChanged()
- Rätta minnesläckor i klassen KBuildSycoca
- KSycoca: Ersätt DBus-underrättelse med filövervakning med KDirWatch.
- kbuildsycoca: Avråd från användning av väljaren --nosignal.
- KBuildSycoca: Ersätt DBus-baserad låsning med en låsfil.
- Krascha inte när ogiltig insticksinformation påträffas.
- Byt namn på deklarationsfiler till _p.h som förberedelse för att flytta till biblioteket kservice.
- Flytta checkGlobalHeader() inne i KBuildSycoca::recreate().
- Ta bort kod för --checkstamps och --nocheckfiles.

### KTextEditor

- Validera fler reguljära uttryck
- Rätta reguljära uttryck i HL-filer (fel 352662)
- Synkronisera ocaml HL med tillståndet i https://code.google.com/p/vincent-hugot-projects/ innan Google-kod är nere, några mindre felrättningar
- Lägg till ordbrytning (fel 352258)
- Validera rad innan radbrytningsgrejer anropas (fel 339894)
- Rätta problem med ordräkning i Kate genom att lyssna på DocumentPrivate istället för Document (fel 353258)
- Uppdatera syntaxfärgläggning för Kconfig: lägg till nya operatorer från Linux 4.2
- Synkronisera med KDE/4.14 kate-gren
- minimap: Rätta att rullningslistens grepp inte ritas med rullningsmarkeringar av (fel 352641).
- syntax: Lägg till alternativet git-user i kdesrc-buildrc

### Ramverket KWallet

- Stäng inte längre automatiskt vid sista användning

### KWidgetsAddons

- Rätta varning C4138 (MSVC): '*/' hittades utanför kommentar

### KWindowSystem

- Utför djup kopiering av QByteArray get_stringlist_reply
- Tillåt interaktion med flera X-servrar i NETWM-klasserna.
- [xcb] Ta hänsyn till ändringar i KKeyServer initierade på plattformar != x11
- Ändra KKeyserver (x11) för att kategorisera loggning

### KXMLGUI

- Gör det möjligt att importera och exportera genvägsscheman symmetriskt

### NetworkManagerQt

- Rätta introspektion, LastSeen ska vara i AccessPoint och inte iActiveConnection

### Plasma ramverk

- Låt dialogrutan för verktygstips döljas när pekaren flyttas in i inaktiv ToolTipArea
- Om skrivbordsfilen har Icon=/foo.svgz använd då den filen från paketet
- Lägg till filtypen "screenshot" i paket
- Ta hänsyn till devicepixelration i fristående rullningslist
- Ingen effekt när pekaren hålls över på pekskärm och mobiltelefon
- Använd marginaler från radeditor SVG vid beräkning av sizeHint
- Tona inte bor animerad ikon i Plasma-verktygstips
- Rätta avkortad knapptext
- Miniprogrammens sammanhangsberoende menyer inne i en panel överlappar inte längre med miniprogrammet
- Förenkla att hämta lista över associerade program i AssociatedApplicationManager

### Sonnet

- Rätta hunspell-insticksprogrammets identifierare för riktig laddning
- Stöd statisk kompilering på Windows, lägg till Windows libreoffice sökväg till hunspell-ordlista
- Anta inte att Hunspell-ordlistor är kodade med UTF-8 (fel 353133).
- Rätta Highlighter::setCurrentLanguage() i fallet när föregående språk var ogiltigt (fel 349151)
- Stöd /usr/share/hunspell som plats för ordlista
- NSSpellChecker-baserat insticksprogram

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
