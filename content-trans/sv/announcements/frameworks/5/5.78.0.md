---
aliases:
- ../../kde-frameworks-5.78.0
date: 2021-01-09
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Ta hänsyn till jobbet som omedelbart avbryts (fel 429939)

### Baloo

* [ExtractorProcess] Flytta DBus-signal från hjälpprocess till huvudprocess
* [timeline] Konsolidera kod för rotkatalog stat och list
* Gör UDS-poster för I/O-slav på toppnivå skrivskyddade
* Undvik fel för programstart om inget Baloo-index någonsin skapats
* [BasicIndexingJob] Ta bort snedstreck sist i kataloger (fel 430273)

### Breeze-ikoner

* Ny kompassåtgärdsikon
* Lägg ikon för saknad bild i tema
* Lägg till ikon WIM-bilder

### Extra CMake-moduler

* Tala om för MSVC att våra källkodsfiler är kodade med UTF-8
* Lägg till Findepoxy.cmake
* Ta hänsyn till lokala fastlane-bildtillgångar
* Reproducerbara komprimerade tar-arkiv bara med GNU tar
* Bevara rich-text delmängden som stöds av F-Droid
* Öka version av cmake som krävs för Android cmake (fel 424392)
* Detektera automatiskt beroenden av insticksprogrambibliotek på Android
* Kontrollera om filen finns innan borttagning av fastlane-arkiv
* Rensa bildkatalog och arkivfil innan de laddas ner eller genereras
* Behåll skärmbildsordning från appstream-filen
* Windows: rätta QT_PLUGIN_PATH för tester
* Misslyckas inte om vi inte hittar några kategorier
* Gör så att KDEPackageAppTemplates skapar en upprepningsbart komprimerat tar-arkiv

### KActivitiesStats

* Ta bort felaktig lastQuery funktion, rättar att Kör program kraschar för mig

### KCalendarCore

* CMakeLists.txt - öka minsta version av libical till 3.0

### KCMUtils

* KPluginSelector implementera förvald färgläggningsindikering
* kcmoduleqml: lås inte kolumnbredden till vybredden (fel 428727)

### KCompletion

* [KComboBox] rätta krasch när setEditable(false) anropas med en öppen sammanhangsberoende meny

### KConfig

* Rätta att fönster maximeras på olämpligt sätt vid start (fel 426813)
* Rätta format på fönstermaximeringssträng
* Rätta ändring av fönsterstorlek och position på Windows (fel 429943)

### KConfigWidgets

* KCodecAction: lägg till icke-överlagrade signaler codecTriggered & encodingProberTriggered

### KCoreAddons

* Konvertera KJobTrackerInterface till Qt5-anslutningssyntax
* KTextToHtml: rätta assert på grund av at()  anrop utanför gränser
* Använd flat hierarki för sökvägar till insticksprogram på Android
* skrivbord till JSON konvertering: ignorera posten "Actions="
* Avråd från KProcess::pid()
* ktexttohtml: rätta användning av KTextToHTMLHelper

### KCrash

* Använd std::unique_ptr<char[]> för att förhindra minnesläckor

### KDeclarative

* Byt till Findepoxy som tillhandahålls av ECM
* KCMShell: Lägg till stöd för att skicka argument
* Provisorisk lösning för krasch med GL-detektering och kwin_wayland
* [KQuickAddons] QtQuickSettings::checkBackend() för återgång till programvarugränssnitt (fel 346519)
* [abstractkcm] Rätta importversion i kodexempel
* Undvik att ställa in QSG_RENDER_LOOP om den redan är inställd
* ConfigPropertyMap : ladda egenskapens förvalda värde i avbilden

### KDocTools

* Lägg till en post för akronymen MathML
* Ändra 'Naval Battle' till 'KNavalBattle' för att uppfylla legala krav

### KGlobalAccel

* Undvik att starta kglobalaccel automatiskt vid avstängning (fel 429415)

### KHolidays

* Uppdatera japanska helger

### KIconThemes

* Hoppa över varning för några Adwaita ikoner för bakåtkompatibilitet
* QSvgRenderer::setAspectRatioMode() introducerades i Qt 5.15

### KImageFormats

* Lägg till AVIF i listan över format som stöds
* Lägg till insticksprogram för AV1-avbildsfilformat (AVIF)

### KIO

* [KFileItemDelegate] slösa inte utrymme för icke-existerande ikoner i kolumner andra än de första
* KFilePlacesView, KDirOperator: konvertera till asynkront askUserDelete()
* Arbeta om sättet som CopyJob söker efter JobUiDelegate utökningar
* Introducera AskUserActionInterface, ett asynkront programmeringsgränssnitt för dialogrutor för namnbyte eller överhoppning
* RenameDialog: anropa bara compareFiles() för filer
* kcm/webshortcuts: Rätta återställningsknapp
* KUrlNavigatorMenu: rätta hantering av mittenklick
* Ta bort objektet knetattach från vyn för I/O-slave remote:// (fel 430211)
* CopyJob: konvertera till AskUserActionInterface
* Jobs: lägg till icke-överlagrad signal "mimeTypeFound" för att avråda från "mimetype"
* RenameDialog: Lägg till saknad initiering av nullptr (fel 430374)
* KShortUriFilter: filtrera inte "../" och co. strängar
* Undvik assert om KIO::rawErrorDetail() anges som en webbadress utan något schema (fel 393496)
* KFileItemActions: rätta villkor, vi vill bara undanta fjärrkataloger (fel 430293)
* KUrlNavigator: ta bort användning av kurisearchfilter
* KUrlNavigator: gör så att kompletteringar av relativa sökvägar fungerar (fel 319700)
* KUrlNavigator: lös upp relativa katalogsökvägar (fel 319700)
* Tysta varningar orsakade av Samba inställningsproblem när Samba inte används explicit
* KFileWidget: tillåt att filer som börjar med ':' kan markeras (fel 322837)
* [KFileWidget] rätta bokmärkesknappens position i verktygsraden
* KDirOperator: avråd från mkdir(const QString &, bool)
* KFilePlacesView: tillåt inställning av en statisk ikonstorlek (fel 182089)
* KFileItemActions: lägg till nya metoder för att infoga åtgärden openwith (fel 423765)

### Kirigami

* [controls/SwipeListItem]: Visa alltid normalt åtgärder på skrivbordet
* [overlaysheet] Använd mer villkorlig positionering för stängningsknapp (fel 430581)
* [controls/avatar]: Öppna upp intern AvatarPrivate som det öppna programmeringsgränssnittet NameUtils
* [controls/avatar]: exponera genererad färg
* Lägg till komponenten Hero
* [controls/Card]: Ta bort animering när muspekaren hålls över
* [controls/ListItem]: Ta bort animering när muspekaren hålls över
* Flytta ListItems till att använda veryShortDuration när muspekaren hålls över istället för longDuration
* [controls/Units]: Lägg till veryShortDuration
* färgläggning av ikonen ActionButton
* använd bara så stora ikon bildpunktsavbildningar som behövs
* [controls/avatar]: bättre standardutseende
* [controls/avatar]: Rätta visuella fel
* Skapa komponenten CheckableListItem
* [controls/avatar]: skala kant enligt avatarstorlek
* Återställ "[Avatar] Ändra bakgrundstoning"
* Återställ "[Avatar] Ändra kantbredd till 1 bildpunkt för att motsvara andra kantbredder"
* [controls/avatar]: Gör avatar handikappvänlig
* [controls/avatar]: Öka vaddering för reservikon
* [controls/avatar]: Låt bildläge ange sourceSize
* [controls/avatar]: Justera storleksändring av text
* [controls/avatar]: Justera tekniker använda för cirkulär form
* [controls/avatar]: Lägg till primär och sekundär åtgärd till avatar
* Hårdkoda vaddering för OverlaySheet rubrikobjekt
* qmake build: lägg till saknad källkod/deklarationsfil sizegroup
* Färgikoner, inte knappar (fel 429972)
* Rätta att bakåt- och framåtknappar inte har någon bredd i rubrik
* [BannerImage]: rätta ej vertikalt centrerad rubriktitel för icke-plasmateman

### KItemModels

* Lägg till egenskapen count, tillåter att koppla rowCount i QML

### KItemViews

* KWidgetItemDelegate tillåt att utlösa en resetModel från KPluginSelector

### KNewStuff

* Avråd från metoderna standardAction och standardActionUpload
* Rätta QtQuick-modell om det bara finns en nyttolast, men inga nerladdningslänkar
* Lägg till en dptr i Cache, och flytta strypningstidtagningen dit för att rätta krasch (fel 429442)
* Omstrukturera KNS3::Button att använda den nya dialogrutan internt
* Skapa omgivande klass för QML-dialogruta
* Kontrollera om version är tom innan version konkateneras

### KNotification

* Förbättra dokumentation av programmeringsgränssnitt för KNotification

### KParts

* Avråd från BrowserHostExtension

### KQuickCharts

* Använd ett eget makro för meddelanden som avråds från i QML
* Använd ECMGenerateExportHeader för makron som avråds från och använd dem
* Ändring av intervall behöver inte rensa historik
* Ändra kontinuerligt linjediagram till exempel på historikproxykälla
* Avråd från Model/ValueHistorySource
* Introducera HistoryProxySource som en ersättning av Model/ValueHistorySource
* Lägg till loggningskategorier för diagram och använd dem för befintliga varningar

### Kör program

* [DBus Runner] Lägg till stöd för egna bildpunktsavbildade ikoner för resultat
* Lägg till nyckel för att kontrollera om inställningen har konverterats
* Separera inställnings- och datafiler
* Nytt programmeringsgränssnitt för att utföra matchningar och för historik
* Bygg inte RunnerContextTest på Windows

### KService

* KSycoca: undvik ombyggnad av databas om XDG_CONFIG_DIRS innehåller dubbletter
* KSycoca: säkerställ att extra filer ordnas för jämförelse (fel 429593)

### KTextEditor

* Byt namn på "Variable:" till "Document:Variable:"
* Variabelexpansion: Rätta att hitta prefixmatchningar med flera kolon
* Flytta uppritning från KateTextPreview till KateRenderer
* Säkerställ att bara rader i vyn används för att rita bildpunktsavbildningen
* Använd KateTextPreview för att återge bildpunktsavbildningen
* Variabelexpansion: Lägg till stöd för %{Document:Variable:<namn>}
* Visa den dragna texten när den dras (fel 398719)
* Rätta bortkoppling i TextRange::fixLookup()
* Rita inte bakgrund för aktuell rad om det finns en överlappande markering
* KateRegExpSearch: rätta logik när '\n' läggs till mellan intervallrader
* byt namn på åtgärd till 'Swap with clipboard contents'
* lägg till en åtgärd för att utlösa kopiera och klistra in som en åtgärd
* feat: lägg till radbrytningsåtgärdsikon för dynamisk radbrytning
* Ångra indentering i ett steg (fel 373009)

### KWidgetsAddons

* KSelectAction: lägg till icke-överlagrade signaler indexTriggered och textTriggered
* KFontChooserDialog: hantera att dialogrutan tas bort av överliggande objekt under exec()
* KMessageDialog: anropa setFocus() för förvalsknappen
* Konvertera från QLocale::Norwegian till QLocale::NorwegianBokmal
* Konvertera KToolBarPopupActionTest till QToolButton::ToolButtonPopupMode

### KXMLGUI

* KXmlGui: när en lokal .rc-fil laddas upp, behåll nya programverktygsrader
* Rätta nyckelinspelning av setWindow innan tagning börjar (fel 430388)
* Ta bort oanvänt beroende av KWindowSystem
* Rensa KXMLGUIClient i xml-dokument i minnet efter att ha sparat genvägar på disk

### Oxygen-ikoner

* Lägg till uppindikator

### Plasma ramverk

* Exponera felinformation till Plasmoid på ett mer strukturerat sätt
* [components] Koppla upp Mnemonics
* [svg] Starta alltid SvgRectsCache tidtagare från rätt tråd
* [PC3 ProgressBar] Ställ in bindning för bredd (fel 430544)
* rätta bygge på Windows + invertering av variabler
* [PlasmaComponents/TabGroup] Rätta kontroll om objekt ärver Page
* Konvertera diverse komponenter till veryShortDuration när musen hålls över
* Flytta ListItems till att använda veryShortDuration när muspekaren hålls över istället för longDuration
* Lägg till veryShortDuration
* Tillåt inte negativa kalenderår (fel 430320)
* Rätta felaktig bakgrund (fel 430390)
* Ersätt QString cache-identifierare med en struct-baserad version
* [TabGroup] Vänd animeringar i höger-till-vänster läge
* Ta bara bort genvägar när miniprogrammet tas bort inte destrueras
* Dölj inaktiverade sammanhangsberoende åtgärder från ExpandableListItem

### Syfte

* KFileItemActions: lägg till windowflag i meny
* Dela fileitemplugin: använd överliggande komponent som menyns överliggande objekt (fel 425997)

### QQC2StyleBridge

* Uppdatera org.kde.desktop/Dialog.qml
* Rita ScrollView genom att använda Frame istället för Edit (fel 429601)

### Sonnet

* Förbättra prestanda av createOrderedModel genom att använda QVector
* Undvik körtidsvarning om inget gissat resultat finns

### Syntaxfärgläggning

* C++ färgläggning: QOverload och co
* Rätta att label som börjar med punkt inte färgläggs i GAS
* C++ färgläggning: lägg till makrot qGuiApp
* Förbättra dracula-tema
* rättning nr. 5: Bash, Zsh: ! med if, while, until. Bash: mönsterstil för ${var,möns} och ${var^möns}
* rättning nr. 5: Bash, Zsh: kommentarer inne i fält
* Cucumber funktionssyntax
* Zsh: öka syntaxversion
* Zsh: rätta expansion av klammerparenteser i ett kommando
* lägg till weakDeliminator och additionalDeliminator med nyckelord, WordDetect, Int, Float, HlCOct och HlCHex
* Indexer: nollställ currentKeywords och currentContext när en ny definition öppnas
* Zsh: många rättningar och förbättringar
* Bash: rätta kommentarer i case, sekvensuttryck och ) efter ]
* Hantera importera färg i bas riktigt och specialisera för C/C++
* Uppdatera Monokai-temat
* Verifiera riktighet/närvaro av egna stilar i teman
* Lägg till mörkt/ljust GitHub tema
* Öka version, ändra inte kate-version tills jag förstår varför det behövs
* Lägg till licenser
* Lägg till mörkt/ljust Atom Ett tema
* missade att öka version vid ändring
* Rätta egenskaper för monokai och operatorfärg
* Lägg till temat Monokai
* CMake: Lägg till saknade 3.19 variabler och några nya tillagda i 3.19.2
* Kotlin: rätta några problem och andra förbättringar
* Groovy: rätta några problem och andra förbättringar
* Scala: rätta några problem och andra förbättringar
* Java: rätta några problem och andra förbättringar
* rätta && och || i ett delsammanhang och rätta funktionsnamnsmönster
* lägg till QRegularExpression::DontCaptureOption när det inte finns någon dynamisk regel
* Bash: lägg till (...), ||, && i [[ ... ]] ; lägg till grav accent i [ ... ] and [[ ... ]]

### Säkerhetsinformation

Den utgivna koden har signerats med GPG genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Fingeravtryck för primär nyckel: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
