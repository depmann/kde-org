---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE levererar KDE-program 18.08.3
layout: application
title: KDE levererar KDE-program 18.08.3
version: 18.08.3
---
8:e november, 2018. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../18.08.0'>KDE-program 18.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring 20 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Ark, Dolphin, KDE-spel, Kate, Okular och Umbrello.

Förbättringar omfattar:

- HTML-visningsläget i Kmail blir ihågkommet, och läser in externa bilder igen om det tillåts
- Kate kommer nu ihåg metainformation (inklusive bokmärken) mellan redigeringssessioner
- Automatisk rullning i textanvändargränssnittet i Telepathy har rättats för nyare versioner av QtWebEngine
