---
aliases:
- ../announce-applications-15.04.0
changelog: true
date: '2015-04-15'
description: KDE Ships Applications 15.04.
layout: application
title: تطلق كدي تطبيقات كدي 15.04.0
version: 15.04.0
---
April 15, 2015. Today KDE released KDE Applications 15.04. With this release a total of 72 applications have been ported to <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. The team is striving to bring the best quality to your desktop and these applications. So we're counting on you to send your feedback.

With this release there are several new additions to the KDE Frameworks 5-based applications list, including <a href='https://www.kde.org/applications/education/khangman/'>KHangMan</a>, <a href='https://www.kde.org/applications/education/rocs/'>Rocs</a>, <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, <a href='https://www.kde.org/applications/development/kompare'>Kompare</a>, <a href='https://kdenlive.org/'>Kdenlive</a>, <a href='https://userbase.kde.org/Telepathy'>KDE Telepathy</a> and <a href='https://games.kde.org/'>some KDE Games</a>.

Kdenlive is one of the best non-linear video editing software available. It recently finished its <a href='https://community.kde.org/Incubator'>incubation process</a> to become an official KDE project and was ported to KDE Frameworks 5. The team behind this masterpiece decided that Kdenlive should be released together with KDE Applications. Some new features are the autosaving function of new projects and a fixed clip stabilization.

كدي تلبثي هي أداة للتواصل الفوريّ. قد نُقل إلى أُطر عمل كدي 5 وكيوت 5 وهو الآن عضو جديد في إصدارات تطبيقات كدي. أغلب التطبيق مكتمل ما عدى واجهة المستخدم للمهاتفة الصوتية والمرئية فهي ما زالت ناقصة.

تستخدم كدي (متى ما كان ذلك ممكنًا) التّقنيّات الموجودة كما حدث مع «حساباتك» المستخدمة الآن في نظام تشغيل سيلفش و"يونتي" لِـ"كانيوكل". في داخل تطبيقات كدي تستخدمها كدي تلبثي الآن فقط. لكن في المستقبل ستستخدم تطبيقات أكثر المكتبة ك«تراسلك» وأكونادي.

In the <a href='https://edu.kde.org/'>KDE Education module</a>, Cantor got some new features around its Python support: a new Python 3 backend and new Get Hot New Stuff categories. Rocs has been turned upside down: the graph theory core has been rewritten, data structure separation removed and a more general graph document as central graph entity has been introduced as well as a major revisit of the scripting API for graph algorithms which now provides only one unified API. KHangMan was ported to QtQuick and given a fresh coat of paint in the process. And Kanagram received a new 2-player mode and the letters are now clickable buttons and can be typed like before.

Besides the usual bug fixes <a href='https://www.kde.org/applications/development/umbrello/'>Umbrello</a> got some usability and stability improvements this time. Furthermore the Find function can now be limited by category: class, interface, package, operations, or attributes.
