---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDEk, Aplikazioak 19.08 kaleratu du.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDEk, Aplikazioak 19.08.3 kaleratu du
version: 19.08.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../19.08.0'>KDE Applications 19.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle, Umbrello, among others.

Improvements include:

- In the video-editor Kdenlive, compositions no longer disappear when reopening a project with locked tracks
- Okular's annotation view now shows creation times in local time zone instead of UTC
- Keyboard control has been improved in the Spectacle screenshot utility
