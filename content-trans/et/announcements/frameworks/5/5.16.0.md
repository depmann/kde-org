---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Monitor'i teek: Kformat::spelloutDuration'i kasutamine ajastringi lokaliseerimiseks
- KDE_INSTALL_DBUSINTERFACEDIR'i kasutamine dbusi liideste paigaldamiseks
- UnindexedFileIndexer: failide käitlemine, mis eemaldati ajal, mil baloo_file ei töötanud
- Transaction::renameFilePath'i eemaldamine ja DocumentOperation'i lisamine selle asemele.
- Ühe parameetriga konstruktorite muutmine ilmutatuks
- UnindexedFileIndexer: ainult faili nõutavate osade indekseerimine
- Transaction: meetodi lisamine ajateabe struct'i tagastamiseks
- Välja jäetavate MIME tüüpide lisamine balooctl'i seadistusele
- Andmebaasid: QByteArray::fromRawData kasutamine andmete edastamisel koodekile
- Balooctl: käsu 'status' liigutamine omaette klassi
- Balooctl: abimenüü näitamine, kui käsku ei tunta ära
- Balooshow: failide otsimise lubamine inode ja devId järgi
- Balooctl jälgija: peatamine, kui baloo sureb
- MonitorCommand: nii started kui ka finished signaalide kasutamine
- Balooctl jälgija: liigutamine sobivasse käsuklassi
- dbusi märguande lisamine faili indekseerimise alustamisel/lõpetamisel
- FileIndexScheduler: lõimede jõuga tapmine väljumisel
- WriteTransaction commit: positionList'i tõmbamise vältimine, kui seda just ei nõuta
- WriteTransaction: lisaeeldused replaceDocument'is

### BluezQt

- isBluetoothOperational sõltub nüüd ka blokkimata rfkill'ist
- rfkill'i lüliti globaalse oleku kindlakstegemise parandus
- QML API: märguandesignaalita omaduste märkimine konstandiks

### CMake'i lisamoodulid

- Hoiatus tõrke asemel, kui ecm_install_icons ei leia ikoone (veateade 354610).
- võimaldamine ehitada macOS'is KDE Frameworks 5 puhta qt 5.5 peale, mis on paigaldatud normaalse qt.io paigaldajaga
- Puhvri muutujate tühistamise välistamine KDEInstallDirs'is (veateade 342717)

### Raamistike lõimimine

- WheelScrollLines'i vaikeväärtuse määramine
- WheelScrollLines'i seadistuste parandus Qt &gt;= 5.5 korral (veateade 291144)
- Plasma 5.5 lülitamine Noto fondi peale

### KActivities

- Ehitamise parandus Qt 5.3 peal
- Kaasatu boost.optional liigutamine asukohta, kus seda vaja läheb
- boost.optional'i asendamine kontinuatsioonides vähemnõudlikuma optional_view struktuuriga
- Seotud tulemuste kohandatud järjekorra toetuse lisamine
- QML-il lubatakse välja kutsuda tegevuste juhtimismoodul
- Tegevuse kustutamise toetuse lisamine tegevuste juhtimismoodulisse
- Uus tegevuste seadistamise kasutajaliides
- Uus seadistamise kasutajaliides, mis toetab kirjelduse ja taustapildi lisamist
- Seadistuste kasutajaliides on nüüd korralikult mooduliteks jagatud

### KArchive

- KArchive'i parandus käitumismuudatuse tõttu Qt 5.6-s
- Mälulekete parandamine, väiksem mälukasutus

### KAuth

- qInfo teadete puhverfamise käitlemine
- Abilist käivitanud asünkroonse lõpetamise järel ootamine enne vastuse kontrollimist (veateade 345234)
- Muutuja nime parandamine, vastasel juhul ei saagi kaasatu toimida

### KConfig

- ecm_create_qm_loader'i kasutuse parandus.
- Kaasatu muutuja parandus
- Use KDE*INSTALL_FULL* variant, so there is no ambiguity
- KConfig'il on lubatud kasutada ressursse tagavaraseadistusfailidena

### KConfigWidgets

- KConfigWidgetsi muutmine autonoomseks, ühe globaalse faili sidumine ressursiga
- doctools ei ole enam kohustuslik

### KCoreAddons

- KAboutData: apidoc "is is" -&gt; "is" addCredit(): ocsUserName -&gt; ocsUsername
- KJob::kill(Quiet) peab samuti väljuma sündmussilmusest
- Töölauafaili nime toetuse lisamine KAboutData's
- Korrektse paomärgi kasutamine
- Omistamiste mõningane vähendamine
- KAboutData::translators/setTranslators muutmine lihtsaks
- setTranslator näidiskoodi parandus
- desktopparser: Encoding= key ei arvestata
- desktopfileparser: Address review comments
- Teenusetüüpide määramise lubamine kcoreaddons_desktop_to_json()-is
- desktopparser: Fix parsing of double and bool values
- KPluginMetaData::fromDesktopFile() lisamine
- desktopparser: suhteliste asukohtade edastamise lubamine teenusetüübi failidele
- desktopparser: kategoriseeritud logimine suurem kasutamine
- QCommandLineParser kasutab võtit -v võtme --version alternatiivina, niisiis kasutame --verbose
- Hulga topeltkoodi eemaldamist desktop{tojson,fileparser}.cpp's
- ServiceType-failide parsimine .desktop-failide lugemisel
- SharedMimeInfo ei ole enam kohustuslik nõue
- QString::squeeze() väljakutse eemaldamine
- desktopparser: tarbetu utf8 dekodeerimise vältimine
- desktopparser: uut kirjet ei lisata, kui kirje lõpeb eraldajaga
- KPluginMetaData: hoiatus, kui loendikirje ei ole JSON-loend
- mimeTypes() lisamine KPluginMetaData'sse

### KCrash

- drkonqui otsingu täiustamine ja selle hoidmine vaikimisi vaiksena, kui ei leita

### KDeclarative

- ConfigPropertyMap suudab nüüd pärida muudetamatute seadistusvalikute järele meetodiga isImmutable(key)
- Unbox QJSValue in config property map
- EventGenerator: Add support for sending wheel events
- fix lost QuickViewSharedEngine initialSize on initializing.
- QuickViewSharedEngine'i kriitilise tagasilanguse parandus sissekandega 3792923639b1c480fd622f7d4d31f6f888c925b9
- make pre-specified view size precede initial object size in QuickViewSharedEngine

### KDED

- doctools ei ole enam kohustuslik

### KDELibs 4 toetus

- Don't try to store a QDateTime in mmap'ed memory
- Sync and adopt uriencode.cmake from kdoctools.

### KDesignerPlugin

- KCollapsibleGroupBox'i lisamine

### KDocTools

- pt_BR olemite uuendamine

### KGlobalAccel

- Do not XOR Shift for KP_Enter (bug 128982)
- Grab all keys for a symbol (bug 351198)
- Do not fetch keysyms twice for every keypress

### KHTML

- Printimise parandamine KHTMLPar'ist korrektselt printSetting'i eellast määrates

### KIconThemes

- kiconthemes toetab nüüd teemasid, mis on põimitud qt ressurssidesse prefiksiga :/icons , nagu teeb seda Qt ise QIcon::fromTheme puhul
- Puuduvate nõutud sõltuvuste lisamine

### KImageFormats

- image/vnd.adobe.photoshop tuvastamine image/x-psd asemel
- d7f457aosaline tühistamine krahhi vältimiseks rakendusest väljumisel

### KInit

- doctools ei ole enam kohustuslik

### KIO

- Puhverserveri URL-i salvestamine korrektse skeemiga
- "Uue faili mallide" pakkumine kiofilewidgets'i teegis qrc abil (veateade 353642)
- Keskklõpsu kohane käitlemine liikumismenüüs
- kio_http_cache_cleaner'i pakkumise võimaldamine rakenduste paigaldisprogrammis/kimbus
- KOpenWithDialog: puuduva MIME tüübiga töölauafaili loomise parandus
- Protokolliteabe lugemine plugina metaandmetest
- Kohaliku KIO-mooduli kasutamise lubamine
- .protocol'i lisamine JSON-i teisendatuna
- Tulemuse topeltväljastamise parandus ja puuduva hoiatuse lisamine, kui loendi koostamisel jõutakse juurdepääsuta kataloogini (veateade 333436)
- Suhteliste linkide sihtmärkide säilitamine nimeviitade kopeerimisel (veateade 352927)
- Sobivate ikoonide kasutamine kasutaja kodukataloogi vaikimisi kataloogidel (veateade 352498)
- Liidese lisamine, mis võimaldab pluginal näidata kohandatud ülekattegia ikoone
- KNotifications ei ole enam KIO (kpac) kohustuslik sõltuvus
- doctools + wallet ei ole enam kohustuslikud
- kio krahhi vältimine, kui dbus'i server ei tööta
- KUriFilterSearchProviderActions'i lisamine näitamaks toimingute loendit teksti otsimisel veebikiirklahvide abil
- "Loo uus" menüü kirjete liigutamine kde-baseapps/lib/konq'ist kio'sse (veateade 349654)
- konqpopupmenuplugin.desktop'i liigutamine kde-baseapps'ist kio'sse (veateade 350769)

### KJS

- MSVC korral globaalse muutuja "_timezone" kasutamine "timezone" asemel. Parandab ehitamise MSVC 2015 peal.

### KNewStuff

- KDE partitsioonihalduri .desktop-faili ja kodulehekülje URL-i parandus

### KNotification

- Nüüd, mil kparts ei vaja enam knotifications'i, on ainukesed asjad, mis tõesti vajavad märguandeid, selle raamistiku nõuded
- speech + phononi kirjelduse ja ülesande lisamine
- Phononi sõltuvus ei ole enam kohustuslik, see on puhtalt sisemine muudatus, nagu see on ka speech'i puhul.

### KParts

- deleteLater kasutamine Part::slotWidgetDestroyed()-s.
- KNotifications'i sõltuvuse eemaldamine KParts'ist
- Funktsiooni kasutamine ui_standards.rc asukoha päringuks selle otse koodi kirjutamise asemel, mis võimaldab kasutada ressusside tagavaravariante

### KRunner

- RunnerManager: plugina laadimise koodi lihtsustamine

### KService

- KBuildSycoca: alati salvestatakse, isegi kui .desktop-failis ei täheldatud ühtegi muutust. (veateade 353203)
- doctools ei ole enam kohustuslik
- kbuildsycoca: kõigi uues spetsifikatsioonis mainitud failide mimeapps.list parsimine.
- Alamkataloogi suurima ajatempli kasutamine ressursikataloogi ajatemplina.
- MIME tüüpide hoidmine lahus KPluginInfo teisendamisel KPluginMetaData'ks

### KTextEditor

- esiletõstmine: gnuplot: .plt laienduse lisamine
- valideerimisvihje parandus tänu Thomas Jarosch'ile &lt;thomas.jarosch@intra2net.com&gt; ning vihje lisamine ka kompileerimisaja valideerimise kohta
- Krahhi vältimine, kui käsku pole saadaval.
- Veateate #307107 parandus
- Haskelli esiletõstmise muutujad algavad märgiga _
- git2 init'i lihtsustamine, kuna me nõuame nagunii üsna uut versiooni (veateade 353947)
- ressursi vaikeseadistuste pakkumine kimbuna
- süntaksi esiletõstmine (d-g): vaikestiilide kasutamine koodi kirjutatud värvide asemel
- parem skriptide otsing: algul kasutaja kohaliku kraami seas, siis meie ressursside seas, siis kõikjal mujal, mis annab kasutajale võimaluse kirjutada meie pakutavad skriptid üle kohalikega
- kogu js kraami pakkimine ressurssidesse, ainult 3 seadistusfaili on veel puudu ja ktexteditor'i saab kasutada lihtsalt teegina ilma ühegi kimpu seotud failita
- uus katse: kõiki kimpu seotud xml süntaksifailide liigutamine ressurssi
- sisestusrežiimi lülitamise kiirklahvi lisamine (veateade 347769)
- xml-failide sidumine ressursis kimpu
- süntaksi esiletõstmine (a-c): üleminek uutele vaikestiilidele, koodi kirjutatud värvide eemaldamine
- süntaksi esiletõstmine: koodi kirjutatud värvide eemaldamine ja selle asemel vaikestiilide kasutamine
- süntaksi esiletõstmine: uute vaikestiilide kasutamine (koodi kirjutatud värvide eemaldamine)
- Vaikestiili parem import
- Valiku "Salvesta kodeeringuga" pakkumine faili salvestamiseks mõnes teises kooditabelis kenasti järjestatud kodeeringumenüü alusel ning kõigi salvestamisdialoogide asendamine operatsioonisüsteemi korrektsete dialoogidega ilma seda tähtsat valikut kaotamata.
- ui-faili pakkimine kimbuna teeki minu xmlgui laienduse abil
- Trükkimisel arvestatakse taas valitud fonti ja värviskeemi (veateade 344 976)
- Breeze'i värvide kasutamine salvestatud ja muudetud ridadel
- Skeemi "Tavaline" ikoonipiirde vaikimisi värvide täiustamine
- autobrace: sulu lisamine aimult siis, kui järgmine märk on tühi või ei ole täht ega number
- autobrace: alustava sulu eemaldamisel Backspace-klahviga eemaldatakse ka lõpetav sulg
- autobrace: ühendus luuakse ainult ühel korral
- Autobrace: lõpetavate sulgude ärasöömine teatavate tingimuste korral
- shortcutoverride'i mainwindow'le edastamata jätmise parandus
- Veateade 342659 - vaikimisi "sulgude esiletõstmise" värv on halvasti näha (tavalise skeemi parandus) (veateade 342659)
- Sobivate vaikevärvide lisamine "Aktiivse rea numbri" värvi
- sulgude sobitamine ja automaatsulud: koodi jagamine
- sulgude sobitamine: kaitse negatiivse maxLines'i eest
- sulgude sobitamine: ainult see, et uus vahemik vastab vanale, ei põhjusta veel uuendamise nõudmist
- Pooltühimärgi laiuse lisamine kursori näitamiseks reavahetuse märgi juures
- Mõne HiDPI problemi parandus ikoonipiiretel
- veateate #310712 parandus: lõputühikute eemaldamine ka kursoriga real (veateade 310712)
- vi sisestusrežiimis ainult teate "märk määratud" näitamine
- &amp; eemaldamine nupu tekstist (veateade 345937)
- aktiivse reanumbri värvi uuendamise parandus (veateade 340363)
- sulgude lisamise võimaldamine sulu kirjutamisel valiku peale (veateade 350317)
- automaatsulud (veateade 350317)
- häire esiletõstmise parandus (veateade 344442)
- veergude kerimise puudumine dünaamilise reamurdmise lubamisel
- meeldejätmine, kas kasutaja on määranud esiletṍstmise püsimise seanssides, et mitte kaotada seda salvestamisel pärast taastamist (veateade 332605)
- tex'i kokkukerimise parandus (veateade 328348)
- veateate #327842 parandus: C-stiilis kommentaari lõpp tuvastati valesti (veateade 327842)
- dünaamilise reamurdmise salvestamine/taastamine seansi salvestamisel/taastamisel (veateade 284250)

### KTextWidgets

- Uue alammenüü lisamine KTextEdit'ile õigekirja kontrollimise keelte vahel lülitumiseks
- Sonneti vaikeseadistuste laadimise parandus

### KWalleti raamistik

- KDE_INSTALL_DBUSINTERFACEDIR'i kasutamine dbusi liideste paigaldamiseks
- KWalleti seadistusfaili hoiatuste parandus sisselogimisel (veateade 351805)
- Kwalleti pam'i väljundi korrektne varustamine prefiksiga

### KWidgetsAddons

- Kokkukeritava konteinerividina KCollapsibleGroupBox lisamine
- KNewPasswordWidget: puuduv värvu initsialiseerimine
- KNewPasswordWidget'i lisamine

### KXMLGUI

- kmainwindow: võimaluse korral tõlkija teabe eelnev täitmine (veateade 345320).
- Kontekstimenüü võtme (all paremal) seostamise lubamine kiirklahvidega (veateade 165542)
- Funktsiooni lisamine standardse xml-faili asukoha päringuks
- Lubamine kxmlgui raamistikku kasutada ka ilma ühegi paigaldatud failita
- Puuduvate nõutud sõltuvuste lisamine

### Plasma raamistik

- Kaardiriba esmaloomisel kirjete üksteise otsa kuhjumise (mis tekkis näiteks Kickoffi menüüs Plasma käivitamise järel) parandus
- Failide lohistamisel töölauale/paneelile võimalike toimingute pakkumata jätmise parandus
- QApplication::wheelScrollLines'i arvestamine ScrollView's
- BypassWindowManagerHint'i kasutamine ainult X11 platvormil
- vana paneeli tausta kustutamine
- loetavamad spinnerid väikese suuruse korral
- värviline vaateajalugu
- kalender: kogu päiseala muutmine klõpsatavaks
- kalender: tänase päeva numbrit ei kasutata enam goToMonth'is
- kalender: 10 päeva ülevaate uuendamise parandus
- Breeze'i ikoonid teemapõhiseks laadimise korral IconItemi kaudu
- Nupu omaduse minimumWidth parandus (veateade 353584)
- Signaali appletCreated lisamine
- Plasma Breeze'i ikoon: SVG ID elementide lisamine puutepadjale
- Plasma Breeze'i ikoon puutepadja suuruseks sai 22x22px
- Breeze'i ikoon: vidinaikooni lisamine märkmetele
- Skript koodi sisse kirjutatud värvide asendamiseks laaditabeliga
- SkipTaskbar'i lisamine ExposeEvent'i
- SkipTaskbar'i ei määrata kõigi sündmuste korral

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
