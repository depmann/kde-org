---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDE Ships Applications 19.08.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE Ships Applications 19.08.3
version: 19.08.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../19.08.0'>KDE Applications 19.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Üle tosina  teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Arki, Cantori, K3b, Kdenlive'i, Konsooli, Okulari, Spectacle'i ja Umbrello täiustused.

Täiustused sisaldavad muu hulgas:

- Videoredaktoris Kdenlive ei kao kompositsioonid enam, kui avada uuesti lukustatud radadega projekt
- Okulari annotatsioonivaade näitab nüüd loomisaega kohaliku ajavööndi, mitte UTC järgi
- Ekraanipiltide tegemise tööriista Spectacle juhtimist klaviatuuri abil on täiustatud
