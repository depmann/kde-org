---
title: Thank You
layout: thanks
donations:

  - organization: Fosshost
    description: Donates virtual servers
    usage: We use a virtual server provided by Fosshost to run WebSVN and LXR, and another to host privacy-respecting analytics and statistics of our websites and software.
    startdate: 2020-10-31

  - organization: DigitalOcean
    description: Sponsors free credits on their cloud platform
    usage: We use a DigitalOcean droplet (virtual private server) to host one of our repository mirrors, and part of the KDE neon infrastructure.
    startdate: 2016-07-04

  - organization: Datacamp Limited (CDN77.com)
    description: Donation of Content Distribution Network services
    usage: CDN77 is used to globally distribute the common images, stylesheets, javascript and other elements which are used on numerous KDE websites, enhancing performance and improving access to our sites in the process.
    startdate: 2016-05-05

  - organization: stepping stone GmbH
    description: Donation of a virtual server (mason.kde.org).
    usage: Mason is used to run an Anonymous Git server, providing access to KDE Git repositories over git:// and http://
    startdate: 2013-12-02

  - organization: Ange Optimization
    description: Sponsors a hosted server (ange.kde.org).
    usage: Ange Optimization has sponsored a server hosted by Hetzner. This server was used as a build node for the KDE CI system prior to it being retired.
    startdate: 2013-09-30
    enddate: 2023-05-21

  - organization: Gärtner Datensysteme GmbH & Co. KG
    description: Donation and hosting of 3 virtual servers.
    usage: Gärtner has donated a series of virtual machines, which host Scripty and the generation process for docs.kde.org as well as our server monitoring system.
    startdate: 2013-05-06

  - organization: Dalhousie University & ACORN
    description: Donation and hosting of a server (dalca.kde.org).
    usage: Dalhousie University has donated dalca.kde.org and hosts it, with network connectivity provided by ACORN. This server is used as a build node for the KDE CI system.
    startdate: 2013-05-28

  - organization: Cloudflare
    description: Donation of CDN services
    usage: Cloudflare protects some of our sites against SQL-injections and XSS-scripting. It also provides a CDN for common images, javascript and stylesheets which are used on multiple sites to optimize performance.
    startdate: 2019-10-01

  - organization: ClouDNS
    description: Donation of DNS services
    usage: CloudDNS provides anycast nameservers for all our domains. Normal namesevers, DDOS-protected nameservers for our important domains and also GEO-ip-based nameservers for providing the right SCM-mirror in each continent.
    startdate: 2019-10-29

  - organization: Canonical
    description: Donation and hosting of a server (cano.kde.org)
    usage:  Canonical has donated cano.kde.org and takes care of the hosting. This server hosted a number of websites prior to its retirement.
    startdate: 2010-12-04
    enddate: 2023-06-06

  - organization: GNU/FSF
    description: Donation of a virtual server (bluemchen.kde.org)
    usage: This server is used for a variety of tasks, including handling all transactional email for KDE.org services.
    startdate: 2005-08-01

  - organization: OSUOSL
    description: Donation of a virtual server (stumptown.kde.org)
    usage: This server was used for a series of small websites prior to its retirement.
    startdate: 2005-11-28
    enddate: 2023-06-06

  - organization: Francesco Riosa
    description: Donation of a virtual server (zivo.kde.org)
    usage: This server was used to host our API documentation (api.kde.org) as well as the English Breakfast Network (ebn.kde.org) prior to it being retired.
    startdate: 2011-09-07
    enddate: 2023-06-06
---

