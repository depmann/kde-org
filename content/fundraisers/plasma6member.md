---
title: Support Plasma 6
layout: plasma6member
scssFiles:
- /scss/plasma6member.scss
more: "If you'd prefer a one-time donation, [click here](https://kde.org/fundraisers/yearend2022/)."
---

## Spark Innovation with Your Donation

Exciting news on the horizon! In February 2024, **Plasma 6** is set to make its grand debut. But we need your help to ensure its successful launch.

![](/fundraisers/plasma6member/Screenshot_20230921_060625.png)

## Why Donate

In short: because generosity is at the heart of KDE and keeps  our projects alive.

It's what motivates our volunteers to share their time and knowledge to keep building new features and keeping KDE software up-to-date and secure.

Generosity also drives our supporters. Their contributions and sponsorships keep KDE's gears turning and ensure that our developers can continue their fantastic work.

### How to Become a Supporter

Fill out the form above and **for less than €10 a month** you too can support the champions who tirelessly improve our software every day.The KDE development ecosystem is a bustling hive of activity, and every supporting member helps keep it buzzing.

If you'd prefer a one-time donation, [please click here](https://kde.org/fundraisers/yearend2022).

### Perks

We know that keeping KDE healthy and running is all the reward you need for your generosity, but, as a token of our appreciation for your support, we are also offering:

- To have your name shine bright on our donation page, forever acknowledging your contribution.
- To display your contribution in Plasma 6 itself by including your name in the Plasma welcome wizard.
- More perks coming soon!

If you don't want any of the above, that is fine too, of course. Remember to mark the **"[✔️] Make donation anonymous"** checkbox in the donation process above.

## How We Use the Money

This where your donation will make a difference:

* **Sprints for Developers**: You will help finance the in-person meetups that keep our developers energized and focused on making KDE even better.
* **Travel Costs to Events**: You will support our team's presence at important gatherings and conferences, like FOSDEM, FOSSAsia and LinuxCons.
* **Akademy Event**: You will ensure the success of KDE's yearly community event for all members, and foster collaboration and growth.
* **Running KDE**: You will keep the lights on at KDE HQ and our digital home running smoothly.
* **Paying Support Staff**: You will ensure KDE has on hand the experts we need to assist our contributors and users.

## Our Goal

Our aim is to sign on **500 new supporters** within the next 4 months **for before Plasma 6 launches in February**. With your support, we can achieve this milestone and ensure the continued success of KDE and the launch of Plasma 6.

Join us in making Plasma 6's launch a spectacular success. Contribute to our fundraiser. Your donation will fuel innovation, support dedicated developers, and ensure that KDE continues to shine brightly in the world of free open-source software. Together, we can achieve great things!

<!--
---

### OLD STUFF TO BE REMOVED

---


Your donation allows KDE to continue developing the spectacular [Plasma desktop](/plasma-desktop/) and [all the apps you need](https://apps.kde.org/) for education, productivity, and creative work.

![](/fundraisers/plasma6member/Screenshot_20230921_060625.png)

Your donation also helps pay for the servers hosting KDE's code, websites, and the [chat](https://go.kde.org/matrix/#/#kde-welcome:kde.org) and video conference channels that KDE contributors use to communicate with each other. In addition, it helps pay for development sprints and the yearly [Akademy](https://akademy.kde.org/) conference.

KDE wants to be able to offer users and community members better services and support. Your donation also helps KDE provide employment to the people who can help make that happen.

![KDE booth at an external event](/fundraisers/yearend2022/kdeconf.jpg)

Your donation benefits the rest of humanity as well! With it, KDE has the resources to promote a spectacular catalogue of Free Software to professionals, artists, software developers, children, public institutions, non-profits, and companies around the globe.

![Children playing with GCompris](/fundraisers/yearend2022/kdechildren.jpg)

Your donation also supports the Free Software movement itself. KDE can adopt and nurture new projects, helping them to reach maturity, and assisting new contributors to become part of the Free Software community with a career in ethical tech.

![KDE India contributors](/fundraisers/yearend2022/kdeindia.jpg)

And if you are a developer, you can benefit from KDE's [feature-rich tools and frameworks](https://develop.kde.org/). KDE's libraries make it quick and easy to build sophisticated apps for all platforms.

In the past year, the list of platforms expanded to include the [Steam Deck](https://www.steamdeck.com), a hand-held gaming device running Plasma and KDE apps!

![The steamdeck](/fundraisers/yearend2022/steamdeck.png)

You can see even more [here](/hardware).
-->
