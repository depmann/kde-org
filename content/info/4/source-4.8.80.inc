<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">SHA1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/analitza-4.8.80.tar.xz">analitza-4.8.80</a></td>
   <td align="right">138kB</td>
   <td><tt>5ebeced5d8f4b2aa58e532d5c46511e01fe31f54</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/ark-4.8.80.tar.xz">ark-4.8.80</a></td>
   <td align="right">149kB</td>
   <td><tt>af00048ee056e4a92c93b35f588ff3eb8cb7eea2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/audiocd-kio-4.8.80.tar.xz">audiocd-kio-4.8.80</a></td>
   <td align="right">41kB</td>
   <td><tt>eb4d8c065a77c43eb4492c75085ddf9a1be0d1c8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/blinken-4.8.80.tar.xz">blinken-4.8.80</a></td>
   <td align="right">551kB</td>
   <td><tt>30070f8de37593ae25c764d269fc6dbcf688f82a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/cantor-4.8.80.tar.xz">cantor-4.8.80</a></td>
   <td align="right">256kB</td>
   <td><tt>cdb2842805d8a35f465101b5efe6906ecf9a6bfe</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/dragon-4.8.80.tar.xz">dragon-4.8.80</a></td>
   <td align="right">381kB</td>
   <td><tt>2bf02ca31e2dac65bc621a89e09cc8e5885593d8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/ffmpegthumbs-4.8.80.tar.xz">ffmpegthumbs-4.8.80</a></td>
   <td align="right">13kB</td>
   <td><tt>d72b42d887d40f2feda1e1200859b9723008de38</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/filelight-4.8.80.tar.xz">filelight-4.8.80</a></td>
   <td align="right">286kB</td>
   <td><tt>a21fff43422eba68c687ca41003e29ed7229cffc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/gwenview-4.8.80.tar.xz">gwenview-4.8.80</a></td>
   <td align="right">1.8MB</td>
   <td><tt>1eb9e84a209dcd03790436a7ab3613e5998ea373</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/jovie-4.8.80.tar.xz">jovie-4.8.80</a></td>
   <td align="right">357kB</td>
   <td><tt>5b1f25ece3e8b0bcb40b72be493c9b7524880b26</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/juk-4.8.80.tar.xz">juk-4.8.80</a></td>
   <td align="right">422kB</td>
   <td><tt>ab7049e8db9162926941b7502178342ddd1674fd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kaccessible-4.8.80.tar.xz">kaccessible-4.8.80</a></td>
   <td align="right">19kB</td>
   <td><tt>b38912943052b797b5568bae7bb2a2ff31839d84</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kactivities-4.8.80.tar.xz">kactivities-4.8.80</a></td>
   <td align="right">79kB</td>
   <td><tt>1283527b0432e3e0f554775b077104e696995a3d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kalgebra-4.8.80.tar.xz">kalgebra-4.8.80</a></td>
   <td align="right">428kB</td>
   <td><tt>fd68687a4923c60c805cf3740b921f051788d848</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kalzium-4.8.80.tar.xz">kalzium-4.8.80</a></td>
   <td align="right">3.4MB</td>
   <td><tt>8d177117eb9b92282c7c47c4d7bfd2fda641d182</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kamera-4.8.80.tar.xz">kamera-4.8.80</a></td>
   <td align="right">35kB</td>
   <td><tt>4b9da61bee650aefe85e46183dd720fcf89e5240</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kanagram-4.8.80.tar.xz">kanagram-4.8.80</a></td>
   <td align="right">1.0MB</td>
   <td><tt>311c23ac72fda6c425b4a74c687e41fb730ec3de</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kate-4.8.80.tar.xz">kate-4.8.80</a></td>
   <td align="right">2.0MB</td>
   <td><tt>b4031257ba5e0873e887343ca3d198e211934502</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kbruch-4.8.80.tar.xz">kbruch-4.8.80</a></td>
   <td align="right">883kB</td>
   <td><tt>38277c19633c864fefbeb337eb27bdbbb851fdf8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kcalc-4.8.80.tar.xz">kcalc-4.8.80</a></td>
   <td align="right">84kB</td>
   <td><tt>bff210b459668cbb47d22edf66c002dd3d31988c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kcharselect-4.8.80.tar.xz">kcharselect-4.8.80</a></td>
   <td align="right">83kB</td>
   <td><tt>1c990336dd9c1af8df12f5a71a59e552b9ac6448</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kcolorchooser-4.8.80.tar.xz">kcolorchooser-4.8.80</a></td>
   <td align="right">4kB</td>
   <td><tt>dd76ff7e8a906dd2010aff6ba674dfe9494ffc21</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdeadmin-4.8.80.tar.xz">kdeadmin-4.8.80</a></td>
   <td align="right">1.0MB</td>
   <td><tt>2196c02501eb1b1bb773909f9760665aa17f889d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdeartwork-4.8.80.tar.xz">kdeartwork-4.8.80</a></td>
   <td align="right">132MB</td>
   <td><tt>0a021b7c4b8595936c6721061b9590bbaecd642e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-baseapps-4.8.80.tar.xz">kde-baseapps-4.8.80</a></td>
   <td align="right">2.3MB</td>
   <td><tt>10cada2c0ea7f1d71169d779ddbea2a30f968e2a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-base-artwork-4.8.80.tar.xz">kde-base-artwork-4.8.80</a></td>
   <td align="right">15MB</td>
   <td><tt>20d63e4beab84ec5a45418c7bbdfd57dd3261966</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdegames-4.8.80.tar.xz">kdegames-4.8.80</a></td>
   <td align="right">53MB</td>
   <td><tt>94378986a7f6196ecd2c176fad1804c8c00f2822</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdegraphics-mobipocket-4.8.80.tar.xz">kdegraphics-mobipocket-4.8.80</a></td>
   <td align="right">18kB</td>
   <td><tt>8d8b8a090190b8545cff1f34f9fe28e73b5acfe0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdegraphics-strigi-analyzer-4.8.80.tar.xz">kdegraphics-strigi-analyzer-4.8.80</a></td>
   <td align="right">38kB</td>
   <td><tt>82afc822ac42d8128f6181c24a84ef8734ec67e8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdegraphics-thumbnailers-4.8.80.tar.xz">kdegraphics-thumbnailers-4.8.80</a></td>
   <td align="right">40kB</td>
   <td><tt>fcaa45c94ff6809722548449deb96149b7e35ba1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdelibs-4.8.80.tar.xz">kdelibs-4.8.80</a></td>
   <td align="right">11MB</td>
   <td><tt>e0d4cef3a09c85e4309e48d7ab62ba9b8eccc575</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdenetwork-4.8.80.tar.xz">kdenetwork-4.8.80</a></td>
   <td align="right">8.5MB</td>
   <td><tt>175ba73cfc873f282736ac9a2eb31863690c9670</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdepim-4.8.80.tar.xz">kdepim-4.8.80</a></td>
   <td align="right">13MB</td>
   <td><tt>af86571d86dbb3c9e7af0ef86c6d90fbaf9304bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdepimlibs-4.8.80.tar.xz">kdepimlibs-4.8.80</a></td>
   <td align="right">2.4MB</td>
   <td><tt>6249c7a8331aaa069e932acdc72a94ac0d75d6db</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdepim-runtime-4.8.80.tar.xz">kdepim-runtime-4.8.80</a></td>
   <td align="right">1.0MB</td>
   <td><tt>c6c09b9b0e8e9f4e15bb8791ab333b22f949130b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdeplasma-addons-4.8.80.tar.xz">kdeplasma-addons-4.8.80</a></td>
   <td align="right">1.6MB</td>
   <td><tt>138ed0e557d31c4254bd4cc90f4bab65885dc35e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-runtime-4.8.80.tar.xz">kde-runtime-4.8.80</a></td>
   <td align="right">6.0MB</td>
   <td><tt>c191073f5731ea2cb585a4fdfb3318b2a4f55154</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdesdk-4.8.80.tar.xz">kdesdk-4.8.80</a></td>
   <td align="right">4.8MB</td>
   <td><tt>f440db71079b2bd90ab402bc5d22fa4f222f2c82</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdetoys-4.8.80.tar.xz">kdetoys-4.8.80</a></td>
   <td align="right">368kB</td>
   <td><tt>dab1f82bb98ee92b77be3e600c8e4005a0579b38</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-wallpapers-4.8.80.tar.xz">kde-wallpapers-4.8.80</a></td>
   <td align="right">73MB</td>
   <td><tt>dd673d1c480eff1d2eadb8a71cbdee17e8146cc0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdewebdev-4.8.80.tar.xz">kdewebdev-4.8.80</a></td>
   <td align="right">2.4MB</td>
   <td><tt>3190023d96038eb58dbaa59a25d5688f07e11559</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-workspace-4.8.80.tar.xz">kde-workspace-4.8.80</a></td>
   <td align="right">21MB</td>
   <td><tt>e65c561aaa60b0e3dd25cc93f389f119e1d65832</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kdf-4.8.80.tar.xz">kdf-4.8.80</a></td>
   <td align="right">151kB</td>
   <td><tt>985c810ba4c8a266b289de6e0935c28c47cdd41f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kfloppy-4.8.80.tar.xz">kfloppy-4.8.80</a></td>
   <td align="right">58kB</td>
   <td><tt>2c1db0970b2b00c12180a9abfdbef558780d9e64</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kgamma-4.8.80.tar.xz">kgamma-4.8.80</a></td>
   <td align="right">25kB</td>
   <td><tt>65abd0f994448a9a92048145af3e4b5121be59ed</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kgeography-4.8.80.tar.xz">kgeography-4.8.80</a></td>
   <td align="right">6.4MB</td>
   <td><tt>5b2a17154fdce9d5bdd27124e8c51816d6679e93</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kgpg-4.8.80.tar.xz">kgpg-4.8.80</a></td>
   <td align="right">789kB</td>
   <td><tt>08d13af5cbc44547b96087cbbcbeae249fd6fdc6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/khangman-4.8.80.tar.xz">khangman-4.8.80</a></td>
   <td align="right">3.6MB</td>
   <td><tt>25a57b963e0ad130fcacd926316bd1237d93a573</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kig-4.8.80.tar.xz">kig-4.8.80</a></td>
   <td align="right">1.4MB</td>
   <td><tt>37c9801eb57c8bc774621f76c12eb7805bd895cd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kimono-4.8.80.tar.xz">kimono-4.8.80</a></td>
   <td align="right">910kB</td>
   <td><tt>348529351170cba46ea3bb071416f54cedd05f9c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kiten-4.8.80.tar.xz">kiten-4.8.80</a></td>
   <td align="right">11MB</td>
   <td><tt>04502f5b20493ec396c31e844ceba4aff09d6b30</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/klettres-4.8.80.tar.xz">klettres-4.8.80</a></td>
   <td align="right">2.5MB</td>
   <td><tt>7e13e0b73dfb7b99372c636f5d8ec52112869cb7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kmag-4.8.80.tar.xz">kmag-4.8.80</a></td>
   <td align="right">89kB</td>
   <td><tt>8e4251fc8cf61a18816d5674ce4e353f5ab70560</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kmix-4.8.80.tar.xz">kmix-4.8.80</a></td>
   <td align="right">276kB</td>
   <td><tt>1b3d07117178d3232d308babb4c42dfdebc58d2d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kmousetool-4.8.80.tar.xz">kmousetool-4.8.80</a></td>
   <td align="right">43kB</td>
   <td><tt>8f7a55b3cb32ffe4cd098f5c66c8d6327d97e027</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kmouth-4.8.80.tar.xz">kmouth-4.8.80</a></td>
   <td align="right">310kB</td>
   <td><tt>6f11f249f3b2fb314a2536c23da2ccea9f491762</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kmplot-4.8.80.tar.xz">kmplot-4.8.80</a></td>
   <td align="right">660kB</td>
   <td><tt>0ccef544a8fcde8be41300d160253505bca41576</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kolourpaint-4.8.80.tar.xz">kolourpaint-4.8.80</a></td>
   <td align="right">1.1MB</td>
   <td><tt>d755e72558fd333697028fdc14bcb8a1db7f8256</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/konsole-4.8.80.tar.xz">konsole-4.8.80</a></td>
   <td align="right">420kB</td>
   <td><tt>4acd23dab275143cd8c182ca3460d259ae472b43</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/korundum-4.8.80.tar.xz">korundum-4.8.80</a></td>
   <td align="right">156kB</td>
   <td><tt>0beb858c76fe21d6575ee48cb8490013ff360ad7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kremotecontrol-4.8.80.tar.xz">kremotecontrol-4.8.80</a></td>
   <td align="right">1.0MB</td>
   <td><tt>4a29cb8461e8ef3f5a145116112b2c013d14804c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kross-interpreters-4.8.80.tar.xz">kross-interpreters-4.8.80</a></td>
   <td align="right">127kB</td>
   <td><tt>829814ae8753ffe4c7dc44a733c022b5447d7fa5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kruler-4.8.80.tar.xz">kruler-4.8.80</a></td>
   <td align="right">129kB</td>
   <td><tt>ccaa5c48bd2a64f9d428729edb6b6dbdbbface3a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/ksaneplugin-4.8.80.tar.xz">ksaneplugin-4.8.80</a></td>
   <td align="right">12kB</td>
   <td><tt>9327ce8c88ec2b425717538f9dc3a0ce4fc0c50c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kscd-4.8.80.tar.xz">kscd-4.8.80</a></td>
   <td align="right">84kB</td>
   <td><tt>2352ef7a8b85ce6a4610563b2be0a37c0c1dfe41</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/ksnapshot-4.8.80.tar.xz">ksnapshot-4.8.80</a></td>
   <td align="right">256kB</td>
   <td><tt>8b19cf73a9a0ad34de612167bf7be6a47babfe8d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kstars-4.8.80.tar.xz">kstars-4.8.80</a></td>
   <td align="right">11MB</td>
   <td><tt>8e869413470abc4b984ae878ebcc34646cfaafe5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/ktimer-4.8.80.tar.xz">ktimer-4.8.80</a></td>
   <td align="right">146kB</td>
   <td><tt>dd5da011a51b93b9aceaa9372ce1a598dfb0bbf5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/ktouch-4.8.80.tar.xz">ktouch-4.8.80</a></td>
   <td align="right">1.6MB</td>
   <td><tt>9cb0caaadc9b80eb452cebe31a7a58b8cdadc037</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kturtle-4.8.80.tar.xz">kturtle-4.8.80</a></td>
   <td align="right">206kB</td>
   <td><tt>b5c3705ac91c0ab3e5781f0fb764da113a427673</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kwallet-4.8.80.tar.xz">kwallet-4.8.80</a></td>
   <td align="right">273kB</td>
   <td><tt>9a10b4c1fa41cc842ca47889a11a107430994f55</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kwordquiz-4.8.80.tar.xz">kwordquiz-4.8.80</a></td>
   <td align="right">1.1MB</td>
   <td><tt>3a0e782456430e878118256e8058518d981433d2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/libkcddb-4.8.80.tar.xz">libkcddb-4.8.80</a></td>
   <td align="right">151kB</td>
   <td><tt>5adbc38ae49d8e948c0ef55e2a2185e82872524d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/libkcompactdisc-4.8.80.tar.xz">libkcompactdisc-4.8.80</a></td>
   <td align="right">73kB</td>
   <td><tt>d7a052a3c7e5a9efaa8d3535f5f350dfc3ec6793</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/libkdcraw-4.8.80.tar.xz">libkdcraw-4.8.80</a></td>
   <td align="right">260kB</td>
   <td><tt>d2b2e3ab0ca1a1ca58e73de0595ea3c3ebcb56c6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/libkdeedu-4.8.80.tar.xz">libkdeedu-4.8.80</a></td>
   <td align="right">204kB</td>
   <td><tt>ebdc42d9eacdeae49183f0393a2bf75ab86da15e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/libkexiv2-4.8.80.tar.xz">libkexiv2-4.8.80</a></td>
   <td align="right">130kB</td>
   <td><tt>7693b3454d6dd0a77e01df6f160bb7721605db73</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/libkipi-4.8.80.tar.xz">libkipi-4.8.80</a></td>
   <td align="right">73kB</td>
   <td><tt>0e2335ab38e65fa33a93fed1d8f35c6984b20c30</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/libksane-4.8.80.tar.xz">libksane-4.8.80</a></td>
   <td align="right">77kB</td>
   <td><tt>f5377bce072e80a122c5e2371076463f70d9a7f1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/marble-4.8.80.tar.xz">marble-4.8.80</a></td>
   <td align="right">18MB</td>
   <td><tt>9cf35d867f1336c5d42bbb3fa90a8310742d5008</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/mplayerthumbs-4.8.80.tar.xz">mplayerthumbs-4.8.80</a></td>
   <td align="right">21kB</td>
   <td><tt>44f530ac94b1b5b4051579e407dd6a98636be1eb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/nepomuk-core-4.8.80.tar.xz">nepomuk-core-4.8.80</a></td>
   <td align="right">371kB</td>
   <td><tt>023f241f61d778cee16d29fbc64968a6925e42ee</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/okular-4.8.80.tar.xz">okular-4.8.80</a></td>
   <td align="right">1.0MB</td>
   <td><tt>01f5bccc92a12847d975ff39f5ae0861fbcefe7c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/oxygen-icons-4.8.80.tar.xz">oxygen-icons-4.8.80</a></td>
   <td align="right">262MB</td>
   <td><tt>9f5284c72c59ac622b9ac48e2e8e2a386cdfd198</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/pairs-4.8.80.tar.xz">pairs-4.8.80</a></td>
   <td align="right">2.1MB</td>
   <td><tt>5e7576227c881c8620495c82673d334f18e6e157</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/parley-4.8.80.tar.xz">parley-4.8.80</a></td>
   <td align="right">7.8MB</td>
   <td><tt>0ea1f3fbe0ab321d347e0d961c0bc335b8babf63</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/perlkde-4.8.80.tar.xz">perlkde-4.8.80</a></td>
   <td align="right">39kB</td>
   <td><tt>ac8fddd40ee3020ce8a344a25810b37675a38bd2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/perlqt-4.8.80.tar.xz">perlqt-4.8.80</a></td>
   <td align="right">1.7MB</td>
   <td><tt>a0e7bbb60978c43dc6f888c3ee0e757d0eb14be0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/printer-applet-4.8.80.tar.xz">printer-applet-4.8.80</a></td>
   <td align="right">34kB</td>
   <td><tt>6a8691b6f810a0ad2b4d69e581f97151b8b9e0df</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/pykde4-4.8.80.tar.xz">pykde4-4.8.80</a></td>
   <td align="right">1.8MB</td>
   <td><tt>bd93872670dc56b0007467a6a9319089e84eb431</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/qtruby-4.8.80.tar.xz">qtruby-4.8.80</a></td>
   <td align="right">517kB</td>
   <td><tt>8cf9bc95f95f72c00f32e0a867e20e5029b8bcbb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/qyoto-4.8.80.tar.xz">qyoto-4.8.80</a></td>
   <td align="right">495kB</td>
   <td><tt>70b384fdc5509fcde8870e5aa1a08b285ef9d35f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/rocs-4.8.80.tar.xz">rocs-4.8.80</a></td>
   <td align="right">804kB</td>
   <td><tt>4c0a872c547f16e59f46992b8549cd098e8234e0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/smokegen-4.8.80.tar.xz">smokegen-4.8.80</a></td>
   <td align="right">142kB</td>
   <td><tt>0ab79864476cfb9888ce55226f539fced5b08ee4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/smokekde-4.8.80.tar.xz">smokekde-4.8.80</a></td>
   <td align="right">35kB</td>
   <td><tt>d349b9fe6ef29c0717d3faa831d2f2d1f0281290</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/smokeqt-4.8.80.tar.xz">smokeqt-4.8.80</a></td>
   <td align="right">29kB</td>
   <td><tt>c24027167d9d05a3feb5ebae8700359d3d92b8de</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/step-4.8.80.tar.xz">step-4.8.80</a></td>
   <td align="right">361kB</td>
   <td><tt>7136e43cb22feb501aee4a3a5efbf96892148198</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/strigi-multimedia-4.8.80.tar.xz">strigi-multimedia-4.8.80</a></td>
   <td align="right">18kB</td>
   <td><tt>0b5c0391909e02dba816fe9a7de368b76dd61379</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/superkaramba-4.8.80.tar.xz">superkaramba-4.8.80</a></td>
   <td align="right">374kB</td>
   <td><tt>52bc79af7791641aee7988bacdbcd63c589ce40a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/svgpart-4.8.80.tar.xz">svgpart-4.8.80</a></td>
   <td align="right">8kB</td>
   <td><tt>812a5b8972137d3edb9cd0c922f4e5630374500f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/sweeper-4.8.80.tar.xz">sweeper-4.8.80</a></td>
   <td align="right">81kB</td>
   <td><tt>2c36c5e3f32fdd9b83fcd908b831a5df5a23e619</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ar-4.8.80.tar.xz">kde-l10n-ar-4.8.80</a></td>
   <td align="right">3.2MB</td>
   <td><tt>d85fd78ebbf600d05686f7aeff161b19d147b30c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-bg-4.8.80.tar.xz">kde-l10n-bg-4.8.80</a></td>
   <td align="right">1.9MB</td>
   <td><tt>65e56ebbb458511c84ddc87c6c6dafde3d70709e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-bs-4.8.80.tar.xz">kde-l10n-bs-4.8.80</a></td>
   <td align="right">2.2MB</td>
   <td><tt>8e48dfa30fff5122933b6a296ee8f6c500c2fe10</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ca-4.8.80.tar.xz">kde-l10n-ca-4.8.80</a></td>
   <td align="right">9.1MB</td>
   <td><tt>7b4156c6e3240418336b1a7d163526a4bf2e7e28</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ca@valencia-4.8.80.tar.xz">kde-l10n-ca@valencia-4.8.80</a></td>
   <td align="right">2.1MB</td>
   <td><tt>447b36fe038b47ea11046448a1dfe80abf1b7f85</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-cs-4.8.80.tar.xz">kde-l10n-cs-4.8.80</a></td>
   <td align="right">2.9MB</td>
   <td><tt>79ecdac846f04a2ef386d26d25258144b4866c24</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-da-4.8.80.tar.xz">kde-l10n-da-4.8.80</a></td>
   <td align="right">12MB</td>
   <td><tt>eb7fbbe08eaf7505047b8d5bf7ec8cd3529b4232</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-de-4.8.80.tar.xz">kde-l10n-de-4.8.80</a></td>
   <td align="right">36MB</td>
   <td><tt>bf5de0a544e2a4e3169f642da8515fee4ed25737</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-el-4.8.80.tar.xz">kde-l10n-el-4.8.80</a></td>
   <td align="right">4.5MB</td>
   <td><tt>07d0abd7eeabce8e5a747a52dd322ce450f3ca6a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-en_GB-4.8.80.tar.xz">kde-l10n-en_GB-4.8.80</a></td>
   <td align="right">3.0MB</td>
   <td><tt>41e876d820adec9837cffdc8a194744d6d9492f2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-es-4.8.80.tar.xz">kde-l10n-es-4.8.80</a></td>
   <td align="right">22MB</td>
   <td><tt>42998484e40d7e47a51f32b29200f67624b654a7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-et-4.8.80.tar.xz">kde-l10n-et-4.8.80</a></td>
   <td align="right">7.1MB</td>
   <td><tt>94de0ebbf026a6187ac0dadff9a0e9132f84ed3e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-eu-4.8.80.tar.xz">kde-l10n-eu-4.8.80</a></td>
   <td align="right">2.0MB</td>
   <td><tt>06bff320cf2022aa15030da4401006cdf14bfff8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-fa-4.8.80.tar.xz">kde-l10n-fa-4.8.80</a></td>
   <td align="right">1.8MB</td>
   <td><tt>78da7e1054bf657de8c89496cd22d64d454b078b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-fi-4.8.80.tar.xz">kde-l10n-fi-4.8.80</a></td>
   <td align="right">2.4MB</td>
   <td><tt>85bea26a321ffdab6c41174f0c40b931ee386167</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-fr-4.8.80.tar.xz">kde-l10n-fr-4.8.80</a></td>
   <td align="right">35MB</td>
   <td><tt>c10310c007392f8edd8c6e83fd6e048fffa6cc44</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ga-4.8.80.tar.xz">kde-l10n-ga-4.8.80</a></td>
   <td align="right">2.7MB</td>
   <td><tt>e20d7d12610a7a8ab433624dd2112939201eeb20</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-gl-4.8.80.tar.xz">kde-l10n-gl-4.8.80</a></td>
   <td align="right">4.0MB</td>
   <td><tt>a6afc2cc7cf44da6e1b2f0aac7e8ba0ba8db3c3d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-he-4.8.80.tar.xz">kde-l10n-he-4.8.80</a></td>
   <td align="right">2.0MB</td>
   <td><tt>88426a0e19c5f6cabcf0fe6ab7e7084675a800a6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-hr-4.8.80.tar.xz">kde-l10n-hr-4.8.80</a></td>
   <td align="right">1.9MB</td>
   <td><tt>1d7b29fae63211183301f85a5ac9bd8f08ccef0f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-hu-4.8.80.tar.xz">kde-l10n-hu-4.8.80</a></td>
   <td align="right">4.0MB</td>
   <td><tt>cd0eb32413104a6b492c0e9e65507a95ca0bc53e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ia-4.8.80.tar.xz">kde-l10n-ia-4.8.80</a></td>
   <td align="right">1.2MB</td>
   <td><tt>ab3179e5f02db6d9ddba2a09e12c36e459f5004f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-id-4.8.80.tar.xz">kde-l10n-id-4.8.80</a></td>
   <td align="right">503kB</td>
   <td><tt>998eef2468aa9c299eeab2c9d224bdbc5ec58942</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-is-4.8.80.tar.xz">kde-l10n-is-4.8.80</a></td>
   <td align="right">1.8MB</td>
   <td><tt>f10e26d950fca8a9cd0e780258be8247856ddffa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-it-4.8.80.tar.xz">kde-l10n-it-4.8.80</a></td>
   <td align="right">9.9MB</td>
   <td><tt>6274541d7fa45f9699ad4a480d5fafb6062618e7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ja-4.8.80.tar.xz">kde-l10n-ja-4.8.80</a></td>
   <td align="right">2.2MB</td>
   <td><tt>64744795f5cb06bb199d151349a053d389bd0ffa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-kk-4.8.80.tar.xz">kde-l10n-kk-4.8.80</a></td>
   <td align="right">2.4MB</td>
   <td><tt>ca358492c6efcc18c21caada0d82837088ecbb31</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-km-4.8.80.tar.xz">kde-l10n-km-4.8.80</a></td>
   <td align="right">2.1MB</td>
   <td><tt>37705949f5cd953a81d68ff77c11b2fb25600d36</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ko-4.8.80.tar.xz">kde-l10n-ko-4.8.80</a></td>
   <td align="right">1.7MB</td>
   <td><tt>1d3d4bc0fe31997fe04c2e7de435a3416b529b7a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-lt-4.8.80.tar.xz">kde-l10n-lt-4.8.80</a></td>
   <td align="right">10MB</td>
   <td><tt>24f41faf1dab72dad2965ca629772d71cfc6ba31</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-lv-4.8.80.tar.xz">kde-l10n-lv-4.8.80</a></td>
   <td align="right">2.0MB</td>
   <td><tt>2f40f3eda3317a740ac7ac79f8d303c7180481a0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-nb-4.8.80.tar.xz">kde-l10n-nb-4.8.80</a></td>
   <td align="right">2.3MB</td>
   <td><tt>6b618f5762243839b605aa3867140ed04303c75a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-nds-4.8.80.tar.xz">kde-l10n-nds-4.8.80</a></td>
   <td align="right">3.0MB</td>
   <td><tt>844e2ba0403d06c398cab75f63e93146475759a8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-nl-4.8.80.tar.xz">kde-l10n-nl-4.8.80</a></td>
   <td align="right">15MB</td>
   <td><tt>3a35db1141c85ccfe98b415da219422bf43224eb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-nn-4.8.80.tar.xz">kde-l10n-nn-4.8.80</a></td>
   <td align="right">1.9MB</td>
   <td><tt>b99c5ba891de0bc09df5ca71fd13353eb7802957</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-pa-4.8.80.tar.xz">kde-l10n-pa-4.8.80</a></td>
   <td align="right">1.8MB</td>
   <td><tt>33e0af4afb814d9db097ed6d782c1c8bfa0bc239</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-pl-4.8.80.tar.xz">kde-l10n-pl-4.8.80</a></td>
   <td align="right">16MB</td>
   <td><tt>7df003542b0d108f628fee16554c6cf9b22c1fc1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-pt-4.8.80.tar.xz">kde-l10n-pt-4.8.80</a></td>
   <td align="right">5.6MB</td>
   <td><tt>ad3461f1368c2237206056956639221694abde44</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-pt_BR-4.8.80.tar.xz">kde-l10n-pt_BR-4.8.80</a></td>
   <td align="right">26MB</td>
   <td><tt>78c434f9de3b8d88b0cd0b7075db16009914cb14</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ro-4.8.80.tar.xz">kde-l10n-ro-4.8.80</a></td>
   <td align="right">2.8MB</td>
   <td><tt>b0782c455b93644cc2e741d5cf701920c8d2f03f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ru-4.8.80.tar.xz">kde-l10n-ru-4.8.80</a></td>
   <td align="right">22MB</td>
   <td><tt>5b02e27332f731fc581b7cd9368621b349ad15a7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-si-4.8.80.tar.xz">kde-l10n-si-4.8.80</a></td>
   <td align="right">1.0MB</td>
   <td><tt>da526cc85c42edd084745d893f73dde5485099a8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-sk-4.8.80.tar.xz">kde-l10n-sk-4.8.80</a></td>
   <td align="right">4.1MB</td>
   <td><tt>c7964f5dca34c5090750a069f1577dfbf4b1f3b5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-sl-4.8.80.tar.xz">kde-l10n-sl-4.8.80</a></td>
   <td align="right">3.2MB</td>
   <td><tt>776916b5172d86e7ee8dc7c7ab7dd9b1c3524d07</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-sr-4.8.80.tar.xz">kde-l10n-sr-4.8.80</a></td>
   <td align="right">5.6MB</td>
   <td><tt>a056e609902d2d48768ed2b25c3d71dc7df99c56</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-sv-4.8.80.tar.xz">kde-l10n-sv-4.8.80</a></td>
   <td align="right">16MB</td>
   <td><tt>6cb0af55fd1028c68c361a8d712b8f60006e38c1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-tg-4.8.80.tar.xz">kde-l10n-tg-4.8.80</a></td>
   <td align="right">1.9MB</td>
   <td><tt>420c375489b53f2b49e0c087b708dd668d05bac1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-th-4.8.80.tar.xz">kde-l10n-th-4.8.80</a></td>
   <td align="right">1.6MB</td>
   <td><tt>d2f38418c80fdc22ff4c28eb9bb07f0b9cd6f376</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-tr-4.8.80.tar.xz">kde-l10n-tr-4.8.80</a></td>
   <td align="right">2.3MB</td>
   <td><tt>7375d48ed4de0caed7ea14de561675cb871cafa1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-ug-4.8.80.tar.xz">kde-l10n-ug-4.8.80</a></td>
   <td align="right">1.6MB</td>
   <td><tt>206d29b225da7f26fe34599cdbb24aff096ed75d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-uk-4.8.80.tar.xz">kde-l10n-uk-4.8.80</a></td>
   <td align="right">21MB</td>
   <td><tt>1d953f1fd467ab21b027c4a051a7bdc2bdd00948</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-vi-4.8.80.tar.xz">kde-l10n-vi-4.8.80</a></td>
   <td align="right">910kB</td>
   <td><tt>6da470e3d179fa5ae8175e388949b5229914f396</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-wa-4.8.80.tar.xz">kde-l10n-wa-4.8.80</a></td>
   <td align="right">1.6MB</td>
   <td><tt>2e3383131355186e7f154e20b7269f6ee2f12c94</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-zh_CN-4.8.80.tar.xz">kde-l10n-zh_CN-4.8.80</a></td>
   <td align="right">3.3MB</td>
   <td><tt>dc905047739dc763a6885c0d9aed656741b09480</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.80/src/kde-l10n/kde-l10n-zh_TW-4.8.80.tar.xz">kde-l10n-zh_TW-4.8.80</a></td>
   <td align="right">2.4MB</td>
   <td><tt>9cb1642b21e8b7a15d4fa4588c3e0e1278dbe509</tt></td>
</tr>

</table>