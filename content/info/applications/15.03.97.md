---
title: "KDE Applications 15.03.97 Info Page"
announcement: /announcements/announce-applications-15.04-rc
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
