---
title: "KDE Applications 16.04.2 Info Page"
announcement: /announcements/announce-applications-16.04.2
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
