KDE Project Security Advisory
=============================

Title:           Ruqola: server info dialog can execute local binary
Risk Rating:     Low
Versions:        Ruqola <= 1.8.1
Author:          David Faure <faure@kde.org>
Date:            24 November 2022

Overview
========

A user can be tricked into launching a local executable from Ruqola.

An attacker could document that users should configure a rocket chat server with a local file URL pointing to an executable. Uninformed users who would then click on the URL in the "Server Info" dialog box would end up launching this local executable unexpectedly.

Impact
======

Depends on the local executable being launched.
If the attacker ensures that a malicious shell script exists somewhere in the system
first, by other means, this could be a way to get the user to execute it.

Workaround
==========

Not clicking on the "Server URL" in the "Server Info" dialog box.

Solution
========

Update to Ruqola 5.8.2
or apply this patch: https://invent.kde.org/network/ruqola/-/commit/2c4ad7efd0fd30c

Credits
=======

Thanks to Mishra Dhiraj for reporting the issue.
Thanks to Laurent Montel for fixing the issue.
Thanks to both of them for reviewing the advisory.

