-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: KDE 2.2 / Konqueror Embedded SSL vulnerability
Original Release Date: 2003-06-02
URL: http://www.kde.org/info/security/advisory-20030602-1.txt

0. References
        http://lists.insecure.org/lists/fulldisclosure/2003/May/0161.html

1. Systems affected:

        Konqueror Embedded and KDE 2.2.2 and earlier versions.
        KDE 3.0 and later versions are not affected.

2. Overview:

        KDE's SSL implementation in the affected versions matches certificates
based on IP number instead of hostname. Due to this it may fail to notice
a man-in-the-middle attack.
      
3. Impact:

        Users of Konqueror and other SSL enabled KDE software may fall victim
to a malicious man-in-the-middle attack without noticing. In such case the
user will be under the impression that there is a secure connection with a
trusted site while in fact a different site has been connected to.

4. Solution:

        Users of KDE 2.2.2 are advised to upgrade to either KDE 3.0.5a or 
KDE 3.1.2. A patch for KDE 2.2.2 is available as well for users that are 
unable to upgrade to KDE 3.

        Users of Konqueror/Embedded are advised to upgrade to a snapshot of
Konqueror/Embedded of May 16, 2003 or later, available from 
http://devel-home.kde.org/~hausmann/snapshots/ :

        a58888ab9b7910c5d5f498da15f2d425  konqueror-embedded-snapshot-20030516.tar.gz
        

5. Patch:
        Patches for KDE 2.2.2 are available from 
ftp://ftp.kde.org/pub/kde/security_patches :

        4c252809dec8be73bbe55367350c27ca  post-2.2.2-kdelibs-kssl-2.diff
        441afec72fab406f8c1cd7d6b839b3e0  post-2.2.2-kdelibs-kio-2.diff

6. Timeline and credits:

        05/14/2003 Notification of security@kde.org by Jesse Burns
        05/14/2003 Fixed KDE 2.2.x in KDE CVS.
        05/17/2003 Fixed version of Konqueror/Embedded announced.
	06/02/2003 Public Security Advisory by the KDE Security team.

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.0.6 (GNU/Linux)
Comment: For info see http://www.gnupg.org

iD8DBQE+2167N4pvrENfboIRApcGAJ90KrA97qBumTGfEuSIeIC0dYOF/wCeM6ND
Qt7FofJtEsQEoonr23zv52Q=
=yPRB
-----END PGP SIGNATURE-----
