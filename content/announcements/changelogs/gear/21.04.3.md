---
aliases:
- ../../fulllog_releases-21.04.3
- ../releases/21.04.3
title: KDE Gear 21.04.3 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Revert "calendarbase.cpp - handleUidChange(): reload memory calendar". [Commit.](http://commits.kde.org/akonadi-calendar/9f8d9d420922686a1d83fb09cf5ebc2b49bb4472) 
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Remove the width/height from screenshot. [Commit.](http://commits.kde.org/ark/524c5c30f559f5198c28d9073d07b67fce076ab1) 
{{< /details >}}
{{< details id="calendarsupport" title="calendarsupport" link="https://commits.kde.org/calendarsupport" >}}
+ Obey the Exclude Private/Confidential flags. [Commit.](http://commits.kde.org/calendarsupport/f3d6dceda6f463c7dd460018b012a881df0afaf9) 
+ Infinite loop if Exclude Private/Confidential skipped something. [Commit.](http://commits.kde.org/calendarsupport/25a356af31613818cdf740a63bc6b5ca61b1f031) 
+ Fix the split week printout. [Commit.](http://commits.kde.org/calendarsupport/6f97d1802eb7e2603919fec9bbcc9052e2c9f9d2) 
+ Improve the small calendars in the printed headers. [Commit.](http://commits.kde.org/calendarsupport/538d1441aabad03e912a1775ea9d131e54f3d5cf) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix shift-action modifier in context menu. [Commit.](http://commits.kde.org/dolphin/dc3beae3ab2c55cd3501e17f93b51e93e876a177) Fixes bug [#425997](https://bugs.kde.org/425997)
+ Fix X-DocPath entries. [Commit.](http://commits.kde.org/dolphin/0a0d0c48ee228e5117a85ee6ebdf8af93aa54d60) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix inhibit suspend on Gnome. [Commit.](http://commits.kde.org/elisa/8562e101563c5fd7e17b59997cd786f98e8d3a1e) 
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Make To-do List sort by priority consistent with KCalendarCore. [Commit.](http://commits.kde.org/eventviews/cc66bbde52e434e39aa30f35e69121639af7cf17) 
+ Display the tops of journal entries in journal frames. [Commit.](http://commits.kde.org/eventviews/6900fddf0f21abed1eefa2b7261638cf2def19bf) Fixes bug [#437669](https://bugs.kde.org/437669)
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Allow journal entries to be private or confidential. [Commit.](http://commits.kde.org/incidenceeditor/9343666359ccbac0ce85a89f7586e559a8a7044e) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Hotfix for upcoming CardListView changes in Kirigami 5.84. [Commit.](http://commits.kde.org/itinerary/747ebdde0b7cfaf15f4a90b601e5b8faeec970dc) 
+ Add content rating to appstream data. [Commit.](http://commits.kde.org/itinerary/5a77ffbee8f68f244a05dff3d041abe22b781620) 
+ Fix line breaks in departure view disruption notes. [Commit.](http://commits.kde.org/itinerary/96d4a1cca3536e86fc1e332035433bc36b374bcd) 
+ Force using org.kde.desktop style on non-Android. [Commit.](http://commits.kde.org/itinerary/bc36aa02c4f87f2837ef2821418c712d1b3d425f) 
{{< /details >}}
{{< details id="kaccounts-providers" title="kaccounts-providers" link="https://commits.kde.org/kaccounts-providers" >}}
+ Preserve Owncloud URL path when returning dav data. [Commit.](http://commits.kde.org/kaccounts-providers/2682610f56de67aa1f3cc5de11723c60f3e2dcac) 
+ Preserve Nextcloud path when returning dav data. [Commit.](http://commits.kde.org/kaccounts-providers/7a05624e4ad06c188c83d1d2590079d0a463b9b5) Fixes bug [#438084](https://bugs.kde.org/438084)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Make sure we only have one button for 'Enable Indexing'. [Commit.](http://commits.kde.org/kate/dbf304023c22d4fe3c442bb1e01d507368c8eca4) 
{{< /details >}}
{{< details id="kde-dev-scripts" title="kde-dev-scripts" link="https://commits.kde.org/kde-dev-scripts" >}}
+ Extractrc: Fix double-unescaping of `&amp;quot;`. [Commit.](http://commits.kde.org/kde-dev-scripts/2fc1ab6acbc4f8e954b6f661ae6d59091fc93f65) 
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Fix Windows build by triggering KIconLoader when indicator starts. [Commit.](http://commits.kde.org/kdeconnect-kde/e1ab0df3a1149055329f67da543f1e1ccdca3f3c) 
+ Disable kpeople qml module finding. [Commit.](http://commits.kde.org/kdeconnect-kde/01153c9e4ee730b3272cb990ce2dabb0c720fe4e) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix crash closing project with a mix on a clip with keyframable effect. [Commit.](http://commits.kde.org/kdenlive/3f1c428b0d8bf2ed38fac1a0746c192e895b9eff) 
+ Fix speech to text after recent VOSK api change. [Commit.](http://commits.kde.org/kdenlive/10ef3b9d6c9902bf3d6d8b6f7b7f2e9b2aa88aa8) 
+ Cleanup font setting for qml timeline. [Commit.](http://commits.kde.org/kdenlive/dc0069923773b958bcabd78b5491ce0bdf9472d2) 
+ Add webp mime type to clip creation dialog. [Commit.](http://commits.kde.org/kdenlive/3195a83785dd38703badf4cc50066d9bd4d069a1) 
+ Fix startup crash on Wayland, thanks to login.kde@unrelenting.technology. [Commit.](http://commits.kde.org/kdenlive/be5f24f72a6b4189b6069a71bef53041689a330a) Fixes bug [#431505](https://bugs.kde.org/431505)
+ Mix: Fix first clip out not correctly reset on second clip deletion. [Commit.](http://commits.kde.org/kdenlive/f32acaf7b6da00f1e263b972027e0db6a509a0c2) 
+ Fix crash on exit when a mix is selected. [Commit.](http://commits.kde.org/kdenlive/8ab92f39068d7aa86ad72cf9e98c8f6585e3aa6d) 
+ Resolved Bug 436895 - "Rotoscoping not working right". [Commit.](http://commits.kde.org/kdenlive/f24a21e92a491ecd7b8e7ec3169c91d839e5f436) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix order. [Commit.](http://commits.kde.org/kdepim-addons/dd909549fc85252e84d73a4c53d3e157af252884) 
+ Don't get list from not local server. It's not authorized. [Commit.](http://commits.kde.org/kdepim-addons/b0ea197ecc7de7cfd4e23c3e9c5561d83431bf1c) 
+ Update url. [Commit.](http://commits.kde.org/kdepim-addons/da20af3a4c134f37d9cfb6a25562670043cc9134) 
+ Fix Bug 439205 - language tool, language list error. [Commit.](http://commits.kde.org/kdepim-addons/5421e8555ee046bb5a33a834666a0d69a08aee5d) Fixes bug [#439205](https://bugs.kde.org/439205)
+ Use translated messages here. [Commit.](http://commits.kde.org/kdepim-addons/1b1e5c107f2a3756a7169e1e2492f6c2969e9e5a) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Add missing include for std::this_thread. [Commit.](http://commits.kde.org/kio-extras/7157908890db738f8c2e47e1ca59e12eaf4091e4) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add extractor for Impfzentren Bayern. [Commit.](http://commits.kde.org/kitinerary/1ee8c56e6461bb8a27f2235b7b0ba23b68a418be) 
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Append the patch number as two digit number. [Commit.](http://commits.kde.org/kleopatra/fe458d914bb0095ec703a537703ba93ebdccc9d7) 
+ Append a compact release service version to Kleopatra's version number. [Commit.](http://commits.kde.org/kleopatra/d0369abcd229d21d786307077af30edde5dc18df) 
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ Minimal changes to use libera.chat. [Commit.](http://commits.kde.org/konversation/8ccc5223aeaccfa9d22b7e055c87ea652f17ba0b) See bug [#437589](https://bugs.kde.org/437589)
+ Honor KDE Kiosk setting lineedit_reveal_password. [Commit.](http://commits.kde.org/konversation/904580c6d1a5291d7fe8b3c17f90f2b5c67206e8) 
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Remove the boundary filter. [Commit.](http://commits.kde.org/kosmindoormap/f2be1d82130cb593be70620ce9fcc82b16023fa8) 
{{< /details >}}
{{< details id="ksystemlog" title="ksystemlog" link="https://commits.kde.org/ksystemlog" >}}
+ Use actually released versions with *_DISABLE_DEPRECATED. [Commit.](http://commits.kde.org/ksystemlog/32f2694cc96a559901ac3d27ddb40181e5956fe0) 
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ Fix VirtualFileQt::seek return value. [Commit.](http://commits.kde.org/libkdegames/e596a46827e795e57b8b57d01ceb63fe95c2e810) 
{{< /details >}}
{{< details id="libksane" title="libksane" link="https://commits.kde.org/libksane" >}}
+ Do not allow to query for new devices when a device is opened. [Commit.](http://commits.kde.org/libksane/c9d8e2e6a3d24d5f204c8020a46d40e9cea070e4) See bug [#438229](https://bugs.kde.org/438229)
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Bug 439501 - Mail auto-prune time for read and unread get switched on. [Commit.](http://commits.kde.org/mailcommon/e0e7e702df21ac3b0b16b94f3871c0ddbe53e962) Fixes bug [#439501](https://bugs.kde.org/439501)
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Request Pixmaps in slotRelayoutPages. [Commit.](http://commits.kde.org/okular/e1a14b4a1a5bb3a10b254a4d9a3c9c31104561c2) 
+ Fix signatures.html help:/ url. [Commit.](http://commits.kde.org/okular/960dc24674bc48f5aca502edcbbe74dc6f6ce30a) 
+ Epub: switch epub logs to warning. [Commit.](http://commits.kde.org/okular/8f269d1884604ff67167e4a03416c184ede5297c) Fixes bug [#438490](https://bugs.kde.org/438490)
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Fix crash in all(?) KIPI export plugins. [Commit.](http://commits.kde.org/spectacle/f7b57e12026875e09de61b6f497787f443806018) 
{{< /details >}}
