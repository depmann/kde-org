---
aliases:
- ../../fulllog_applications-15.04.2
hidden: true
title: KDE Applications 15.04.2 Full Log Page
type: fulllog
version: 15.04.2
---

<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Libarchive: Stop manually handling temporary work files. <a href='http://commits.kde.org/ark/de3cc8dc8c1061e5d40a5fbe8fbe2d45f7f9f9a8'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Show]</a></h3>
<ul id='ulcantor' style='display: none'>
<li>Make appdata basename match .desktop basename. <a href='http://commits.kde.org/cantor/84e6e0384cc9b75b1903d5ceb3864cf002d765e4'>Commit.</a> </li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Show]</a></h3>
<ul id='ulfilelight' style='display: none'>
<li>Reimplement support for relative paths in argv (ported to QUrl::fromUserInput). <a href='http://commits.kde.org/filelight/c3df40b92957cbaf2cfcc6dbdeff85db9cdb7899'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Show]</a></h3>
<ul id='ulgwenview' style='display: none'>
<li>Make discovery of libjpeg's version more robust. <a href='http://commits.kde.org/gwenview/4dc18b994558cbc88d8f1a701247ece3868682a8'>Commit.</a> See bug <a href='https://bugs.kde.org/343105'>#343105</a></li>
</ul>
<h3><a name='kaccounts-integration' href='https://cgit.kde.org/kaccounts-integration.git'>kaccounts-integration</a> <a href='#kaccounts-integration' onclick='toggle("ulkaccounts-integration", this)'>[Show]</a></h3>
<ul id='ulkaccounts-integration' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/kaccounts-integration/b775e4c681d69bd3d0e94e240f4ed665c164a943'>Commit.</a> </li>
<li>Fix build when Akonadi is present. <a href='http://commits.kde.org/kaccounts-integration/bd35f0167769eba6ea6507e51f1fe774588d3acf'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>Fix crash/assert on opening files with similar paths. <a href='http://commits.kde.org/kate/43de035acc6b43ef4327f9c4ec71a0285446233c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347578'>#347578</a>. Code review <a href='https://git.reviewboard.kde.org/r/123793'>#123793</a></li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Update Italian translation of install-intro entity. <a href='http://commits.kde.org/kdelibs/ae3c2568a478017c64d1716d781f0617b9ac0771'>Commit.</a> </li>
<li>Update translation of entity install-intro.docbook. <a href='http://commits.kde.org/kdelibs/625e1711f0744e657732f9ffe0f9ea3c55f1abd4'>Commit.</a> </li>
<li>Fix entity install-intro.docbook with wrong link. <a href='http://commits.kde.org/kdelibs/ca4311613cbd06f4af051c2765a92c830931567b'>Commit.</a> </li>
<li>Implement "find previous" in KTextEdit. <a href='http://commits.kde.org/kdelibs/7c29b4a7ca0f8461d2d4b93639639e19928427f7'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix timeline corruption when using scroll wheel over an item in zoomed view. <a href='http://commits.kde.org/kdenlive/0410c3f53cc64f486e63b07ef40ed81dffe6313d'>Commit.</a> </li>
<li>Fix timeline corruption: Don't allow scrolling while resizing a clip. <a href='http://commits.kde.org/kdenlive/85afb7ccda961ac4987e9f49459cfefd0460f881'>Commit.</a> </li>
<li>Get ready for release. <a href='http://commits.kde.org/kdenlive/f977531b98da1632ea9272b6d5e1562ee3609b66'>Commit.</a> </li>
<li>Fix saving of custom transcoding settings. <a href='http://commits.kde.org/kdenlive/9aad1a70b002f0a0289ad25da35571152828e358'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348151'>#348151</a></li>
<li>Fix timeline corruption when zooming with mouse wheel over selected item. <a href='http://commits.kde.org/kdenlive/6f38d6d4ca2a45ce8afea55e3a7ccd32ac760ebd'>Commit.</a> </li>
<li>Color scheme fix for status bar. <a href='http://commits.kde.org/kdenlive/341314cc1852d392000d9adc5804dc0ad0b32294'>Commit.</a> </li>
<li>Development mode re-enabled. <a href='http://commits.kde.org/kdenlive/7fdcaa02ba9bb1b06e36ebcc9b71f63afc9ff392'>Commit.</a> </li>
<li>Fix custom profiles not displayed. <a href='http://commits.kde.org/kdenlive/cf70641750960cc06dbc2bfeb6e240ba284de8c9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347822'>#347822</a></li>
<li>Fix markers and guides corrupted on locales with comma separator. <a href='http://commits.kde.org/kdenlive/1195bb5fb222ad83b86151437cd883c9486add89'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347391'>#347391</a></li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Fix Bug 348360 - Creating Event from message, end time dropdown itemslist is truncated at around initial start time. <a href='http://commits.kde.org/kdepim/b1682e1088d2db5758fe6b06f37c2c459421f3b5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348360'>#348360</a></li>
<li>Improve it. <a href='http://commits.kde.org/kdepim/f9c27da91b37ab3efa65a026b117d4adc711e7e8'>Commit.</a> </li>
<li>Fix Bug 348360 - Creating Event from message, end time dropdown itemslist is truncated at around initial start time. <a href='http://commits.kde.org/kdepim/8cab399991db61a008c952f275c9280d727c3f51'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348360'>#348360</a></li>
<li>Const'ify. <a href='http://commits.kde.org/kdepim/a87c8176875028bdc81ce872070f052b68ab8f4c'>Commit.</a> </li>
<li>Improve it. <a href='http://commits.kde.org/kdepim/c810af7ad427902daae88c407240151810987ed4'>Commit.</a> </li>
<li>Improve gravatar support. Create application test. <a href='http://commits.kde.org/kdepim/06ab3bc3633b9e2e63b544621031ee6d094ae087'>Commit.</a> </li>
<li>Read/write config. <a href='http://commits.kde.org/kdepim/e42ee4aa01f83183ad99b7f641752981b5838d03'>Commit.</a> </li>
<li>Make it compile. <a href='http://commits.kde.org/kdepim/17612b42061e41f1554f2685fa640dcf9dcd9d10'>Commit.</a> </li>
<li>Clear cache when we disable gravatar support. <a href='http://commits.kde.org/kdepim/71ef2abaee22ac1ed97e37e2b021f0f4e76b4632'>Commit.</a> </li>
<li>Enable/disable checkbox. <a href='http://commits.kde.org/kdepim/f73dc8b9d3bd2db2064050c32d974dee4b39705a'>Commit.</a> </li>
<li>Fix enable/disable search button. <a href='http://commits.kde.org/kdepim/a8566101377b8a0c05722ca241386073db6eeef8'>Commit.</a> </li>
<li>Get gravatar. <a href='http://commits.kde.org/kdepim/28dbee50ee76e182553b9494c64ca6e1f84efefb'>Commit.</a> </li>
<li>Improve create gravatar. <a href='http://commits.kde.org/kdepim/7d2b53aedccec2e23b199d1010a7ef750ad1175e'>Commit.</a> </li>
<li>Use const'ref. <a href='http://commits.kde.org/kdepim/1ac134127aef5bd0c3f3fa31a868894bf3ff9da6'>Commit.</a> </li>
<li>Add new widget for update gravatar. <a href='http://commits.kde.org/kdepim/57c89f5e8d3b7f369babd38cc69ebc4e9df25b40'>Commit.</a> </li>
<li>Improve autotests. <a href='http://commits.kde.org/kdepim/52a1bb22e6af1d71370c1c391fb9e6c7c3d3ba3b'>Commit.</a> </li>
<li>Add widget. <a href='http://commits.kde.org/kdepim/e90ebb46e47bf51c7f9caf64e6c63ae4f287021a'>Commit.</a> </li>
<li>Add class for creating gravatar. <a href='http://commits.kde.org/kdepim/237c92087c16d44e40eeb4635faad852569ced26'>Commit.</a> </li>
<li>Move in own directory. <a href='http://commits.kde.org/kdepim/957e2c67de594d11c11d215054f1e229b942802c'>Commit.</a> </li>
<li>Const(ify. <a href='http://commits.kde.org/kdepim/ac5c02f87c1bde55259e1ee65dce8b4ac5ba904d'>Commit.</a> </li>
<li>Fix check potential phising. <a href='http://commits.kde.org/kdepim/8cd53136cb7963bc03584a739870a4234da36e78'>Commit.</a> </li>
<li>Move clear cache directly in gravatarcach. <a href='http://commits.kde.org/kdepim/cc834e4f4472bd198da5a03a33b52f529ccc183b'>Commit.</a> </li>
<li>Read gravatar config. <a href='http://commits.kde.org/kdepim/832fbe96becced96112fb9c2931a481d3de6de63'>Commit.</a> </li>
<li>Allow to specify cache size. <a href='http://commits.kde.org/kdepim/0e4a7e6b68fa95482f390ca41d01ff099525d3a9'>Commit.</a> </li>
<li>Allow to clear cache. <a href='http://commits.kde.org/kdepim/bb85baeb1816ca245a594990dffa1f9a2113e51c'>Commit.</a> </li>
<li>Use QCache . <a href='http://commits.kde.org/kdepim/fce4a88557e1f8cc0d32f4a549a6658f298f4ba2'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim/4373cd1f4d324c0908f6dc925ef7d48ae3c34fd4'>Commit.</a> </li>
<li>Fix spacing. <a href='http://commits.kde.org/kdepim/847ab4644b9548d6cf15ea12c93182dd5f9dafdd'>Commit.</a> </li>
<li>Fix Bug 347707 - email completion blacklist is not easily discoverable. <a href='http://commits.kde.org/kdepim/fac7eef62654323009537ca9c53633aac9005c80'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347707'>#347707</a></li>
<li>Fix menu. <a href='http://commits.kde.org/kdepim/453499b6f28fcd1b7182fef2609d3fda6c78b3ca'>Commit.</a> </li>
<li>Fix show gravatar when pixmap is in cache. <a href='http://commits.kde.org/kdepim/387c70757d8217f57ca1385446808ad4bda4a60e'>Commit.</a> </li>
<li>Comment debug. <a href='http://commits.kde.org/kdepim/9bcfaba143b0cf5d1804d2444651614a47e6e2ce'>Commit.</a> </li>
<li>Improve gravatar support. <a href='http://commits.kde.org/kdepim/4f868a6d8f97c239832351353d7db95c1091535c'>Commit.</a> </li>
<li>Fix detect network connection. <a href='http://commits.kde.org/kdepim/fb50a730f9241e03b1e0a2fa81042558b9712bc1'>Commit.</a> </li>
<li>Return pixmap. <a href='http://commits.kde.org/kdepim/0d023cee720d50748d84fd17a8ae1a65b2437629'>Commit.</a> </li>
<li>Fix save on disk. <a href='http://commits.kde.org/kdepim/1eea513135279c4963b300c13ef1565a64996274'>Commit.</a> </li>
<li>Use cache to avoid to load from disk or check server. <a href='http://commits.kde.org/kdepim/eb5088698a0f8bd50999b53c12d8f17c4289efc3'>Commit.</a> </li>
<li>Improve cache. <a href='http://commits.kde.org/kdepim/0d79914e3be3372a9eaa025f36029f674c0f46f8'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kdepim/d4a0c3de543ba709850c91fd272133fc97c312b2'>Commit.</a> </li>
<li>We need a cache. <a href='http://commits.kde.org/kdepim/52598d7c52b2150315795b72ee8a3390f9b0f015'>Commit.</a> </li>
<li>Don't start it when we can't start job. <a href='http://commits.kde.org/kdepim/255befa5c6042883c59cb806adb4b0c3d7d7b675'>Commit.</a> </li>
<li>Don't try to resolv url when network is not connected. <a href='http://commits.kde.org/kdepim/aecc18b9dd5eb2a0a4067d42c7b63ae0dee3772b'>Commit.</a> </li>
<li>We need to update item. <a href='http://commits.kde.org/kdepim/5543d57c2407c9b91ffef4990fececba750d75ac'>Commit.</a> </li>
<li>Return pixmap. <a href='http://commits.kde.org/kdepim/30801f952d2fafe7e457eb1b288b643ef9d1985a'>Commit.</a> </li>
<li>Improve gravatar support. <a href='http://commits.kde.org/kdepim/4e592bd9ef98c360629600bf20779718e6ed927b'>Commit.</a> </li>
<li>Add config widget. <a href='http://commits.kde.org/kdepim/d761c47b2374754c2379a104b22f47c8a3a51ea6'>Commit.</a> </li>
<li>Allow to load/save settings. <a href='http://commits.kde.org/kdepim/dae764c3dfe547b1db1b1d209de9989a12a64a41'>Commit.</a> </li>
<li>Add dialog box to get gravatar pixmap. <a href='http://commits.kde.org/kdepim/44c643682de28a3490bbd9e02dc5f51f0f0dec12'>Commit.</a> </li>
<li>Show gravatar in header. <a href='http://commits.kde.org/kdepim/70c9120e520bede0dcec386c98aeab44e4b62a3e'>Commit.</a> See bug <a href='https://bugs.kde.org/347441'>#347441</a></li>
<li>Now we can see gravatar. <a href='http://commits.kde.org/kdepim/b6c1a3adb1dd3ebc5ea67d4839d98c6bfa5d0373'>Commit.</a> </li>
<li>Add test application. <a href='http://commits.kde.org/kdepim/e5f54da84954ea661532425abd651491f835c5fe'>Commit.</a> </li>
<li>Improve widget. <a href='http://commits.kde.org/kdepim/6e2ba928fbc8cc9c98f2a19e290b47addc2efc41'>Commit.</a> </li>
<li>Add widget. <a href='http://commits.kde.org/kdepim/8d45769374fd6e34220e505bdc9f908c9724ee67'>Commit.</a> </li>
<li>Allow to use default value. <a href='http://commits.kde.org/kdepim/21eaf3c05fe7b9f9cf478525ff4871005521f984'>Commit.</a> </li>
<li>Add more unittest. <a href='http://commits.kde.org/kdepim/f8c97ce672acc68e416f2ad7a1ca3a444d26c42a'>Commit.</a> </li>
<li>Add code to get pixmap. <a href='http://commits.kde.org/kdepim/8740b4ab3f35bc166600eaeb3639b5e5e230175b'>Commit.</a> </li>
<li>Add support for define size. <a href='http://commits.kde.org/kdepim/1d427e91bc1e608515781a334c896fc4f73c97f9'>Commit.</a> </li>
<li>Return hash value. <a href='http://commits.kde.org/kdepim/ab8ab2e7a78227d68dc43472bf0af40d904a3360'>Commit.</a> </li>
<li>Not necessary to export it. <a href='http://commits.kde.org/kdepim/538fc83be87d42ea88757276e6cb95d1ddbf20b4'>Commit.</a> </li>
<li>Need for the future. <a href='http://commits.kde.org/kdepim/7a970b3feeb0fa05fcc412ece2b24d4f7a6a4723'>Commit.</a> </li>
<li>Add more check. <a href='http://commits.kde.org/kdepim/0678aa19a689225148fd3a0cf493af9e69eecefc'>Commit.</a> </li>
<li>Improve gravatar support. <a href='http://commits.kde.org/kdepim/f234893e3668e85a8e1afa3979073b4cdab80a99'>Commit.</a> </li>
<li>Move gui class to gui directory. <a href='http://commits.kde.org/kdepim/3923d109b983f5808a5162b83ddd0813f0f18dbf'>Commit.</a> </li>
<li>Minor. <a href='http://commits.kde.org/kdepim/bb7ce5aad3c14202fc5d457fc43c92650baa931f'>Commit.</a> </li>
<li>Don't detach them. <a href='http://commits.kde.org/kdepim/43d71a954d57e231bc06186317c37486dfa08e97'>Commit.</a> </li>
<li>Start to resolve url. <a href='http://commits.kde.org/kdepim/691bd8a6b3dc31cfb7edd97cebf2b3c2c6883b8b'>Commit.</a> </li>
<li>Improve it. <a href='http://commits.kde.org/kdepim/44ac13478cb070e40e72077771372f6afe436376'>Commit.</a> </li>
<li>Don't install it. <a href='http://commits.kde.org/kdepim/7c0720ce228cee742d279498fc3bdc180f043a2d'>Commit.</a> </li>
<li>Not necessary for the moment. <a href='http://commits.kde.org/kdepim/6e74fc57eb7b13bd1130d2cb0520be9cf6bf9047'>Commit.</a> </li>
<li>Start to fix gravatar support. <a href='http://commits.kde.org/kdepim/efd54e9bf585cd278b39341e775f964d2f1e9043'>Commit.</a> See bug <a href='https://bugs.kde.org/347441'>#347441</a></li>
<li>Create a console indicator. <a href='http://commits.kde.org/kdepim/c3322cd3bbf01240811c6b881f3487ea273f1905'>Commit.</a> </li>
<li>Use directly mPimSettingsBackupRestoreUI. <a href='http://commits.kde.org/kdepim/43c99c21af8a6ca96e16feab4ddc0b61668b2fd4'>Commit.</a> </li>
<li>Start to use pimsettingsbackuprestoreui.h. <a href='http://commits.kde.org/kdepim/18b0f7b1747fcc1865365cbbfa7a21e284e67bc8'>Commit.</a> </li>
<li>Move CMakeLists.txt to gui directory. <a href='http://commits.kde.org/kdepim/157e647349534788634e3a922e693942034d8fe6'>Commit.</a> </li>
<li>Move in own directory. <a href='http://commits.kde.org/kdepim/d905b9162843ad0edea9980c815c56183d9bf77b'>Commit.</a> </li>
<li>Export symbole. <a href='http://commits.kde.org/kdepim/728aa97c06b312e80a6f48fa96213cdcaf8b869a'>Commit.</a> </li>
<li>Clean forward declaration. <a href='http://commits.kde.org/kdepim/518cdc6f1f74268bdfd34b4711629da85e37b919'>Commit.</a> </li>
<li>Remove some gui code. <a href='http://commits.kde.org/kdepim/5ccc32ecd814e93dd0093fd0c988d32d217cd549'>Commit.</a> </li>
<li>Add more method in importexportprogressindicatorbase. <a href='http://commits.kde.org/kdepim/4c2a1b5748b2de61e1ccb73de300bcf1eaab133a'>Commit.</a> </li>
<li>Use importexportprogressindicatorgui.h when we use gui application. <a href='http://commits.kde.org/kdepim/39aeb2d39e0462a36d1d3e6a057d6c3626840a69'>Commit.</a> </li>
<li>Use importexportprogressindicatorbase.h. <a href='http://commits.kde.org/kdepim/3eadab9ffe7f972008f805b745f2baa0ed651c58'>Commit.</a> </li>
<li>Clean up code. <a href='http://commits.kde.org/kdepim/a2fa909c2385af0386b15410904efcf687e56d52'>Commit.</a> </li>
<li>Create progressindicator base. <a href='http://commits.kde.org/kdepim/63b8e0865424ee15d8346bd5ff434182a4efc54f'>Commit.</a> </li>
<li>Move in own directory. <a href='http://commits.kde.org/kdepim/6fc56578d2a931f84c0fb5af88a42e90099fafce'>Commit.</a> </li>
<li>Check is map is empty. <a href='http://commits.kde.org/kdepim/0d92cc2bf52755dd7e04715f3ae45b1374de8875'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/kdepim/5b8e4947d245c4b6dac0b6f10d7a1ff3f5a616bb'>Commit.</a> </li>
<li>Create a lib for the future. <a href='http://commits.kde.org/kdepim/f445c7503cda629872c184d69a648698e7daf214'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/8857aabc40b5468edd70ede0ca541bb804d1bcb3'>Commit.</a> </li>
</ul>
<h3><a name='kdepimlibs' href='https://cgit.kde.org/kdepimlibs.git'>kdepimlibs</a> <a href='#kdepimlibs' onclick='toggle("ulkdepimlibs", this)'>[Show]</a></h3>
<ul id='ulkdepimlibs' style='display: none'>
<li>Start to add support for external logo/picture from contact. <a href='http://commits.kde.org/kdepimlibs/ede66996edb51e8742da554d58892503f3e61ae6'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepimlibs/4e94dc0f0319e22f8bdfcc1afe2b190491897e5f'>Commit.</a> </li>
<li>Make pixmap as static here. <a href='http://commits.kde.org/kdepimlibs/5e340773b00f47d964525cfc0d7373ad93a2912d'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Show]</a></h3>
<ul id='ulkgpg' style='display: none'>
<li>Bump version number for KDE Applications 15.04.2. <a href='http://commits.kde.org/kgpg/ed18cef3bda2bf2983520b608813bc2a64e46026'>Commit.</a> </li>
<li>Some container optimizations. <a href='http://commits.kde.org/kgpg/d00b9cac79d22794e5c104397a26f6f86c0079ae'>Commit.</a> </li>
<li>Disable rich text in editor. <a href='http://commits.kde.org/kgpg/5230123ab58170aad85e7e3b142aea681ce24dfe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346983'>#346983</a></li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Show]</a></h3>
<ul id='ulkig' style='display: none'>
<li>Remove Magic Booleans in CoordinateValidator. <a href='http://commits.kde.org/kig/084c39165d441a2472ca45474746d8cb8cfedc65'>Commit.</a> </li>
<li>Fixed Several Layout Issues with KigInputDialog. <a href='http://commits.kde.org/kig/4b7f66458fc46b2d19af2523738136b85aaa7ca0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348125'>#348125</a></li>
</ul>
<h3><a name='kmix' href='https://cgit.kde.org/kmix.git'>kmix</a> <a href='#kmix' onclick='toggle("ulkmix", this)'>[Show]</a></h3>
<ul id='ulkmix' style='display: none'>
<li>Add missing translation domain definition for KF5 build. <a href='http://commits.kde.org/kmix/bdf3371706d53c21198a99f10ab3b3ea1830a013'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>"Save Output As" is a "Save" dialog. <a href='http://commits.kde.org/konsole/911129f0527dddff7a965b28846ec9bee3d85f73'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347918'>#347918</a>. Code review <a href='https://git.reviewboard.kde.org/r/123844'>#123844</a></li>
</ul>
<h3><a name='ktp-accounts-kcm' href='https://cgit.kde.org/ktp-accounts-kcm.git'>ktp-accounts-kcm</a> <a href='#ktp-accounts-kcm' onclick='toggle("ulktp-accounts-kcm", this)'>[Show]</a></h3>
<ul id='ulktp-accounts-kcm' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-accounts-kcm/f4d68219d1c257dbc06d81c9f914586125e693ef'>Commit.</a> </li>
</ul>
<h3><a name='ktp-approver' href='https://cgit.kde.org/ktp-approver.git'>ktp-approver</a> <a href='#ktp-approver' onclick='toggle("ulktp-approver", this)'>[Show]</a></h3>
<ul id='ulktp-approver' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-approver/5a563247d087785cd7db7b6edb81941a368a9b9b'>Commit.</a> </li>
</ul>
<h3><a name='ktp-auth-handler' href='https://cgit.kde.org/ktp-auth-handler.git'>ktp-auth-handler</a> <a href='#ktp-auth-handler' onclick='toggle("ulktp-auth-handler", this)'>[Show]</a></h3>
<ul id='ulktp-auth-handler' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-auth-handler/5a3d89ad448408a2c8070aa9313e9aa46e8c8011'>Commit.</a> </li>
</ul>
<h3><a name='ktp-common-internals' href='https://cgit.kde.org/ktp-common-internals.git'>ktp-common-internals</a> <a href='#ktp-common-internals' onclick='toggle("ulktp-common-internals", this)'>[Show]</a></h3>
<ul id='ulktp-common-internals' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-common-internals/b1ba41185ad5e35aab975408b6b8490df5098fd7'>Commit.</a> </li>
</ul>
<h3><a name='ktp-contact-list' href='https://cgit.kde.org/ktp-contact-list.git'>ktp-contact-list</a> <a href='#ktp-contact-list' onclick='toggle("ulktp-contact-list", this)'>[Show]</a></h3>
<ul id='ulktp-contact-list' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-contact-list/516eb9bd78ab3340be4b7100edb93335e82ae226'>Commit.</a> </li>
</ul>
<h3><a name='ktp-contact-runner' href='https://cgit.kde.org/ktp-contact-runner.git'>ktp-contact-runner</a> <a href='#ktp-contact-runner' onclick='toggle("ulktp-contact-runner", this)'>[Show]</a></h3>
<ul id='ulktp-contact-runner' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-contact-runner/e1ebfea48df9c1ac5cbb70435b397389ed0e20e9'>Commit.</a> </li>
</ul>
<h3><a name='ktp-desktop-applets' href='https://cgit.kde.org/ktp-desktop-applets.git'>ktp-desktop-applets</a> <a href='#ktp-desktop-applets' onclick='toggle("ulktp-desktop-applets", this)'>[Show]</a></h3>
<ul id='ulktp-desktop-applets' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-desktop-applets/b0d993725d8557764edf9f4962eba82dbd4c40b6'>Commit.</a> </li>
<li>[contactlist] Remove unused component. <a href='http://commits.kde.org/ktp-desktop-applets/e10e4356d16963bc073593c916d628f78bcfa202'>Commit.</a> </li>
</ul>
<h3><a name='ktp-filetransfer-handler' href='https://cgit.kde.org/ktp-filetransfer-handler.git'>ktp-filetransfer-handler</a> <a href='#ktp-filetransfer-handler' onclick='toggle("ulktp-filetransfer-handler", this)'>[Show]</a></h3>
<ul id='ulktp-filetransfer-handler' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-filetransfer-handler/0fff7042de230a07c37103a385730b96c7fe39bc'>Commit.</a> </li>
</ul>
<h3><a name='ktp-kded-module' href='https://cgit.kde.org/ktp-kded-module.git'>ktp-kded-module</a> <a href='#ktp-kded-module' onclick='toggle("ulktp-kded-module", this)'>[Show]</a></h3>
<ul id='ulktp-kded-module' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-kded-module/ce69bbd0a9a867d9df3e7e9ca29adb33c3010e1e'>Commit.</a> </li>
</ul>
<h3><a name='ktp-send-file' href='https://cgit.kde.org/ktp-send-file.git'>ktp-send-file</a> <a href='#ktp-send-file' onclick='toggle("ulktp-send-file", this)'>[Show]</a></h3>
<ul id='ulktp-send-file' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-send-file/3e7f509200089b8cf4dfb831ed1f056b7ece2a30'>Commit.</a> </li>
</ul>
<h3><a name='ktp-text-ui' href='https://cgit.kde.org/ktp-text-ui.git'>ktp-text-ui</a> <a href='#ktp-text-ui' onclick='toggle("ulktp-text-ui", this)'>[Show]</a></h3>
<ul id='ulktp-text-ui' style='display: none'>
<li>Make log viewer usable in multiple instances. <a href='http://commits.kde.org/ktp-text-ui/50a901a06733299af2238fc4c4d26d2f8052dae0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123907'>#123907</a>. Fixes bug <a href='https://bugs.kde.org/346395'>#346395</a></li>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/ktp-text-ui/b535da5c937e13098b35c90fe28ccc7a201b906e'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Version bump to 0.21.2 (stable release). <a href='http://commits.kde.org/marble/38a11f75c20e284200fc14219a185fcb104c6686'>Commit.</a> </li>
<li>Fix saving APRS plugin settings. <a href='http://commits.kde.org/marble/1e8381ff2bd3cc61cf3faf3adde9d3e9189d0c7e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348207'>#348207</a></li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Rename appdata to match .desktop filename. <a href='http://commits.kde.org/okteta/3b88f4bb42336ec38e08f1dffbd268105182604d'>Commit.</a> </li>
<li>Bump version to 0.16.2. <a href='http://commits.kde.org/okteta/60756ac6cdd81f43145048fec447d94a04bb92e6'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Fix embed mode detection. <a href='http://commits.kde.org/okular/223092aa0e1fd5fd1b48a34702d2102fdb1acccf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123680'>#123680</a></li>
</ul>
<h3><a name='signon-kwallet-extension' href='https://cgit.kde.org/signon-kwallet-extension.git'>signon-kwallet-extension</a> <a href='#signon-kwallet-extension' onclick='toggle("ulsignon-kwallet-extension", this)'>[Show]</a></h3>
<ul id='ulsignon-kwallet-extension' style='display: none'>
<li>Bump version to 15.04.2. <a href='http://commits.kde.org/signon-kwallet-extension/2ff0bc7429db679f0b699ad4ce4ddd14f8f84292'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix 'No color dialog shown from context menu of fork/join widget'. <a href='http://commits.kde.org/umbrello/0cb98d6978030e577e771d921e5e4da6f3208668'>Commit.</a> </li>
<li>Fix 'No colour change for fork/join widgets possible'. <a href='http://commits.kde.org/umbrello/8e6e605c0d62f5ba1ccf1d5b071e420c9ee726f4'>Commit.</a> </li>
<li>Fix broken layout of general properties dialog page of components on deployment diagrams. <a href='http://commits.kde.org/umbrello/bb94b057a294204c168b3ddbbe398e99b73c87de'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347689'>#347689</a></li>
<li>Remove duplicated removal of scene widgets, which is done in the UMLScene destructor. <a href='http://commits.kde.org/umbrello/8978beee27c1bfb75bdcd6038825ffc6ebca7860'>Commit.</a> </li>
<li>Fix memory leak in UMLApp::executeCommand() not deleting undo command with disabled undo stack. <a href='http://commits.kde.org/umbrello/2cb3559c385a1de73e0b0e36056d32fbfdb7957f'>Commit.</a> See bug <a href='https://bugs.kde.org/347498'>#347498</a></li>
<li>Remove unused code in class ListPopupMenu. <a href='http://commits.kde.org/umbrello/81ec2640881ee71572b672ddf109bf9fdbfeb64f'>Commit.</a> </li>
<li>Remove obsolate TODO in UMLView::umlScene(). <a href='http://commits.kde.org/umbrello/91fbc4db4d3a3f11503c766370003cc98af5c4bb'>Commit.</a> </li>
<li>Remove obsolate UMLScene* variable in UMLScene constructor. <a href='http://commits.kde.org/umbrello/ff3c0b71b89c581a98efbd175b96b12eb43acae2'>Commit.</a> </li>
<li>Fix memory leak in class UMLAppPrivate not deleting class member logWindow. <a href='http://commits.kde.org/umbrello/b2c94b0003eec73ad8534ed84aea01936acbc582'>Commit.</a> See bug <a href='https://bugs.kde.org/347498'>#347498</a></li>
<li>Fix memory leaks in class UMLDoc by not deleting class members and not disconnecting signal/slot connections. <a href='http://commits.kde.org/umbrello/da87d376a0cad8e12e0ae862af43e4f66b30ed60'>Commit.</a> See bug <a href='https://bugs.kde.org/347498'>#347498</a></li>
<li>Fix memory leak in class UMLView not deleting UMLScene instance. <a href='http://commits.kde.org/umbrello/c765a558338badd29beee430e757921d62b8a4f7'>Commit.</a> See bug <a href='https://bugs.kde.org/347498'>#347498</a></li>
<li>Fix memory leak in class UMLScene not deleting m_layoutGrid class member. <a href='http://commits.kde.org/umbrello/9188fd0dc7b4b07b4043cf02ed14113787c55ba4'>Commit.</a> See bug <a href='https://bugs.kde.org/347498'>#347498</a></li>
<li>Fix memory leaks reported by valgrind in class UMLApp by not deleting class members and not disconnecting signal/slot connections. <a href='http://commits.kde.org/umbrello/cbe2312b8fb7a3e21edb9de99d2f996c07dd12a0'>Commit.</a> See bug <a href='https://bugs.kde.org/347498'>#347498</a></li>
<li>Fix memory leak in class FindDialog caused by not disconnecting signal/slot connection. <a href='http://commits.kde.org/umbrello/9d8d32498916210721215b27de650760ec7375ad'>Commit.</a> See bug <a href='https://bugs.kde.org/347489'>#347489</a></li>
<li>Fix memory leak not freeing UMLApp instance. <a href='http://commits.kde.org/umbrello/fe32a49862d7203f450c51c30025b99fcf80c748'>Commit.</a> See bug <a href='https://bugs.kde.org/347498'>#347498</a></li>
<li>Remove outdated kdevelop3 project files, kdevelop4 is able to open CMakeLists.txt directly. <a href='http://commits.kde.org/umbrello/2ec5e2e49a4cdc7457a1ef7db888224108f5a8e8'>Commit.</a> </li>
<li>Doc spelling fix. <a href='http://commits.kde.org/umbrello/dbdaf16503cb17b377c85ed25bddca540792936e'>Commit.</a> </li>
<li>Fix 'Crash when copying actor of use case to collaboration diagram'. <a href='http://commits.kde.org/umbrello/5a05d35e2e809899b2f65bea9fb44681042d10f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345571'>#345571</a></li>
<li>Refactor code checking valid uml object type for a given diagram type into a new function Moodel_Utils::typeIsAllowedInDiagram(). <a href='http://commits.kde.org/umbrello/66ed34766b015c56a4f1ed880d614a9127c44697'>Commit.</a> See bug <a href='https://bugs.kde.org/345571'>#345571</a></li>
</ul>