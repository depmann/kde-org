---
title: Plasma 5.24.3 complete changelog
version: 5.24.3
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ KStyle: center QTabBar custom tab buttons vertically in vertical tabs. [Commit.](http://commits.kde.org/breeze/8a6426dfc9640367dc25454b8f26edd3bdac67c5) Fixes bug [#447315](https://bugs.kde.org/447315)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: Do not emit about upgradeable packages that were just created. [Commit.](http://commits.kde.org/discover/4a874a638d24298811476036e23ff3eb77214865) 
+ Flatpak: Use the sources map to check if a resource is already being used. [Commit.](http://commits.kde.org/discover/3cb9e7b8e121b9d360f5f3f9354877f26a667c38) 
+ Set textFormat in Label to StyledText. [Commit.](http://commits.kde.org/discover/25e928aae15e9ba44cc35edb30cca647e3c6b96f) 
+ Fix build by explicitly creating a QUrl from QString. [Commit.](http://commits.kde.org/discover/9365486f656e166f29476c6ac1a67adbb31dc838) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Make sure to create the wallet folder before using it. [Commit.](http://commits.kde.org/drkonqi/c98f842926bd9c24f2f0e08988a36a0af2c6906a) Fixes bug [#446925](https://bugs.kde.org/446925)
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Fix cmake policy CMP0115 warning. [Commit.](http://commits.kde.org/kde-gtk-config/7668a6adc744875f8bc658f887aeff7ea2ea6e4f) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Fix up help paths. [Commit.](http://commits.kde.org/kinfocenter/53c021fccb1f49e20a161b4e3db3fdbb67985b40) Fixes bug [#450918](https://bugs.kde.org/450918)
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Revert "KCM: Workaround unknown Qt issue that causes the revert dialog to be invisible". [Commit.](http://commits.kde.org/kscreen/9509c64a8ef0a4bc06a33eb8823a4cb4181ddaa9) 
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Fix kdebugsettings categories file. [Commit.](http://commits.kde.org/kwayland-server/fc0283f56d8580c307169f9d4bc9b1f9f7f16a1f) 
+ Linuxdmabuf: Add unistd.h include. [Commit.](http://commits.kde.org/kwayland-server/93f4201388cda6e8999374de30e96e35ea5d4b1a) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Backends/drm: don't change the configuration while KWin is terminating. [Commit.](http://commits.kde.org/kwin/e7a88a1f451b4c34c6c40ea945f3f2caccc5ae31) 
+ Backends/drm: wait for pending pageflips before doing a modeset. [Commit.](http://commits.kde.org/kwin/6ec4efff3ca65d7c40fcb77580a86846d23ba57f) 
+ Output changes: handle to-be-enabled outputs first. [Commit.](http://commits.kde.org/kwin/696c3d65df8122f9fb7cb2d02e7da704913ed190) 
+ Platform: check all outputs, not only enabled ones for the enabled flag. [Commit.](http://commits.kde.org/kwin/dbe2a7ed36db654f5ad594504646804dc7839f7a) See bug [#450721](https://bugs.kde.org/450721)
+ Backends/drm: fix recording with direct scanout. [Commit.](http://commits.kde.org/kwin/b7a3a0283de6eb3ae816f9f52993c06580809098) Fixes bug [#450943](https://bugs.kde.org/450943)
+ Backends/drm: fix multi gpu. [Commit.](http://commits.kde.org/kwin/85558234e1d86dc084cd2776cdb22799eaa9254b) Fixes bug [#450737](https://bugs.kde.org/450737)
+ Backends/drm: fix format choosing. [Commit.](http://commits.kde.org/kwin/1d058a79d1b73091e4427e106eb4b98d13c57bb4) Fixes bug [#450779](https://bugs.kde.org/450779)
+ Inputmethod: fix placing the virtual keyboard at the bottom. [Commit.](http://commits.kde.org/kwin/f70a2c79bbe4eb5c0ff9361ca89719cbbe0e6e9d) 
+ Revert "Remove mysterious s_cursorUpdateBlocking boolean flag in pointer_input.cpp". [Commit.](http://commits.kde.org/kwin/bac4f4ed62017bb4aeb81078f5ec2cd915af5de4) Fixes bug [#449273](https://bugs.kde.org/449273)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Fixed computational bug for bar chart spacing. [Commit.](http://commits.kde.org/libksysguard/309e7aa8a6ce58569c49713247a507210c41976c) Fixes bug [#449868](https://bugs.kde.org/449868)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Kcms/touchpad: Remove weird Q_EMIT changed(false) in resizeEvent. [Commit.](http://commits.kde.org/plasma-desktop/5cf2788fdd18cb8604fd172535b044c6cfd370fc) See bug [#449843](https://bugs.kde.org/449843)
{{< /details >}}

{{< details title="plasma-integration" href="https://commits.kde.org/plasma-integration" >}}
+ [KDEPlatformFileDialog] Don't do stat if baseUrl didn't change. [Commit.](http://commits.kde.org/plasma-integration/46a8c287789dafd257c0119196b4c24faae1f068) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ [kcm] Update device combobox when current device changes externally. [Commit.](http://commits.kde.org/plasma-pa/9dd929c3489aa92357a60f241fed6f348eda6bfa) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Kcms/colors: Implement radio button layouts better. [Commit.](http://commits.kde.org/plasma-workspace/911ac8278a87a84ecb171e9a4e28d831b24644b8) 
+ Kcms/colors: fix spacing between radio buttons and content. [Commit.](http://commits.kde.org/plasma-workspace/d7469d570c5d331419f64293d86b7fa06e215b55) 
+ [Battery Monitor] Only show charge threshold hint for power supply batteries. [Commit.](http://commits.kde.org/plasma-workspace/b2cdc6f8ba663e137ed54f24d2ea8b1af94bf299) Fixes bug [#451161](https://bugs.kde.org/451161)
+ [Icons KCM] Give measure delegate a text. [Commit.](http://commits.kde.org/plasma-workspace/13fd1a27435aaa1a21b95534cd61e88755ae73fe) 
+ Wallpapers: Sort BackgroundListModel by title. [Commit.](http://commits.kde.org/plasma-workspace/eb9a32b5778e914e5ae056b10071dba04017e794) 
+ Applets/digital-clock: Fix `Qt.formatDateTime` returns different date when minute changes. [Commit.](http://commits.kde.org/plasma-workspace/788f52fc9f33dd3e38112815deeb27886ce69d93) Fixes bug [#436796](https://bugs.kde.org/436796)
+ Applets/systray: align applet labels with differing line counts in hidden view. [Commit.](http://commits.kde.org/plasma-workspace/5727362e3666f3d74c641665f5fb86b7d1434216) Fixes bug [#438347](https://bugs.kde.org/438347)
+ Show panel config above other windows. [Commit.](http://commits.kde.org/plasma-workspace/83400745d82bfe69b3fe176677931fa8ec86763c) Fixes bug [#450794](https://bugs.kde.org/450794)
+ Use current accent colour to set ColorDialog object in colour picker. [Commit.](http://commits.kde.org/plasma-workspace/bc9acf7079bf5af439a802cc238f414188560d4c) 
+ SystemDialog: Allow accepting the dialogs with the keyboard. [Commit.](http://commits.kde.org/plasma-workspace/b1577e100136e5b3446fcfc74d76c61c674ceaca) Fixes bug [#450223](https://bugs.kde.org/450223)
+ Applets/systemtray: Do not open context menu on mouse pressed for SNI. [Commit.](http://commits.kde.org/plasma-workspace/a9d5ea2afd33ee4fb51b6caf0af5a902a3d81352) Fixes bug [#409768](https://bugs.kde.org/409768)
+ Revert "Fix overdraw on Wayland". [Commit.](http://commits.kde.org/plasma-workspace/be50191b437231d42709163f102418c06da02538) 
+ Startkde: Forward stdout/stderr of started processes. [Commit.](http://commits.kde.org/plasma-workspace/b525767c86cd90b21d5079a4642f01b64941fd79) 
+ SDDM theme: stop eliding people's names so aggressively. [Commit.](http://commits.kde.org/plasma-workspace/9b307559ef19a773576c745d65b337ff287d475f) Fixes bug [#450673](https://bugs.kde.org/450673)
+ Applets/digital-clock: Word-wrap date string for desktop representation. [Commit.](http://commits.kde.org/plasma-workspace/71193cd23eb48af8db7a548a2a23917ced308864) Fixes bug [#450632](https://bugs.kde.org/450632)
+ Wrap completely the invariants timer in NDEBUG. [Commit.](http://commits.kde.org/plasma-workspace/6827051b1cabd4bf43ff757868bb6f959fabc62c) 
+ ScreenPool as the source of truth of QScreen info. [Commit.](http://commits.kde.org/plasma-workspace/8e0524f3469cda96009598b2a3d4f78b112501e0) 
+ Always ensure there is an user selected. [Commit.](http://commits.kde.org/plasma-workspace/3e32e78ece5f416c0d2b38a0951232ce86686a92) Fixes bug [#450182](https://bugs.kde.org/450182)
+ Prevent panel going out of screen boundaries. [Commit.](http://commits.kde.org/plasma-workspace/f7a1a5dd04228adaa096f1ff4c768c3b8e158b5d) See bug [#438114](https://bugs.kde.org/438114)
+ Applets/clipboard: Focus on text area when transition is done. [Commit.](http://commits.kde.org/plasma-workspace/f476c1821bb25262d9399bc8455c4b5174d3eb89) 
+ Applets/clipboard: Fix highlight after exiting edit mode. [Commit.](http://commits.kde.org/plasma-workspace/b55521b89ddad32aaecdf47fb01cb0149412461f) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Improved backlight devices selection. [Commit.](http://commits.kde.org/powerdevil/c6b7c714af1d25d04bc820bc5ce554241999901f) Fixes bug [#399646](https://bugs.kde.org/399646)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ App/SettingsBase: Fix systemsettings unable to start when missing plugin. [Commit.](http://commits.kde.org/systemsettings/d2a77e1c0ae5068e3ff1acf8056f05b7fa66cf61) Fixes bug [#451054](https://bugs.kde.org/451054)
+ ModuleView: Simplify and fix custom headers logic. [Commit.](http://commits.kde.org/systemsettings/9a35fd4c93a7121c184442b60138e1a457a2c40b) 
{{< /details >}}

