------------------------------------------------------------------------
r836887 | rahn | 2008-07-23 10:54:24 +0200 (Wed, 23 Jul 2008) | 3 lines

- Adding MarbleControlBox patch by Claudiu Covaci to fix the item order.


------------------------------------------------------------------------
r836896 | rahn | 2008-07-23 11:18:26 +0200 (Wed, 23 Jul 2008) | 4 lines

-Disabling the sorting stuff until claudiu has sorted out the selection
issue.


------------------------------------------------------------------------
r836907 | rahn | 2008-07-23 11:57:07 +0200 (Wed, 23 Jul 2008) | 5 lines

- Fixing Order and Selection in MarbleControlBox
- Fixing uninitialized Canvas on first start.
- Fixing MarbleStarsPlugin when viewport yheight is bigger than width.


------------------------------------------------------------------------
r837387 | chehrlic | 2008-07-24 16:58:02 +0200 (Thu, 24 Jul 2008) | 2 lines

win32 qt-only compile++
no need to port it to branch -did a similar fix there some days go
------------------------------------------------------------------------
r837394 | rahn | 2008-07-24 17:19:14 +0200 (Thu, 24 Jul 2008) | 7 lines

- Backport of Claudiu Covaci's 

SVN commit 837214 by covaci:

Fix: re-enable zoom buttons if newly selected map supports higher zoom levels.


------------------------------------------------------------------------
r837399 | chehrlic | 2008-07-24 17:27:25 +0200 (Thu, 24 Jul 2008) | 1 line

update to 0.6.0
------------------------------------------------------------------------
r837576 | rahn | 2008-07-25 08:21:18 +0200 (Fri, 25 Jul 2008) | 3 lines

- Fixing marble translation for map theme names as requested by Burkhard Lück.


------------------------------------------------------------------------
r837587 | rahn | 2008-07-25 08:56:07 +0200 (Fri, 25 Jul 2008) | 3 lines

- trying to fix the descpription translation ( doesn't work though )


------------------------------------------------------------------------
r837841 | ilic | 2008-07-25 22:51:00 +0200 (Fri, 25 Jul 2008) | 1 line

Exclude CDATA markers when extracting from XML data files. (bport: 837838)
------------------------------------------------------------------------
r837931 | metellius | 2008-07-26 06:00:45 +0200 (Sat, 26 Jul 2008) | 2 lines

Fixing kiten crashing at exit.

------------------------------------------------------------------------
r838069 | sengels | 2008-07-26 18:19:51 +0200 (Sat, 26 Jul 2008) | 1 line

document additional installation possibilities (backport)
------------------------------------------------------------------------
r838552 | mlaurent | 2008-07-28 09:13:55 +0200 (Mon, 28 Jul 2008) | 3 lines

Backport:
Fix meme leak

------------------------------------------------------------------------
r838561 | mlaurent | 2008-07-28 09:36:43 +0200 (Mon, 28 Jul 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r838620 | rahn | 2008-07-28 12:55:09 +0200 (Mon, 28 Jul 2008) | 4 lines

- Backports from trunk. 
- painting lines on screen coordinates -- not inbetween


------------------------------------------------------------------------
r838937 | scripty | 2008-07-29 06:45:21 +0200 (Tue, 29 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r839459 | scripty | 2008-07-30 07:35:39 +0200 (Wed, 30 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r839650 | apol | 2008-07-30 15:46:47 +0200 (Wed, 30 Jul 2008) | 2 lines

Fixed crash when entering an unsupported bvar.

------------------------------------------------------------------------
r840086 | cniehaus | 2008-07-31 12:40:33 +0200 (Thu, 31 Jul 2008) | 1 line

Typofixes
------------------------------------------------------------------------
r840101 | cniehaus | 2008-07-31 13:09:30 +0200 (Thu, 31 Jul 2008) | 1 line

update the README-file
------------------------------------------------------------------------
r840105 | cniehaus | 2008-07-31 13:28:36 +0200 (Thu, 31 Jul 2008) | 1 line

backport commit 840104
------------------------------------------------------------------------
r840437 | mlaurent | 2008-08-01 09:00:59 +0200 (Fri, 01 Aug 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r840439 | mlaurent | 2008-08-01 09:09:58 +0200 (Fri, 01 Aug 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r840446 | mlaurent | 2008-08-01 09:34:05 +0200 (Fri, 01 Aug 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r840451 | mlaurent | 2008-08-01 09:40:11 +0200 (Fri, 01 Aug 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r840453 | mlaurent | 2008-08-01 09:43:14 +0200 (Fri, 01 Aug 2008) | 3 lines

Bakport:
Fix mem leak

------------------------------------------------------------------------
r840607 | mlaurent | 2008-08-01 12:58:08 +0200 (Fri, 01 Aug 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r840638 | rahn | 2008-08-01 13:57:21 +0200 (Fri, 01 Aug 2008) | 3 lines

- Making Marble sources more lean


------------------------------------------------------------------------
r840716 | rahn | 2008-08-01 17:57:12 +0200 (Fri, 01 Aug 2008) | 3 lines

- Fixing crash introduced by lmontel's latest patch


------------------------------------------------------------------------
r840740 | aacid | 2008-08-01 19:20:01 +0200 (Fri, 01 Aug 2008) | 3 lines

Backport r840739 kgeography/trunk/KDE/kdeedu/kgeography/data/world.kgm:
Fix honduras color

------------------------------------------------------------------------
r840801 | sengels | 2008-08-01 23:03:25 +0200 (Fri, 01 Aug 2008) | 1 line

install AbstractFloatItem.h and AbstractLayer.h
------------------------------------------------------------------------
r840810 | sengels | 2008-08-01 23:38:38 +0200 (Fri, 01 Aug 2008) | 1 line

backport install AbstractLayerInterface.h
------------------------------------------------------------------------
r841076 | nienhueser | 2008-08-02 17:10:55 +0200 (Sat, 02 Aug 2008) | 3 lines

Install MarbleLayerInterface.h.
Backport of commit 841075.

------------------------------------------------------------------------
r841220 | nienhueser | 2008-08-02 21:42:07 +0200 (Sat, 02 Aug 2008) | 3 lines

Split MarbleMap::paint into several private methods so that MarbleWidget can reuse them and call its customPaint in between.
Fixes customPaint not working in MarbleWidget, backport of commit 841117.

------------------------------------------------------------------------
r841309 | scripty | 2008-08-03 06:50:10 +0200 (Sun, 03 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r841480 | aacid | 2008-08-03 12:56:40 +0200 (Sun, 03 Aug 2008) | 2 lines

Backport r841479 marble/trunk/KDE/kdeedu/marble/data/maps/earth/citylights/citylights.dgml: close <i> tag correctly

------------------------------------------------------------------------
r841654 | lueck | 2008-08-03 22:22:44 +0200 (Sun, 03 Aug 2008) | 2 lines

make it translatable
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r841890 | cniehaus | 2008-08-04 12:05:47 +0200 (Mon, 04 Aug 2008) | 6 lines

Backporting revision 841888 to KDE 4.1.

Also increasing the versionnumber to 4.1.0.

CCBUG:167785

------------------------------------------------------------------------
r841965 | cniehaus | 2008-08-04 13:08:26 +0200 (Mon, 04 Aug 2008) | 2 lines

Backporting commits number r841963, r841959 and r841934.

------------------------------------------------------------------------
r841971 | cniehaus | 2008-08-04 13:33:15 +0200 (Mon, 04 Aug 2008) | 1 line

Backporting commit r841970: Improve the layout of the timeline widget and remove a not used string
------------------------------------------------------------------------
r842081 | rahn | 2008-08-04 19:14:51 +0200 (Mon, 04 Aug 2008) | 3 lines

- Make really sure that the paint device gets destroyed after the painter ...


------------------------------------------------------------------------
r842084 | rahn | 2008-08-04 19:16:55 +0200 (Mon, 04 Aug 2008) | 3 lines

- Mostly fixing the Designer Plugin (except for projection change)


------------------------------------------------------------------------
r842270 | rahn | 2008-08-05 06:24:05 +0200 (Tue, 05 Aug 2008) | 3 lines

- Getting rid of the blue rectangle and a memory leak


------------------------------------------------------------------------
r842291 | rahn | 2008-08-05 08:45:21 +0200 (Tue, 05 Aug 2008) | 3 lines

- Fixing the lakes and rivers in the plain map.


------------------------------------------------------------------------
r842298 | mlaurent | 2008-08-05 09:23:35 +0200 (Tue, 05 Aug 2008) | 3 lines

Backport:
Fix potential mem leak

------------------------------------------------------------------------
r842612 | cniehaus | 2008-08-05 18:26:42 +0200 (Tue, 05 Aug 2008) | 1 line

BUG:163000
------------------------------------------------------------------------
r842626 | cniehaus | 2008-08-05 18:53:10 +0200 (Tue, 05 Aug 2008) | 1 line

backport of the fix for bug #142047
------------------------------------------------------------------------
r842633 | pino | 2008-08-05 19:00:23 +0200 (Tue, 05 Aug 2008) | 4 lines

Do not copy the document, it is not a good idea...

BUG: 168438

------------------------------------------------------------------------
r842662 | cniehaus | 2008-08-05 20:26:26 +0200 (Tue, 05 Aug 2008) | 1 line

Fix an obviously wrong string
------------------------------------------------------------------------
r843092 | rahn | 2008-08-06 14:36:54 +0200 (Wed, 06 Aug 2008) | 5 lines

- Fix: Making the app not hang on quit
- Fix: Lakes in the precipitation and temperature maps
- Adding various deletes where necessary.


------------------------------------------------------------------------
r843160 | lueck | 2008-08-06 17:21:52 +0200 (Wed, 06 Aug 2008) | 1 line

make marbles doc visible in khelpcenter
------------------------------------------------------------------------
r843235 | jmhoffmann | 2008-08-06 21:27:05 +0200 (Wed, 06 Aug 2008) | 8 lines

    Change container type of m_jobQueue from QQueue to QStack which
    has the advantage that tiles of the current visited area are
    loaded first.
    This is noticable if you scroll and zoom a lot or if the internet
    connection is slower than DSL like GPRS/EDGE or even 3G GSM.
    Backported from trunk.


------------------------------------------------------------------------
r843369 | scripty | 2008-08-07 06:49:19 +0200 (Thu, 07 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r845034 | scripty | 2008-08-11 07:04:37 +0200 (Mon, 11 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r845405 | aacid | 2008-08-11 17:58:09 +0200 (Mon, 11 Aug 2008) | 2 lines

an space is missing here

------------------------------------------------------------------------
r846141 | scripty | 2008-08-13 07:33:25 +0200 (Wed, 13 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r847249 | scripty | 2008-08-15 06:54:36 +0200 (Fri, 15 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r847947 | rahn | 2008-08-16 15:25:37 +0200 (Sat, 16 Aug 2008) | 4 lines

- Fixing Marble not showing anything if no maptheme got assigned.
- Fixing a corner case where the bounding box didn't get detected properly


------------------------------------------------------------------------
r849003 | rahn | 2008-08-18 23:03:41 +0200 (Mon, 18 Aug 2008) | 3 lines

- Fix [Bug 169379] New: Please correct the position of Mount Kilimanjaro!


------------------------------------------------------------------------
r849340 | rahn | 2008-08-19 13:38:04 +0200 (Tue, 19 Aug 2008) | 3 lines

- Better colors ...


------------------------------------------------------------------------
r849670 | rahn | 2008-08-19 23:54:30 +0200 (Tue, 19 Aug 2008) | 4 lines

- Every nice feature starts off as a hack: 
  Fancy boundaries.


------------------------------------------------------------------------
r850042 | aacid | 2008-08-20 19:51:26 +0200 (Wed, 20 Aug 2008) | 7 lines

Backport r846085 | aacid | 2008-08-13 00:49:49 +0200 (dc, 13 ago 2008) | 2 lines
Changed paths:
   M /trunk/KDE/kdeedu/kbruch/src/mainqtwidget.cpp
   M /trunk/KDE/kdeedu/kbruch/src/mainqtwidget.h

We want a page widget not a page dialog

------------------------------------------------------------------------
r850201 | harris | 2008-08-21 02:05:32 +0200 (Thu, 21 Aug 2008) | 20 lines

Backporting fix for FOV symbols (trunk r848577).  Original commit
message fllows:

Applying patch from Mederic Boquien.  Quoting his message:

"I have made a patch to correct a few problems on the FOV tool:
* It was not possible to use the semitransparent FOV because in kstars.kcfg,
the max index was set to 3 whereas the semitransparent FOV had index #4. When
selecting a semitransparent FOV, it switched back to #3 (bull's eye)
* Now the FOV preview is anti-aliased and the text indicating the FOV size is
correctly centred
* Now the semitransparent FOV is really semitransparent using the alpha
channel and not filled with a not-so-pretty pattern
* After having edited a FOV, the preview in the dialog containing the list of
FOV is updated"

Thanks, Mederic!



------------------------------------------------------------------------
r851112 | rahn | 2008-08-23 00:25:29 +0200 (Sat, 23 Aug 2008) | 5 lines

- Color update for clownesque Precipitation maps
- Icon update
- Version update


------------------------------------------------------------------------
r851302 | harris | 2008-08-23 16:15:51 +0200 (Sat, 23 Aug 2008) | 4 lines

When a custom object catalog is installed with the Get New Stuff tool, it will now 
be automatically added to the display.  Backported from trunk.


------------------------------------------------------------------------
r851304 | harris | 2008-08-23 16:17:07 +0200 (Sat, 23 Aug 2008) | 2 lines

version bump

------------------------------------------------------------------------
r851363 | pino | 2008-08-23 17:30:24 +0200 (Sat, 23 Aug 2008) | 2 lines

when saving, be sure to encode the xml as utf-8, and add the proper processing instruction for it

------------------------------------------------------------------------
r851424 | mlaurent | 2008-08-23 18:16:32 +0200 (Sat, 23 Aug 2008) | 2 lines

Backport: add missing i18n

------------------------------------------------------------------------
r851431 | mlaurent | 2008-08-23 18:24:10 +0200 (Sat, 23 Aug 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r851703 | pino | 2008-08-24 13:51:35 +0200 (Sun, 24 Aug 2008) | 2 lines

update the custom python syntax file to the current kate 4.1.x version

------------------------------------------------------------------------
r853447 | ereslibre | 2008-08-27 20:38:19 +0200 (Wed, 27 Aug 2008) | 4 lines

Backport of fix. setupGUI needs to be called after createGUI is, so the window is restored 
properly. Also, we remove the Create flag from the setupGUI call, since createGUI has already 
been called, so it doesn't get called twice

------------------------------------------------------------------------
r853453 | ereslibre | 2008-08-27 20:54:01 +0200 (Wed, 27 Aug 2008) | 2 lines

Backport of fix. Avoid to call to redundant setAutoSaveSettings when setupGUI is called

------------------------------------------------------------------------
