---
aliases:
- ../changelog4_8_1to4_8_2
hidden: true
title: KDE 4.8.2 Changelog
---

<h2>Changes in KDE 4.8.2</h2>
    <h3 id="kderuntime"><a name="kderuntime">kderuntime</a><span class="allsvnchanges"> [ <a href="4_8_2/kderuntime.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="nepomuk">nepomuk</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Backport from nepomuk-core: improved performance on res identification. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=289932">289932</a>.  See Git commit <a href="http://commits.kde.org/kde-runtime/754275eda610dce1160286a76339353097d8764c">754275e</a>. </li>
      </ul>
      </div>
      <h4><a name="kioslave">kioslave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix moving files from and to the trash bin. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=229465">229465</a>.  See Git commit <a href="http://commits.kde.org/kde-runtime/9d025212a78237a48308a691cb1448ce3703b4e4">9d02521</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeworkspace"><a name="kdeworkspace">kdeworkspace</a><span class="allsvnchanges"> [ <a href="4_8_2/kdeworkspace.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">XRender support for magnifier effect. See Git commit <a href="http://commits.kde.org/kde-workspace/c63944ee67906b49b75856d394e580def609c64a">c63944e</a>. </li>
          <li class="improvement">Implement xrender support for snaphelper effect. See Git commit <a href="http://commits.kde.org/kde-workspace/4abba97f727faa6b801e7ceb73819e7f20cc59b8">4abba97</a>. </li>
          <li class="improvement">Add xrender support to screenshot effect. See Git commit <a href="http://commits.kde.org/kde-workspace/30ee236d9d7a9c42d763fa4704156846cfa6af25">30ee236</a>. </li>
          <li class="improvement">Add offscreen render support to xrender.</li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Ensure that KConfig update script does not overwrite custom settings. See Git commit <a href="http://commits.kde.org/kde-workspace/05e1ee03e8b2dcd8e7a7563ca36f4fc01299e570">05e1ee0</a>. </li>
        <li class="bugfix normal">Lower windows does not change focus for Focus (Strictly) Under Mouse Focus Policies. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=80897">80897</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/095c602617949afc55e34d0634f8de8357ca28eb">095c602</a>. </li>
        <li class="bugfix normal">Fix Window Shading for XRender compositing. See Git commit <a href="http://commits.kde.org/kde-workspace/96b8a1d356b125990356c6667aa489961140a784">96b8a1d</a>. </li>
        <li class="bugfix normal">Desktop windows which do not annouce the virtual desktop they belong to are set on all desktops. See Git commit <a href="http://commits.kde.org/kde-workspace/e9dcf1a8d1c9e2a6b15982293a207e15b1116179">e9dcf1a</a>. </li>
        <li class="bugfix normal">Zoom effect zooms towards the mouse cursor if mouse tracking is disabled. See Git commit <a href="http://commits.kde.org/kde-workspace/e8d1d9709713e804c4ce26215b06cd85c505aca5">e8d1d97</a>. </li>
        <li class="bugfix normal">Fix restore size for shaded windows. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=243423">243423</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/5b11bf56780928cd531143ad6675bbe775548f92">5b11bf5</a>. </li>
        <li class="bugfix normal">Select minimum stacking order layer per screen. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=261799">261799</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/d854fc98630bcdbc0d450ae7bc642240f5f535ad">d854fc9</a>. </li>
        <li class="bugfix normal">Save restore size before removing the decoration for borderless maximized windows. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=295449">295449</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/677915da069b4d563b40250c4b69943af53a70bd">677915d</a>. </li>
        <li class="bugfix normal">Do not animate the highlight the first time the tabbox is shown. See Git commit <a href="http://commits.kde.org/kde-workspace/56345a9f158790e5f7102618c55c600cc688c855">56345a9</a>. </li>
        <li class="bugfix normal">Split decorationRect / visibleRect usage. See Git commit <a href="http://commits.kde.org/kde-workspace/0f3380f3b10e57416f81a1288dc10b8dfe11d87e">0f3380f</a>. </li>
        <li class="bugfix normal">Only keep fullscreen for transients on top - not random group members Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293265">293265</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/8634d94682800ad0d62b2c6f0c9877b8ba745009">8634d94</a>. </li>
      </ul>
      </div>
      <h4><a name="solid">solid</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix ejection of optical drives. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=296965">296965</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/1b0ecb4d951a4f0c742ba641f9954d45c015a5c3">1b0ecb4</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_8_2/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Do not think we have no free memory for the first two seconds. See Git commit <a href="http://commits.kde.org/okular/bf47b97cc28238599175477fe61acc2625f595e4">bf47b97</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_8_2/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://dolphin.kde.org" name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Select files which have been pasted or dropped. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=295389">295389</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/210e5e3b96883f5d856913f81834310ecb3819bf">210e5e3</a>. </li>
        <li class="bugfix normal">Prevent flickering of icons or previews when downloading files. See Git commit <a href="http://commits.kde.org/kde-baseapps/ef2ef83d762f4c2bdd0c13eb87759c58b1609957">ef2ef83</a>. </li>
        <li class="bugfix normal">Fix sorting-issue when "Sort folders first" is disabled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=296437">296437</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/7120e0166dc886e0a7fcbe03b8b5fb46137b5e75">7120e01</a>. </li>
        <li class="bugfix normal">Bypass crash in combination with the Polyester-style. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=296453">296453</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/010699e756d0bb6b8659a65e8af8c08d9c424080">010699e</a>. </li>
        <li class="bugfix normal">Fix alternate background issue in the Details mode. See Git commit <a href="http://commits.kde.org/kde-baseapps/f3e6298235238634bc34a23fdadd999c04fb5e1a">f3e6298</a>. </li>
        <li class="bugfix normal">Fix translation-issue in the context-menu. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=290620">290620</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/44482f8b66e7e3925f68936d811df0d644f5bc3c">44482f8</a>. </li>
        <li class="bugfix normal">Prevent endless scrolling when dragging items. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=295584">295584</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/6bf5f888c6735407645b53da82f57ec557fad5fc">6bf5f88</a>. </li>
        <li class="bugfix normal">Details view: Allow to turn off expandable folders like in Dolphin 1.7. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=289090">289090</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/e04cb14b19e05282f58d5d39d790b3c334366172">e04cb14</a>. </li>
        <li class="bugfix normal">Allow custom ordering of columns from the Details view. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=164696">164696</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/b62c74ec4cc891bc5fafeeafe67dbcc1d17fd445">b62c74e</a>. </li>
        <li class="bugfix normal">Increase the timeout for keyboard searches (typing the first few letters of a file name) to 5 seconds. See Git commit <a href="http://commits.kde.org/kde-baseapps/02eab49b2de51c31fe46a0d9501327b579b3648e">02eab49</a>. </li>
      </ul>
      </div>
      <h4><a name="konsole">konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Konsole 4.8.1 (KDE 4.8.1, Kubuntu 11.10, 64 bit) has crashed when closing a tab Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=296239">296239</a>.  See Git commit <a href="http://commits.kde.org/konsole/0e4844a3daa8bc4def7b952e3f936f23e381f71b">0e4844a</a>. </li>
        <li class="bugfix crash">konsole crashes running 'ssh -t' Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=297156">297156</a>.  See Git commit <a href="http://commits.kde.org/konsole/6cda78850fdbf7e155b73fe55fd5dfcfb8a3ff9d">6cda788</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_8_2/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash in KResources version if calendar fails to open during initialisation. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=296383">296383</a>.  See Git commit <a href="http://commits.kde.org/kdepim/4a3204b75464406ab3b3505e2fe222937aac6750">4a3204b</a>. </li>
        <li class="bugfix normal">Fix error preventing alarms from being edited or deleted. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=295926">295926</a>.  See Git commit <a href="http://commits.kde.org/kdepim/ae66e32fb194e01e06521c78e9c72d772d291261">ae66e32</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_8_2/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Allow encryption of multiple folders at once using filemanager integration. See Git commit <a href="http://commits.kde.org/kgpg/68e727d9eb64e99bb4655dbcf90b789344f5f4ce">68e727d</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Reload key groups when changing the GnuPG config file. See Git commit <a href="http://commits.kde.org/kgpg/092be7e2df9c7d013463f96ff7d187027fed20b3">092be7e</a>. </li>
        <li class="bugfix normal">Fix some editor actions with UTF-8 texts. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=295397">295397</a>.  See Git commit <a href="http://commits.kde.org/kgpg/fa5e306040bb977f38da6732d6a10330ed10d6b4">fa5e306</a>. </li>
        <li class="bugfix normal">Fix password entry when pinentry is not used. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=248161">248161</a>.  See Git commit <a href="http://commits.kde.org/kgpg/e2ac9db16bbab040e51ebdfb3c4e296644dc9a24">e2ac9db</a>. </li>
        <li class="bugfix normal">Use correct line endings when reading GnuPG output on Windows. See Git commit <a href="http://commits.kde.org/kgpg/7eb5ed3e439edf06fe4da36a12bb435a7e38905e">7eb5ed3</a>. </li>
        <li class="bugfix normal">When autodetecting the GnuPG executable look for "gpg2" first. See Git commit <a href="http://commits.kde.org/kgpg/954c7c3e8a1b8313bae8d717c557df8b7eafba12">954c7c3</a>. </li>
        <li class="bugfix normal">Honor the selected compression format when encrypting a folder. See Git commit <a href="http://commits.kde.org/kgpg/5bf78b98d39a789b596c755548bb6c0cc382e015">5bf78b9</a>. </li>
        <li class="bugfix normal">Distinguish between key id and fingerprint. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292405">292405</a>.  See Git commit <a href="http://commits.kde.org/kgpg/2a100ae8d203107d17e438f67f65d26a4949cccc">2a100ae</a>. </li>
        <li class="bugfix crash">Fix crash when list of files to decrypt is empty. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=294641">294641</a>.  See Git commit <a href="http://commits.kde.org/kgpg/42f8b1b51b09cb4b64fadf7e6cdd5ce359b0fa74">42f8b1b</a>. </li>
        <li class="bugfix crash">Fix crash when GnuPG operation fails while a password entry window is shown. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=296634">296634</a>.  See Git commit <a href="http://commits.kde.org/kgpg/2de8490d527d47d4fa4d8e643aefba5e842d2826">2de8490</a>. </li>
      </ul>
      </div>
    </div>