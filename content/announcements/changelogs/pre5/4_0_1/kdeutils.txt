2008-01-05 13:20 +0000 [r757591]  mlaurent

	* branches/KDE/4.0/kdeutils/sweeper/sweeper.cpp,
	  branches/KDE/4.0/kdeutils/sweeper/privacyfunctions.h,
	  branches/KDE/4.0/kdeutils/sweeper/privacyfunctions.cpp: Backport:
	  kicker is dead remove not necessary action

2008-01-05 13:50 +0000 [r757602]  mlaurent

	* branches/KDE/4.0/kdeutils/sweeper/privacyfunctions.cpp: Backport:
	  fix clear history

2008-01-05 13:57 +0000 [r757605]  mlaurent

	* branches/KDE/4.0/kdeutils/sweeper/privacyfunctions.cpp: Backport:
	  fix dbus function. Now function returns void element and not bool

2008-01-05 14:01 +0000 [r757607]  mlaurent

	* branches/KDE/4.0/kdeutils/sweeper/privacyfunctions.cpp: Backport:
	  Now all action works in kde4 kio_http_cache_cleaner is in
	  lib/kde4/libexec/

2008-01-05 14:56 +0000 [r757624]  mlaurent

	* branches/KDE/4.0/kdeutils/kwallet/konfigurator/konfigurator.cpp:
	  Backport fix dbus interface

2008-01-05 15:07 +0000 [r757627]  mlaurent

	* branches/KDE/4.0/kdeutils/sweeper/privacyfunctions.cpp: Backport
	  fix dbus error

2008-01-09 16:36 +0000 [r758929]  amantia

	* branches/KDE/4.0/kdeutils/kwallet/kwalletmanager.cpp: Backport:
	  Initialize variable to avoid crash on session restoration.

2008-01-09 22:48 +0000 [r759076]  brandybuck

	* branches/KDE/4.0/kdeutils/kcalc/kcalc_button.cpp,
	  branches/KDE/4.0/kdeutils/kcalc/kcalc.cpp,
	  branches/KDE/4.0/kdeutils/kcalc/kcalc_button.h: BUG: 153741
	  Improved text drawing in KCalcButton. Text is no longer being cut
	  off. The text is still cramped with the Oxygen style, however,
	  because KStyle defines PM_ButtonMargin to be zero.

2008-01-14 04:08 +0000 [r761114]  brandybuck

	* branches/KDE/4.0/kdeutils/ark/plugins/libarchive/libarchivehandler.cpp:
	  Backport from trunk: Use ARCHIVE_API_VERSION to determine the
	  proper return type for archive_read_finish().

2008-01-14 19:22 +0000 [r761403]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/keysmanager.cpp: Fix connect

2008-01-14 20:34 +0000 [r761429]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/core/kgpgkey.h,
	  branches/KDE/4.0/kdeutils/kgpg/kgpginterface.h,
	  branches/KDE/4.0/kdeutils/kgpg/keyinfodialog.cpp,
	  branches/KDE/4.0/kdeutils/kgpg/keysmanager.cpp,
	  branches/KDE/4.0/kdeutils/kgpg/core/kgpgkey.cpp,
	  branches/KDE/4.0/kdeutils/kgpg/kgpginterface.cpp: Backport some
	  improvements from working branch -save some memory for each key
	  by not storing things two or three times -more conversions from
	  K3ProcIO to KProcess

2008-01-15 16:47 +0000 [r761908]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/kgpgwizard.ui: Give descriptive
	  names to pages in wizard Backport of r739389

2008-01-15 17:00 +0000 [r761914]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/kgpg.cpp: When encrypting a folder
	  also support uncompressed tar files Backport of r742664

2008-01-15 21:57 +0000 [r762021]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/Mainpage.dox: put some useful text
	  in doxygen

2008-01-15 23:33 +0000 [r762045]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/keylistview.cpp: remove duplicate
	  instruction

2008-01-17 22:54 +0000 [r762827]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/keysmanager.cpp: Don't crash when
	  user requests to show a photo The code is probably not the most
	  beauty one now but it at least works at expected. BUG:156054

2008-01-21 23:36 +0000 [r764502]  lukas

	* branches/KDE/4.0/kdeutils/kcharselect/kcharselect-generate-datafile.py:
	  just in case the file gets regenerated, fix the script as well

2008-01-26 21:07 +0000 [r766889]  henrique

	* branches/KDE/4.0/kdeutils/ark/part/part.cpp: * Backport of fix to
	  bug 156743: Ark does not update the buttons on the toolbar after
	  loading an archive

2008-01-28 23:17 +0000 [r767887]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/kgpginterface.h,
	  branches/KDE/4.0/kdeutils/kgpg/kgpginterface.cpp: Port key
	  revoking to KProcess Fixes another obscure crash in
	  K3ProcessController CCBUG:154182 Backport of r767815

