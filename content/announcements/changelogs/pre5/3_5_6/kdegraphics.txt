2006-10-11 22:53 +0000 [r594669]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashErrorCodes.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/Splash.cc: Painting
	  an image of 0x0 does not seem right and is making it crash With
	  this code we get roughly the same output than acroread, this
	  basically means your pdf is broken BUGS: 135417

2006-10-15 11:50 +0000 [r595680]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.h,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: When reloading a
	  document because it changed still be in presentation mode after
	  the reload BUGS: 135578

2006-10-26 20:40 +0000 [r599376]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp: Don't try
	  querying the document if it's closed. BUG: 126614

2006-10-27 08:29 +0000 [r599458]  kebekus

	* branches/KDE/3.5/kdegraphics/kdvi/TeXFontDefinition.cpp: fixes
	  bug #128639

2006-10-28 10:46 +0000 [r599720]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  Show a 'keep password' checkbox when using a wallet so the user
	  can decide if he wants the password saved in the wallet. BUG:
	  122605

2006-10-29 08:47 +0000 [r599951]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.cpp: use
	  the right arrows in RtL layout

2006-10-29 12:00 +0000 [r600007]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp: Cancel
	  selecting when Esc is pressed. BUG: 126359

2006-10-30 19:14 +0000 [r600494]  kebekus

	* branches/KDE/3.5/kdegraphics/kdvi/special.cpp: fixes problems
	  with unbalances parenthesis in certain kinds of links

2006-10-31 12:34 +0000 [r600673]  mkoller

	* branches/KDE/3.5/kdegraphics/kfile-plugins/xpm/kfile_xpm.cpp:
	  BUG: 136535 Use QImage instead of QPixmap to have no dependency
	  on X11

2006-11-05 21:19 +0000 [r602388]  mkoller

	* branches/KDE/3.5/kdegraphics/kghostview/kgv_miniwidget.cpp: BUG:
	  111517 check 0-pointer to avoid crashes

2006-11-10 12:04 +0000 [r603822]  cartman

	* branches/KDE/3.5/kdegraphics/kpdf/shell/shell.cpp: show filename
	  in KPDF title instead of whole path which is ugly and
	  undistinguishable on kicker when multiple PDF files are opened

2006-11-10 15:05 +0000 [r603864]  cartman

	* branches/KDE/3.5/kdegraphics/kpdf/shell/shell.cpp: revertlast
	  until Albert OKs for NACKs it

2006-11-10 19:42 +0000 [r603921]  cartman

	* branches/KDE/3.5/kdegraphics/kpdf/shell/shell.cpp: KPDF pretty
	  titles, re-applying with Albert's approval :-)

2006-11-12 18:29 +0000 [r604390]  mkoller

	* branches/KDE/3.5/kdegraphics/kfax/kfax.cpp,
	  branches/KDE/3.5/kdegraphics/kfax/viewfax.cpp,
	  branches/KDE/3.5/kdegraphics/kfax/faxinput.cpp: BUG: 136590 Fix
	  handling of filenames with non-ascii chars

2006-11-17 12:55 +0000 [r605598]  mueller

	* branches/KDE/3.5/kdegraphics/kghostview/dscparse.cpp: - fix
	  off-by-one issue during parsing certain ps files

2006-11-17 22:03 +0000 [r605704]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSOutputDev.cc: fix
	  typo error

2006-11-17 22:15 +0000 [r605710]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Fix kpdf ignoring
	  user defined margins when printing @translators, it introduces a
	  four new messages, hope it's ok for you, if not please shout and
	  i will revert the change. BUGS: 124192 CCMAIL:
	  kde-i18n-doc@kde.org

2006-11-19 13:32 +0000 [r606144]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp: delete the
	  page items when destroying the page view

2006-11-19 16:02 +0000 [r606194]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  plug small memory leak

2006-11-19 16:09 +0000 [r606198]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  plug another small leak

2006-11-19 22:32 +0000 [r606296]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.h: Fix "No
	  scrolling when selection reaches border" BUGS: 107803

2006-11-21 19:01 +0000 [r606774]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/shell.cpp: Fix ¿finally?
	  kpdf wrong restoring on session restore, thanks a lot Lubos for
	  finding the problem. BUGS: 135373

2006-11-24 00:24 +0000 [r607303]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kmrml/kmrml/kcontrol/mainpage.cpp,
	  branches/KDE/3.5/kdegraphics/kmrml/kmrml/mrml_part.cpp,
	  branches/KDE/3.5/kdegraphics/kmrml/kmrml/kcontrol/indexer.h,
	  branches/KDE/3.5/kdegraphics/kmrml/kmrml/kcontrol/mainpage.h,
	  branches/KDE/3.5/kdegraphics/kmrml/kmrml/kcontrol/indexer.cpp:
	  better error handling: show an error messagebox when indexing
	  failed (e.g. because gift is not installed) CCBUG 137238

2006-11-24 01:17 +0000 [r607310]  mhunter

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  Typographical corrections and changes

2006-11-24 09:49 +0000 [r607360]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kmrml/kmrml/kcontrol/kcmkmrml.h,
	  branches/KDE/3.5/kdegraphics/kmrml/kmrml/kcontrol/kcmkmrml.cpp:
	  check for a valid gift installation and disable the indexing GUI
	  if not found. Provide a reference to gnu.org/software/gift

2006-11-25 18:08 +0000 [r607778]  mkoller

	* branches/KDE/3.5/kdegraphics/kfile-plugins/jpeg/exif.cpp: BUG:
	  110038 Apply patch from Loïc Brarda fixing exif information
	  reading

2006-11-27 19:17 +0000 [r608521-608520]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/gp_outputdev.cpp:
	  last days memleak fixing was deleting too much and causing
	  crashed because of double deletes, bad! bad!

	* branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.h: Using
	  a singleshot timer to start the auto page change is bad because
	  if the user changes the page manually you get more than one timer
	  active at once and then next pages change in what seems a random
	  time. BUGS: 137971

2006-11-29 21:49 +0000 [r609253]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/core/observer.h: Preload next
	  and previous page if threading is enabled and not on low memory
	  setting BUGS: 132029

2006-12-01 17:24 +0000 [r609687]  mueller

	* branches/KDE/3.5/kdegraphics/kfile-plugins/jpeg/exif.h,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/jpeg/exif.cpp: fix
	  endless recursion with malicious input files

2006-12-08 13:56 +0000 [r611504]  mhunter

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp: i18n
	  plural handling correction - not sure about %1 either. Can't that
	  just be 'No'?

2006-12-09 14:01 +0000 [r611809]  woebbe

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp: compile

2006-12-09 14:31 +0000 [r611812]  woebbe

	* branches/KDE/3.5/kdegraphics/kiconedit/kicongrid.cpp:
	  editPaste(bool): warning: value computed is not used && makes
	  more sense than , in the conditional statement of a for loop

2006-12-10 10:17 +0000 [r612077]  mhunter

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp: Blame
	  the laptop keyboard

2006-12-14 04:35 +0000 [r613574]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kptool.cpp: add note
	  about memory leak in code that probably doesn't get executed
	  anyway; let's not touch the code as we would probably introduce
	  bugs

2006-12-14 04:49 +0000 [r613575]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kptool.cpp:
	  KActionCollection::remove() frees the memory as well

2006-12-15 00:37 +0000 [r613757]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/tools/kptoolpen.cpp:
	  +comment explaining why "kpToolPen::hover()" sets "m_mouseButton"

2006-12-15 10:27 +0000 [r613815]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/tools/kptoolselection.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/tools/kptoolselection.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: * Fix crash
	  triggered by rapidly deselecting a selection after drag-scaling
	  the selection (Bug 117866) [before inserting
	  "kpToolSelectionResizeScaleCommand" into the command history,
	  finalize the smooth scale so that the "m_smoothScaleTimer" is
	  disabled] * Up ver to 1.4.6_relight-pre Needs backporting to
	  branches/KDE/3.[34]/, branches/kolourpaint/1.2_kde3/ and forward
	  porting to trunk/KDE/ (KDE 4). We lazily claim in the NEWS file
	  that we've already backported. This fix will be in KDE 3.5.6.
	  CCMAIL: 117866@bugs.kde.org

2006-12-15 10:53 +0000 [r613827]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS: better wording

2006-12-19 20:37 +0000 [r615000]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SplashOutputDev.cc:
	  Fix gray calculation, patch by Scott Turner <scotty1024@mac.com>

2007-01-04 20:59 +0000 [r619947-619946]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.cpp:
	  Backport from SVN commit 619673 by pino in okular: Fix a nasty
	  bug that could happened in presentation mode: when requestng
	  -synchronously- a pixmap for the current page, it could be
	  scheduled in the queue. Then, requesting the previous and next
	  page in presentation mode -in a second request- caused the
	  deletion of the original page request. The fix consists in
	  requesting all the requests -at once-, so no request is lost.

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp: Backport
	  from SVN commit 619810 by pino in okular: One single char remove
	  to create correctly the queue of pixmap requests: new
	  asynchronous requests goes as last one in the group of other with
	  the same priority. Thus, if an observer requests eg pixmap for
	  pages 2 and 3, now they are generated in the correct order the
	  observer specified, and not in the reversed one.

2007-01-04 21:44 +0000 [r619958]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp: As now the
	  requests are done in the right order, request first the next and
	  then the previous page.

2007-01-08 22:27 +0000 [r621507]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/gmem.c,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JBIG2Stream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.h: Merge
	  ftp://ftp.foolabs.com/pub/xpdf/xpdf-3.01pl2.patch Most of the
	  patches were already on our codebase but this are some
	  refinements over them

2007-01-10 20:12 +0000 [r622127]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Catalog.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Catalog.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Array.h: Keep a set
	  of the already read page tree nodes and each time we follow a new
	  one check if we already processed it so we don't end in a
	  infinite loop. Fixes crash in malicious pdf found at
	  http://projects.info-pull.com/moab/MOAB-06-01-2007.html It may
	  seem quite intensive but my measurements indicate the page tree
	  processing is I/O bound as i got the same average times with and
	  without this patch. Obviously if anyone wants to measure more
	  it'll be appreciated. CCMAIL: okular-devel@kde.org CCMAIL:
	  security@kde.org

2007-01-11 21:50 +0000 [r622461]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Catalog.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Catalog.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Array.h: Less
	  complete but equally effective and more quick way of fixing the
	  MOAB-06-01-2007 problem.

2007-01-12 19:24 +0000 [r622742]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.h,
	  branches/KDE/3.5/kdegraphics/kpdf/core/generator.h,
	  branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSOutputDev.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSOutputDev.h: When
	  generating a Postscript document, output the %%Title for the PS
	  document this way: - the document title, if it has one, or - the
	  file name of the document (eg foo.pdf for /home/me/foo.pdf) BUGS:
	  122147

2007-01-12 23:46 +0000 [r622781]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaint.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: * Mark
	  1.4.6_relight as frozen for KDE 3.5.6 *
	  kolourpaint.sourceforge.net -> www.kolourpaint.org

2007-01-13 11:53 +0000 [r622849]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: bump the version to
	  0.5.6 for KDE 3.5.6

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

