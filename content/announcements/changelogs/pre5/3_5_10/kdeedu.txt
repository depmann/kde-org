2008-06-29 15:02 +0000 [r825873]  annma

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/nb.keyboard (added):
	  Norvegian keyboard, fixes Debian bug 449150, thanks for the file!
	  http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=449150

2008-06-29 16:50 +0000 [r825957]  anagl

	* branches/KDE/3.5/kdeedu/ktouch/ktouch.desktop,
	  branches/KDE/3.5/kdeedu/blinken/src/blinken.desktop,
	  branches/KDE/3.5/kdeedu/kig/kig/kig.desktop,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvoctrain.desktop,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/kverbos.desktop,
	  branches/KDE/3.5/kdeedu/keduca/resources/keduca.desktop,
	  branches/KDE/3.5/kdeedu/kpercentage/kpercentage/kpercentage.desktop,
	  branches/KDE/3.5/kdeedu/keduca/resources/keducabuilder.desktop,
	  branches/KDE/3.5/kdeedu/keduca/keduca/keduca_part.desktop,
	  branches/KDE/3.5/kdeedu/kturtle/src/kturtle.desktop,
	  branches/KDE/3.5/kdeedu/kalzium/src/kalzium.desktop,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/kmplot.desktop,
	  branches/KDE/3.5/kdeedu/kbruch/src/kbruch.desktop,
	  branches/KDE/3.5/kdeedu/kiten/kiten.desktop,
	  branches/KDE/3.5/kdeedu/kstars/kstars/kstars.desktop,
	  branches/KDE/3.5/kdeedu/kwordquiz/src/kwordquiz.desktop,
	  branches/KDE/3.5/kdeedu/klatin/klatin/klatin.desktop,
	  branches/KDE/3.5/kdeedu/kgeography/src/kgeography.desktop,
	  branches/KDE/3.5/kdeedu/klettres/klettres/klettres.desktop,
	  branches/KDE/3.5/kdeedu/kanagram/src/kanagram.desktop: Desktop
	  validation fixes: remove deprecated entries for Encoding and fix
	  Categories.

2008-06-29 22:27 +0000 [r826162]  anagl

	* branches/KDE/3.5/kdeedu/kig/mimetypes/x-drgeo.desktop,
	  branches/KDE/3.5/kdeedu/applnk/languages.desktop,
	  branches/KDE/3.5/kdeedu/khangman/khangman/khangman.desktop,
	  branches/KDE/3.5/kdeedu/applnk/science.desktop,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/x-kvtml.desktop,
	  branches/KDE/3.5/kdeedu/kig/mimetypes/x-kig.desktop,
	  branches/KDE/3.5/kdeedu/keduca/resources/x-edugallery.desktop,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/kmplot_part.desktop,
	  branches/KDE/3.5/kdeedu/kig/mimetypes/x-cabri.desktop,
	  branches/KDE/3.5/kdeedu/kig/kfile/kfile_drgeo.desktop,
	  branches/KDE/3.5/kdeedu/kig/mimetypes/x-kgeo.desktop,
	  branches/KDE/3.5/kdeedu/kig/mimetypes/x-kseg.desktop,
	  branches/KDE/3.5/kdeedu/kig/kig/kig_part.desktop,
	  branches/KDE/3.5/kdeedu/applnk/mathematics.desktop,
	  branches/KDE/3.5/kdeedu/applnk/tools.desktop,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/x-kmplot.desktop,
	  branches/KDE/3.5/kdeedu/applnk/miscellaneous.desktop,
	  branches/KDE/3.5/kdeedu/keduca/resources/x-edu.desktop,
	  branches/KDE/3.5/kdeedu/kig/kfile/kfile_kig.desktop: Desktop
	  validation fixes: remove deprecated entries for Encoding.

2008-06-30 18:17 +0000 [r826425]  pino

	* branches/KDE/3.5/kdeedu/libkdeedu/extdate/Makefile.am:
	  test_extdate and test_extdatepicker are tests applications, so
	  compile them as such (and without installing them, too)

2008-08-19 19:47 +0000 [r849597]  coolo

	* branches/KDE/3.5/kdeedu/kdeedu.lsm: update for 3.5.10

