2006-01-20 10:38 +0000 [r500460]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/rubyconfigwidget.cpp: *
	  If the 'interpreter' project option is blank, set it to 'ruby'

2006-01-26 21:39 +0000 [r502666]  rdale

	* branches/KDE/3.5/kdevelop/src/profiles/IDE/ScriptingLanguageIDE/RubyIDE/profile.config:
	  * Disable some plugins that aren't appropriate for ruby
	  development

2006-01-27 20:33 +0000 [r503005]  rdale

	* branches/KDE/3.5/kdevelop/parts/filecreate/filecreate_part.cpp: *
	  Set the initial size of the 'New File Creation' dialog to width
	  500 and height 200, as it was too small for most filenames.

2006-01-28 15:17 +0000 [r503254]  scripty

	* branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopiaapp/qtopiaapp.kdevtemplate:
	  Remove buggy (non-UTF-8) French line (Backport of revision
	  503253) (goutte)

2006-02-08 05:10 +0000 [r507001]  mattr

	* branches/KDE/3.5/kdevelop/languages/pascal/compiler/fpcoptions/optiontabs.cpp:
	  use the appropriate debug options. fixes bug 114350 BUG: 114350

2006-02-08 13:23 +0000 [r507061]  mattr

	* branches/KDE/3.5/kdevelop/kdevelop.m4.in: Fix bug 101586. use the
	  proper libsuffix BUG: 101586

2006-02-08 15:38 +0000 [r507111]  mueller

	* branches/KDE/3.5/kdevelop/lib/util/urlutil.cpp: remove annoying
	  debug output

2006-02-16 03:21 +0000 [r510021]  mattr

	* branches/KDE/3.5/kdevelop/buildtools/lib/parsers/autotools/tests/viewer.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/autotools/tests/viewer.h,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/autotools/tests/viewerbase.ui,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/autotools/tests/Makefile.am:
	  initial port of the test programs to Qt 4

2006-02-16 04:23 +0000 [r510027]  mattr

	* branches/KDE/3.5/kdevelop/buildtools/lib/parsers/autotools/tests/viewer.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/autotools/tests/viewer.h,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/autotools/tests/viewerbase.ui,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/autotools/tests/Makefile.am:
	  revert commit that was supposed to go to trunk

2006-02-19 23:00 +0000 [r511452]  mattr

	* branches/KDE/3.5/kdevelop/buildtools/lib/widgets/makeoptionswidget.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/autotools/autoprojectpart.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/lib/widgets/makeoptionswidgetbase.ui:
	  Port the bugfix of 115418 to the KDE 3.5 branch for KDevelop
	  3.3.2 CCBUG: 115418

2006-02-20 02:19 +0000 [r511476]  mattr

	* branches/KDE/3.5/kdevelop/parts/outputviews/makewidget.cpp: port
	  the fix for 104652 to the KDE 3.5 branch. Should be in KDE 3.5.2
	  (KDevelop 3.3.2) CCBUG: 104652

2006-02-20 04:25 +0000 [r511490]  mattr

	* branches/KDE/3.5/kdevelop/lib/widgets/processwidget.cpp: Don't
	  strip whitespace when inserting. CCBUG: 105376

2006-02-20 04:55 +0000 [r511493]  mattr

	* branches/KDE/3.5/kdevelop/src/kdevideextension.cpp,
	  branches/KDE/3.5/kdevelop/src/settingswidget.ui: Fix 105374 for
	  KDevelop 3.3.2 as well. (KDE 3.5.2) CCBUG: 105374

2006-02-20 05:03 +0000 [r511495]  mattr

	* branches/KDE/3.5/kdevelop/parts/outputviews/makewidget.cpp: use
	  the right setting for the output view font. left over from 511493

2006-02-20 17:31 +0000 [r511687]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/debugger/variablewidget.h,
	  branches/KDE/3.5/kdevelop/languages/ruby/doc/rails.toc (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rails/rails.png
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rails/rails.kdevtemplate
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/file_templates/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rails
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/file_templates/js
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/qtruby/app.kdevelop,
	  branches/KDE/3.5/kdevelop/languages/ruby/doc/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ruby/file_templates/rxml
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/ruby.appwizard,
	  branches/KDE/3.5/kdevelop/languages/ruby/doc/qtruby.toc,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rails/Makefile.am
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/rubyconfigwidgetbase.ui,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rails/rails
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/file_templates/rhtml
	  (added),
	  branches/KDE/3.5/kdevelop/src/profiles/IDE/ScriptingLanguageIDE/RubyIDE/profile.config,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/qtrubyapp/qtrubyapp.kdevelop,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rails/Makefile
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rails/CMakeLists.txt
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/file_templates/css
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/rubysupport_part.cpp,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rubyhello/app.kdevelop,
	  branches/KDE/3.5/kdevelop/languages/ruby/debugger/variablewidget.cpp,
	  branches/KDE/3.5/kdevelop/ChangeLog,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rails/app.kdevelop
	  (added): * Promote the basic rails support and project template
	  to the release branch * Turn off a few more plugins * Remove the
	  'Type' column from the ruby debugger

2006-02-21 19:54 +0000 [r512094]  rdale

	* branches/KDE/3.5/kdevelop/parts/appwizard/importdlg.cpp,
	  branches/KDE/3.5/kdevelop/parts/appwizard/appwizarddlg.cpp,
	  branches/KDE/3.5/kdevelop/parts/appwizard/kdevlicense.cpp: *
	  Promote support for license comments in XML languages to the
	  release branch.

2006-02-21 20:57 +0000 [r512116-512115]  gianni

	* branches/KDE/3.5/kdevelop/languages/ruby/rubyconfigwidgetbase.ui:
	  applied fixuifiles

	* branches/KDE/3.5/kdevelop/buildtools/lib/widgets/makeoptionswidgetbase.ui:
	  applied fixuifiles

2006-02-21 21:58 +0000 [r512133]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/debugger/rdbcontroller.h,
	  branches/KDE/3.5/kdevelop/ChangeLog,
	  branches/KDE/3.5/kdevelop/languages/ruby/debugger/rdbcontroller.cpp:
	  * Add the pid to the filename of the Unix domain socket used by
	  the ruby debugger so that more than one instance of KDevelop can
	  be run on a machine.

2006-02-22 11:25 +0000 [r512369]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rails/Makefile.am:
	  * The $(GZIP) makefile variable wasn't defined, so hard code as
	  'gzip'.

2006-02-22 15:06 +0000 [r512445]  rdale

	* branches/KDE/3.5/kdevelop/parts/filecreate/template-info.xml,
	  branches/KDE/3.5/kdevelop/ChangeLog: * Disable the 'New File'
	  sidetab by default --This line, and thosichard Dale
	  <rdale@foton.es> M ChangeLog M parts/filecreate/template-info.xml

2006-03-01 12:59 +0000 [r514777]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/debugger/breakpoint.cpp:
	  * Fixed bug where breakpoints for sources not in the project
	  directory wouldn't work

2006-03-09 20:24 +0000 [r517060]  dymo

	* branches/KDE/3.5/kdevelop/src/profiles/IDE/ScriptingLanguageIDE/PHPIDE/profile.config,
	  branches/KDE/3.5/kdevelop/parts/doxygen/kdevdoxygen.desktop:
	  Backport - enable doxygen for php

2006-03-13 02:49 +0000 [r518080]  nhasan

	* branches/KDE/3.5/kdevelop/src/generalinfowidgetbase.ui,
	  branches/KDE/3.5/kdevelop/languages/cpp/configproblemreporter.ui,
	  branches/KDE/3.5/kdevelop/parts/documentation/docprojectconfigwidgetbase.ui,
	  branches/KDE/3.5/kdevelop/editors/editor-chooser/editchooser.ui,
	  branches/KDE/3.5/kdevelop/parts/vcsmanager/vcsmanagerprojectconfigbase.ui:
	  Fix margins.

2006-03-13 19:55 +0000 [r518350]  escuder

	* branches/KDE/3.5/kdevelop/languages/php/phpfile.h,
	  branches/KDE/3.5/kdevelop/languages/php/phpsupportpart.h,
	  branches/KDE/3.5/kdevelop/languages/php/phpparser.cpp,
	  branches/KDE/3.5/kdevelop/languages/php/phpfile.cpp,
	  branches/KDE/3.5/kdevelop/languages/php/phpparser.h,
	  branches/KDE/3.5/kdevelop/languages/php/phpsupport_event.h,
	  branches/KDE/3.5/kdevelop/languages/php/phpsupportpart.cpp:
	  Kdevelop / PHP part : Fix bugs in the php support language that
	  cause some crash

2006-03-14 18:14 +0000 [r518624]  escuder

	* branches/KDE/3.5/kdevelop/languages/php/phpfile.cpp,
	  branches/KDE/3.5/kdevelop/languages/php/phpsupportpart.cpp:
	  Kdevelop / PHP Add more thread sleeping

2006-03-14 21:18 +0000 [r518676]  rdale

	* branches/KDE/3.5/kdevelop/parts/outputviews/appoutputwidget.cpp:
	  * Look for ruby errors in the app output widget

2006-03-14 21:42 +0000 [r518685]  dymo

	* branches/KDE/3.5/kdevelop/parts/appwizard/common/wx-configure.in:
	  Backported fix for incorrect compiler flags.

2006-03-15 18:47 +0000 [r518942]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/doc/rails.toc,
	  branches/KDE/3.5/kdevelop/languages/ruby/doc/qtruby.toc: * Fix
	  toc links so they work correctly for Rails and QtRuby

2006-03-15 19:33 +0000 [r518954]  dymo

	* branches/KDE/3.5/kdevelop/parts/documentation/interfaces/kdevdocumentationplugin.cpp:
	  Backported the incorrect path reading fix.

2006-03-17 21:28 +0000 [r519762]  coolo

	* branches/KDE/3.5/kdevelop/configure.in.in: tagging 3.5.2

2006-03-17 21:34 +0000 [r519792]  coolo

	* branches/KDE/3.5/kdevelop/kdevelop.lsm: tagging 3.5.2

