------------------------------------------------------------------------
r1169546 | dakon | 2010-08-30 04:42:28 +1200 (Mon, 30 Aug 2010) | 4 lines

fix crash when a selected codec is not supported by QTextCodec

BUG:249260

------------------------------------------------------------------------
r1169696 | rkcosta | 2010-08-30 09:24:43 +1200 (Mon, 30 Aug 2010) | 3 lines

Fix include order.

SVN_SILENT
------------------------------------------------------------------------
r1170166 | rkcosta | 2010-08-31 12:07:40 +1200 (Tue, 31 Aug 2010) | 11 lines

Fix constness of extractFile() parameters in the header file.

'options' is a const parameter in archivemodel.cpp, and should be so in
the header declaration as well.

This should fix compilation with Sun Studio.

Patch by tropikhajma AT gmail.com, thanks!

BUG: 249552
FIXED-IN: 4.5.2
------------------------------------------------------------------------
r1170664 | lueck | 2010-09-02 07:28:50 +1200 (Thu, 02 Sep 2010) | 1 line

doc backport for 4.5.2
------------------------------------------------------------------------
r1171417 | kossebau | 2010-09-04 08:23:18 +1200 (Sat, 04 Sep 2010) | 2 lines

backport of 1171416 : fixed: better check parent index in rowCount()/columnCount(), being a QAbstractTableModel seems not to protect (after all views still might be dumb)

------------------------------------------------------------------------
r1171424 | kossebau | 2010-09-04 08:54:49 +1200 (Sat, 04 Sep 2010) | 1 line

patch_version++
------------------------------------------------------------------------
r1171440 | kossebau | 2010-09-04 09:35:07 +1200 (Sat, 04 Sep 2010) | 5 lines

fixed: no update if new font was set to view

QWidget::fontChange() was Qt3, so AbstractByteArrayView::fontChange() will never be called
this is a BC workaround, trunk has correct fix

------------------------------------------------------------------------
r1171462 | kossebau | 2010-09-04 09:59:34 +1200 (Sat, 04 Sep 2010) | 2 lines

fixed: react to changes in global font settings, BC-adapted backport of 1171151

------------------------------------------------------------------------
r1171575 | kossebau | 2010-09-05 01:50:02 +1200 (Sun, 05 Sep 2010) | 1 line

fixed: width of offset column was wrongly calculated (is not multiple of width of char in pixel, as that int value is rounded from float)
------------------------------------------------------------------------
r1171975 | kossebau | 2010-09-06 09:43:13 +1200 (Mon, 06 Sep 2010) | 2 lines

backport of 1171974: fixed: react to changes in global settings for fixed font

------------------------------------------------------------------------
r1172876 | scripty | 2010-09-08 15:23:59 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173534 | dakon | 2010-09-10 04:24:33 +1200 (Fri, 10 Sep 2010) | 2 lines

fix generation of keys not using RSA keys but DSA/ElGamal if "RSA" is not translated as "RSA"

------------------------------------------------------------------------
r1173545 | dakon | 2010-09-10 05:10:49 +1200 (Fri, 10 Sep 2010) | 1 line

handle subkeys with multiple usages right
------------------------------------------------------------------------
r1174640 | scripty | 2010-09-13 14:38:55 +1200 (Mon, 13 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1174739 | dakon | 2010-09-13 20:02:25 +1200 (Mon, 13 Sep 2010) | 1 line

fix KFontChooser() creation with bool passed as flags
------------------------------------------------------------------------
r1174890 | cfeck | 2010-09-14 01:49:52 +1200 (Tue, 14 Sep 2010) | 2 lines

SVN_SILENT backport compile fix

------------------------------------------------------------------------
r1175027 | pino | 2010-09-14 10:38:02 +1200 (Tue, 14 Sep 2010) | 4 lines

rename as hicolor, so they can be used when the current icon theme is not oxygen

they are already installed in the data dir of kwalletmanager, so there's no pollution of the global hicolor icon theme

------------------------------------------------------------------------
r1175142 | pino | 2010-09-14 22:04:33 +1200 (Tue, 14 Sep 2010) | 4 lines

rename as hicolor, so they can be used when the current icon theme is not oxygen

they are already installed in the data dir of kgpg, so there's no pollution of the global hicolor icon theme

------------------------------------------------------------------------
r1175299 | lueck | 2010-09-15 05:12:33 +1200 (Wed, 15 Sep 2010) | 1 line

screenshot update
------------------------------------------------------------------------
r1176321 | lueck | 2010-09-17 22:52:18 +1200 (Fri, 17 Sep 2010) | 1 line

backport from trunk 1176319: remove useless X-DocPath entry, the correct entry is in kwalletmanager.desktop
------------------------------------------------------------------------
