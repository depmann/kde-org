------------------------------------------------------------------------
r1107863 | scripty | 2010-03-27 15:33:33 +1300 (Sat, 27 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1107998 | ppenz | 2010-03-28 02:21:16 +1300 (Sun, 28 Mar 2010) | 3 lines

Backport of SVN commit 1106403: Send CTRL+C to the terminal instead of of backspaces. Thanks to the FiNeX and g111 for the hint. 

CCBUG: 161637
------------------------------------------------------------------------
r1108168 | scripty | 2010-03-28 15:36:19 +1300 (Sun, 28 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1108467 | hpereiradacosta | 2010-03-29 13:17:10 +1300 (Mon, 29 Mar 2010) | 3 lines

Backport r1108456
avoid double deletion of animationData while unregistering widget

------------------------------------------------------------------------
r1108469 | hpereiradacosta | 2010-03-29 13:20:03 +1300 (Mon, 29 Mar 2010) | 4 lines

backport r1108406
proper deletion of transitionwidget, which for some reason is not properly taken care of by Qt


------------------------------------------------------------------------
r1108474 | hpereiradacosta | 2010-03-29 13:38:25 +1300 (Mon, 29 Mar 2010) | 5 lines

backport: r1108472
revert r1108406
Do _not_ delete TransitionWidget. This is the responsibility of the parent widget (whose deletion triggers the TransitionData 
deletion anyway)

------------------------------------------------------------------------
r1108704 | hpereiradacosta | 2010-03-30 05:05:30 +1300 (Tue, 30 Mar 2010) | 5 lines

Backport: r1108701
Fixed painter CompositionMode when rendering rounded corners for menus, detached dockpanels, detached 
toolbars and combobox containers. This prevents the corners to appear "flat" in some cases, notably when 
a stylesheet is given to the application.

------------------------------------------------------------------------
r1108721 | hpereiradacosta | 2010-03-30 06:17:43 +1300 (Tue, 30 Mar 2010) | 5 lines

Backport r1108717
Added explicit painting of toolbar background in non-detached mode, when one of the parent has 
autofillBackground set to true.


------------------------------------------------------------------------
r1108846 | hpereiradacosta | 2010-03-30 10:59:55 +1300 (Tue, 30 Mar 2010) | 5 lines

Backport: r1108844
Properly account for decoration padding when calculating mousePosition in window, needed to 
decide cursor shape
CCBUG: 231844

------------------------------------------------------------------------
r1108850 | hpereiradacosta | 2010-03-30 11:11:03 +1300 (Tue, 30 Mar 2010) | 4 lines

Backport: r1108849
Properly account for decoration padding when calculating mousePosition in buttonReleaseEvent.
CCBUG: 231844

------------------------------------------------------------------------
r1108974 | annma | 2010-03-30 21:35:02 +1300 (Tue, 30 Mar 2010) | 3 lines

Move folders correctly. Thanks a lot Maris for all the investigation and the patch testing, it's very nice of you having taken the time to research this bug!
BUG=227836

------------------------------------------------------------------------
r1109055 | annma | 2010-03-31 02:26:35 +1300 (Wed, 31 Mar 2010) | 2 lines

add ability to uncompress archives with wallpapers in the next 4.4 version 

------------------------------------------------------------------------
r1109066 | annma | 2010-03-31 02:46:49 +1300 (Wed, 31 Mar 2010) | 2 lines

add Category 'other'

------------------------------------------------------------------------
r1109126 | hpereiradacosta | 2010-03-31 05:15:06 +1300 (Wed, 31 Mar 2010) | 5 lines

backport: r1109123
Properly account for the fact that Hover takes precedence over Focus in PushButtons to trigger 
animations. This fixes a couple of glitches/flicker when pressing on non-editable comboboxes, as well 
as pushbuttons.

------------------------------------------------------------------------
r1109266 | mart | 2010-03-31 11:05:33 +1300 (Wed, 31 Mar 2010) | 2 lines

backport fix to bug 232753

------------------------------------------------------------------------
r1109315 | hpereiradacosta | 2010-03-31 13:38:17 +1300 (Wed, 31 Mar 2010) | 4 lines

Backport r1109312
Fixed handling of 'enabled' flag so that configuration changes are updated 'on fly' rather than being applied 
to newly launched applications only

------------------------------------------------------------------------
r1109329 | scripty | 2010-03-31 14:39:24 +1300 (Wed, 31 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1109537 | graesslin | 2010-04-01 01:30:43 +1300 (Thu, 01 Apr 2010) | 5 lines

Backport rev 1109536:
Compositing settings have to be read if compositing is enforced by the environment variable.
CCBUG: 231851


------------------------------------------------------------------------
r1109664 | mart | 2010-04-01 06:10:49 +1300 (Thu, 01 Apr 2010) | 2 lines

backport fixed size to avoid jumpy behaviour

------------------------------------------------------------------------
r1109721 | mart | 2010-04-01 10:28:15 +1300 (Thu, 01 Apr 2010) | 2 lines

backport fix to first config file restore: assign the screen to the configured containment

------------------------------------------------------------------------
r1109766 | scripty | 2010-04-01 15:09:21 +1300 (Thu, 01 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110076 | aseigo | 2010-04-02 09:03:29 +1300 (Fri, 02 Apr 2010) | 2 lines

 ensure the value passed to stretch is always > 0

------------------------------------------------------------------------
r1110078 | rdieter | 2010-04-02 09:06:21 +1300 (Fri, 02 Apr 2010) | 2 lines

backport Qt 4.7 fix, r1095576

------------------------------------------------------------------------
r1110079 | rdieter | 2010-04-02 09:07:57 +1300 (Fri, 02 Apr 2010) | 2 lines

backport r1082187 , fix for Qt 4.7

------------------------------------------------------------------------
r1110080 | rdieter | 2010-04-02 09:17:08 +1300 (Fri, 02 Apr 2010) | 2 lines

backport r1010068 , fix for Qt 4.7

------------------------------------------------------------------------
r1110098 | hpereiradacosta | 2010-04-02 10:22:21 +1300 (Fri, 02 Apr 2010) | 4 lines

Backport r1110096
Do not highlight (focus or hover) Frames for which WA_HOVER is not set, because it creates 
artifacts, notably for straight sunken QFrames.

------------------------------------------------------------------------
r1110144 | scripty | 2010-04-02 15:01:28 +1300 (Fri, 02 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110240 | darioandres | 2010-04-03 03:33:25 +1300 (Sat, 03 Apr 2010) | 7 lines

Backport to 4.4 of:
- Fix references to the "Reload" button
  Approved by five i18n teams (Ukrainian, Danish, Dutch, Czech, German)

BUG: 232682


------------------------------------------------------------------------
r1110420 | hpereiradacosta | 2010-04-03 14:49:32 +1300 (Sat, 03 Apr 2010) | 3 lines

Backport r1110419:
properly restart busy indicator timer when duration is changed

------------------------------------------------------------------------
r1110422 | scripty | 2010-04-03 14:50:47 +1300 (Sat, 03 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110613 | jlayt | 2010-04-04 08:16:04 +1200 (Sun, 04 Apr 2010) | 8 lines

Add default date format for USA, which has apparently never been set before.  
Amazing no-one has never spotted this before.

Backported from trunk commit 1110612..

CCBUG: 233070


------------------------------------------------------------------------
r1110780 | scripty | 2010-04-04 13:48:17 +1200 (Sun, 04 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1111028 | mart | 2010-04-05 05:10:59 +1200 (Mon, 05 Apr 2010) | 3 lines

backport r1105508
non autohide panels work again

------------------------------------------------------------------------
r1111036 | mart | 2010-04-05 05:33:31 +1200 (Mon, 05 Apr 2010) | 2 lines

backport the immutability check to the containment

------------------------------------------------------------------------
r1111074 | hpereiradacosta | 2010-04-05 07:47:52 +1200 (Mon, 05 Apr 2010) | 5 lines

Backport r1111073:
Fixed precedence of Focus over Hover for comboboxes and generic frames animated highlight. This fixes some glitches notably when 
hovering a focused editable combobox


------------------------------------------------------------------------
r1111755 | mart | 2010-04-07 04:00:03 +1200 (Wed, 07 Apr 2010) | 2 lines

backport fix to 233511

------------------------------------------------------------------------
r1111890 | ossi | 2010-04-07 10:50:25 +1200 (Wed, 07 Apr 2010) | 4 lines

place themed greeter on correct screen
BUG: 194312


------------------------------------------------------------------------
r1111908 | hpereiradacosta | 2010-04-07 13:07:32 +1200 (Wed, 07 Apr 2010) | 6 lines

backport r1111900, r1111907
Fixed window background for QMdiSubWindow. (was transparent since r1081687, and flat before that).
Use normal RenderWindowBackground.
Also: use proper rect from QPaintEvent to render normal Window background.
fixed background reference in maximized QMdiSubWindows

------------------------------------------------------------------------
r1111913 | scripty | 2010-04-07 13:43:27 +1200 (Wed, 07 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1111919 | hpereiradacosta | 2010-04-07 14:06:26 +1200 (Wed, 07 Apr 2010) | 5 lines

Backport: r1111917
- Fixed bug (blue pixels in top left corner) for MdiSubWindows
- remove MdiSubWindows mask
- use anti-aliased round corners

------------------------------------------------------------------------
r1112074 | sitter | 2010-04-07 21:26:53 +1200 (Wed, 07 Apr 2010) | 17 lines

Backporting r1111779, r1111783, r1111865, r1111878 and r1111886

* Make ntpUtility lookup code work properly (fullpath lookup) and streamline it a bit
* Cleanup helper
* Pass ntpUtility found in dtime to helper instead of looking it up again
* Ensure kcmclockrc is world readable to make sure it can be read later on
This makes the KCM save the NTP config properly AND makes the NTP settings actually apply \o/
* Lock widget before invoking auth, to prevent problems from occuring... the KCM controls however do not quite follow my orders and stay active :(
* Make timezone tab searchable via a KTreeWidgetSearchLine
* Insert timezones4 catalog for KTimeZoneWidget::displayName
* Fix timezone changes

BUG: 123538
BUG: 148890
BUG: 148790


------------------------------------------------------------------------
r1112109 | sitter | 2010-04-07 23:42:32 +1200 (Wed, 07 Apr 2010) | 2 lines

fix build

------------------------------------------------------------------------
r1112328 | hpereiradacosta | 2010-04-08 09:25:00 +1200 (Thu, 08 Apr 2010) | 4 lines

Backport r1112325
fixed background positionning when title bar is hidden


------------------------------------------------------------------------
r1112343 | bram | 2010-04-08 09:51:17 +1200 (Thu, 08 Apr 2010) | 3 lines

Backport r1112121: do not enable screensaver when it's switched off in the settings.

CCBUG:206475
------------------------------------------------------------------------
r1112352 | hpereiradacosta | 2010-04-08 10:18:10 +1200 (Thu, 08 Apr 2010) | 5 lines

Backport r1112351:
Re-added 'full widget' background painting, because using clipRect breaks tab transition (to be 
investigated)


------------------------------------------------------------------------
r1112546 | dfaure | 2010-04-09 01:03:46 +1200 (Fri, 09 Apr 2010) | 4 lines

Repair "Check Status" so that it actually displays the results.
Fixed for: 4.4.3
BUG: 198880

------------------------------------------------------------------------
r1112606 | darioandres | 2010-04-09 04:50:42 +1200 (Fri, 09 Apr 2010) | 3 lines

- Add Konqueror to the KDEPIM app mappings (as several components use KHTML)


------------------------------------------------------------------------
r1112749 | scripty | 2010-04-09 13:49:58 +1200 (Fri, 09 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1113201 | scripty | 2010-04-10 13:50:17 +1200 (Sat, 10 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1113217 | hpereiradacosta | 2010-04-10 14:57:30 +1200 (Sat, 10 Apr 2010) | 3 lines

backport r1113212
do not draw hover rect on disabled menubar items, consistently with menus and toolbars

------------------------------------------------------------------------
r1113250 | trueg | 2010-04-10 20:03:33 +1200 (Sat, 10 Apr 2010) | 1 line

Backport: properly set the window icon
------------------------------------------------------------------------
r1113327 | ossi | 2010-04-11 01:59:29 +1200 (Sun, 11 Apr 2010) | 2 lines

backport: revert nonsensical double-fix

------------------------------------------------------------------------
r1113402 | hpereiradacosta | 2010-04-11 07:13:13 +1200 (Sun, 11 Apr 2010) | 3 lines

backport r1113401:
removed double-initialization of labelEngine duration

------------------------------------------------------------------------
r1113866 | scripty | 2010-04-12 13:40:00 +1200 (Mon, 12 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1113960 | sitter | 2010-04-12 23:27:14 +1200 (Mon, 12 Apr 2010) | 5 lines

Backport r1113958.
Fix stuff randomly disappearing from kickoff's computer tab by making the SystemModel use reloadApplications.
Thanks to Felix Geyer for the patch!
BUG: 232901

------------------------------------------------------------------------
r1113969 | sitter | 2010-04-12 23:48:06 +1200 (Mon, 12 Apr 2010) | 5 lines

Backport r1113965.
Make revert display resolution change work by enabling output/crtc in the RandROutput::propose* functions.
Thanks to Felix Geyer for the patch!
BUG: 222110

------------------------------------------------------------------------
r1114034 | sitter | 2010-04-13 02:39:19 +1200 (Tue, 13 Apr 2010) | 4 lines

Backport r1114030.
Fix by Felix Geyer to allow the activity bar applet to shrink below intended size, as discussed by Kubuntu developers with notmart.


------------------------------------------------------------------------
r1114417 | ossi | 2010-04-14 02:05:30 +1200 (Wed, 14 Apr 2010) | 3 lines

backport: fix CVE-2010-0436: local root hole relating to command sockets


------------------------------------------------------------------------
r1114498 | graesslin | 2010-04-14 06:51:54 +1200 (Wed, 14 Apr 2010) | 5 lines

Backport rev 1114497:
Update the tab-order when adding a new desktop in kcmdesktop.
Thanks to Jeffery MacEachern for the patch.


------------------------------------------------------------------------
r1114959 | scripty | 2010-04-15 13:41:53 +1200 (Thu, 15 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1114967 | hindenburg | 2010-04-15 14:49:21 +1200 (Thu, 15 Apr 2010) | 4 lines

Prevent crash if no "Copy To" is active and user selects "Copy To None"

BUG: 234330

------------------------------------------------------------------------
r1114968 | hindenburg | 2010-04-15 14:51:22 +1200 (Thu, 15 Apr 2010) | 1 line

update version
------------------------------------------------------------------------
r1114971 | hindenburg | 2010-04-15 15:33:51 +1200 (Thu, 15 Apr 2010) | 4 lines

Don't crash when saving session data; don't use a reference to a freed object.

CCBUG: 232584

------------------------------------------------------------------------
r1115243 | fgeyer | 2010-04-16 06:18:39 +1200 (Fri, 16 Apr 2010) | 8 lines

Backport r1115238.

Fix PowerDevil always suspending the system twice and not waiting the
configured time before suspending/shutting down.

CCBUG: 221637


------------------------------------------------------------------------
r1115252 | fgeyer | 2010-04-16 06:45:57 +1200 (Fri, 16 Apr 2010) | 7 lines

Backport r1115250.

Fix bug that causes idle actions to be triggered only once per session.
d->status was never reset to NoAction after such an action.

CCBUG: 221648

------------------------------------------------------------------------
r1115344 | scripty | 2010-04-16 13:50:56 +1200 (Fri, 16 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1115599 | scripty | 2010-04-17 13:42:24 +1200 (Sat, 17 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1115610 | hindenburg | 2010-04-17 15:34:56 +1200 (Sat, 17 Apr 2010) | 4 lines

Use the default profile if the session management's Konsole file is unusable (corrupt/missing/etc).

BUG: 203621

------------------------------------------------------------------------
r1115717 | ossi | 2010-04-17 20:53:49 +1200 (Sat, 17 Apr 2010) | 7 lines

re-add xscreensaver check

BUG: 234336

based on http://reviewboard.kde.org/r/3605


------------------------------------------------------------------------
r1116587 | mart | 2010-04-20 08:12:26 +1200 (Tue, 20 Apr 2010) | 2 lines

backport fix to bug 234010

------------------------------------------------------------------------
r1116952 | mart | 2010-04-21 05:15:32 +1200 (Wed, 21 Apr 2010) | 2 lines

backpot window border fix

------------------------------------------------------------------------
r1116983 | craig | 2010-04-21 07:34:19 +1200 (Wed, 21 Apr 2010) | 7 lines

In policykit, the persistence was given implicitely for the running
application, whereas for polkit this has to be given explicitely.

Using 'Persistence=session' resolves this.

BUG: 228765

------------------------------------------------------------------------
r1116987 | craig | 2010-04-21 07:40:04 +1200 (Wed, 21 Apr 2010) | 2 lines

Set icon

------------------------------------------------------------------------
r1117007 | hpereiradacosta | 2010-04-21 08:53:52 +1200 (Wed, 21 Apr 2010) | 4 lines

Backport r117005:
Fixed hover over focus precedence for non-editable comboboxes.


------------------------------------------------------------------------
r1117024 | hpereiradacosta | 2010-04-21 10:00:06 +1200 (Wed, 21 Apr 2010) | 4 lines

Backport r1117023
Do not draw hover/focus frame for disabled 'input' frames.


------------------------------------------------------------------------
r1117060 | hpereiradacosta | 2010-04-21 15:24:34 +1200 (Wed, 21 Apr 2010) | 4 lines

Backport r1117059
fixed mouseOver precedence over Focus for pushButtons when animating


------------------------------------------------------------------------
r1117331 | scripty | 2010-04-22 09:37:43 +1200 (Thu, 22 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117546 | lunakl | 2010-04-23 01:14:19 +1200 (Fri, 23 Apr 2010) | 3 lines

backport, qt4.6.0 already includes Key_Display


------------------------------------------------------------------------
r1117696 | trueg | 2010-04-23 07:19:05 +1200 (Fri, 23 Apr 2010) | 1 line

Backport: Delete the old model if it is empty instead of converting. This prevents dangling Virtuoso sessions.
------------------------------------------------------------------------
r1117789 | scripty | 2010-04-23 13:57:24 +1200 (Fri, 23 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117839 | trueg | 2010-04-23 20:29:50 +1200 (Fri, 23 Apr 2010) | 1 line

Backport: do not update the systray tooltip and status all the time to lower the repaint rate
------------------------------------------------------------------------
r1117912 | mart | 2010-04-24 00:36:34 +1200 (Sat, 24 Apr 2010) | 2 lines

backport a removal of code duplication in prevision of a bigger fix (since the design in 4.4 is conceptually wrong and buggy)

------------------------------------------------------------------------
r1117980 | mart | 2010-04-24 04:22:04 +1200 (Sat, 24 Apr 2010) | 4 lines

backport r1117944:
manage plasmoid tasks in a correct way
CCBUG: 235066

------------------------------------------------------------------------
r1117985 | mart | 2010-04-24 04:38:00 +1200 (Sat, 24 Apr 2010) | 3 lines

backport r1117982
forward constraints only to plasmoids belonging the proper systray

------------------------------------------------------------------------
r1118452 | orlovich | 2010-04-25 07:39:36 +1200 (Sun, 25 Apr 2010) | 6 lines

Backport  abunch of fixes from trunk:
- Avoid some pointless wakeups
- Fix initialization in top-level mode
- Partial npruntime support, to make new youtube skin work.
Invasive, but critical

------------------------------------------------------------------------
r1118537 | scripty | 2010-04-25 13:57:49 +1200 (Sun, 25 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1118845 | scripty | 2010-04-26 13:46:26 +1200 (Mon, 26 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1118934 | anschneider | 2010-04-26 20:54:31 +1200 (Mon, 26 Apr 2010) | 4 lines

kio_sftp: Fixed the slave to use the kwallet password.

This should remember the password correctly too now.

------------------------------------------------------------------------
r1119398 | scripty | 2010-04-27 14:15:55 +1200 (Tue, 27 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1119881 | johnflux | 2010-04-28 09:36:14 +1200 (Wed, 28 Apr 2010) | 3 lines

Limit the percentage to 100% when drawing the background for it.

There was code for this before, but it was done too late as to be useless
------------------------------------------------------------------------
r1119882 | johnflux | 2010-04-28 09:36:31 +1200 (Wed, 28 Apr 2010) | 5 lines

Add better solaris support

Written by: Jan Hnatek

(I made only whitespace fix changes)
------------------------------------------------------------------------
r1119883 | johnflux | 2010-04-28 09:36:47 +1200 (Wed, 28 Apr 2010) | 3 lines

Fix CPU and Network sensors on Solaris

Written by: Jan Hnatek + whitespace fixes
------------------------------------------------------------------------
r1119884 | johnflux | 2010-04-28 09:37:03 +1200 (Wed, 28 Apr 2010) | 3 lines

Disable the ionice radio boxes if there's no ionice support

Based on a patch by: Jan Hnatek  - Thanks!
------------------------------------------------------------------------
r1119950 | scripty | 2010-04-28 13:55:04 +1200 (Wed, 28 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1119960 | hindenburg | 2010-04-28 14:45:32 +1200 (Wed, 28 Apr 2010) | 6 lines

Correct ANSI's 'delete char' to delete the last character in a line.

Patch provided by Alexandre Becoulet

CCBUG: 217669

------------------------------------------------------------------------
r1120086 | dfaure | 2010-04-28 22:40:16 +1200 (Wed, 28 Apr 2010) | 6 lines

Remove connection to a slot that doesn't exist (and no such slot nor connection in trunk, so I assume this is the right fix).

However in this branch dolphin fails soon after due to
No such signal DolphinController::requestUrlChange
(I'm using QT_FATAL_WARNINGS=1 to catch such things nowadays)

------------------------------------------------------------------------
r1120142 | dfaure | 2010-04-29 01:24:26 +1200 (Thu, 29 Apr 2010) | 3 lines

Backport fix for bug 210551 (crash when closing tabs fast) to the 4.4 branch, so that the fix is in 4.4.3.
CCBUG: 210551

------------------------------------------------------------------------
r1120173 | ppenz | 2010-04-29 02:53:47 +1200 (Thu, 29 Apr 2010) | 3 lines

Remove connection to non-existent signal (the backport of SVN commit 1080351 has removed the signal requestUrlChange()).

On trunk this issue has already been resolved because of the DolphinController improvements.
------------------------------------------------------------------------
r1120395 | scripty | 2010-04-29 13:59:21 +1200 (Thu, 29 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1120719 | mueller | 2010-04-30 07:52:27 +1200 (Fri, 30 Apr 2010) | 2 lines

bump version

------------------------------------------------------------------------
