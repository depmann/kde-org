---
aliases:
- ../../plasma-5.4.2
changelog: 5.4.1-5.4.2
date: 2015-10-06
layout: plasma
figure:
  src: /announcements/plasma/5/5.4.0/plasma-screen-desktop-2-shadow.png
---

{{< i18n_date >}}

{{< i18n "annc-plasma-bugfix-intro" "5" "5.4.2" >}}

{{% i18n "annc-plasma-bugfix-minor-release-8" "5.4" "/announcements/plasma/5/5.4.0" "2015" %}}

{{< i18n "annc-plasma-bugfix-worth-5" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

- <a href="https://kdeonlinux.wordpress.com/2015/09/15/breeze-is-finished/">Many new Breeze icons</a>.
- Support absolute libexec path configuration, fixes binaries invoked by KWin work again on e.g. Fedora. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=85b35157943ab4e7ea874639a4c714a10feccc00">Commit.</a> Fixes bug <a href="https://bugs.kde.org/353154">#353154</a>. Code review <a href="https://git.reviewboard.kde.org/r/125466">#125466</a>
- Set tooltip icon in notifications applet. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3f8fbd3d4a6b9aafa6bbccdd4282d2538018a7c6">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/125193">#125193</a>
