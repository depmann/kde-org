---
aliases:
- ../../plasma-5.17.90
changelog: 5.17.5-5.17.90
date: 2020-01-16
layout: plasma
noinfo: true
title: 'Plasma 5.18 LTS Beta: More Convenient and with Long Term Stability'
figure:
  src: /announcements/plasma/5/5.18.0/plasma-5.18.png
---

{{% i18n_date %}}

The Plasma 5.18 LTS Beta is out! This new version of your favorite desktop environment adds neat new features that make your life easier, including clearer notifications, streamlined settings for your system and the desktop layout, much improved GTK integration, and more. Plasma 5.18 is easier and more fun, while at the same time allowing you to do more tasks faster.

Apart from all the cool new stuff, Plasma 5.18 also comes with LTS status. LTS stands for "Long Term Support" and this means 5.18 will be updated and maintained by KDE contributors for the next couple of years (regular versions are maintained for 4 months). So, if you are thinking of updating or migrating your school, company or organization to Plasma, this version is your best bet. You get the most recent stable version of Plasma for the long term.

Read on to discover everything that is new in Plasma 5.18 LTS…

## Plasma

{{<figure src="/announcements/plasma/5/5.18.0/emoji.png" alt="Emoji Selector"  caption="Emoji Selector" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.18.0/customize-layout.png" alt="Customize Layout Global Settings"  caption="Customize Layout Global Settings" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.18.0/gtk-csd-theme.png" alt="GTK Apps with CSD and Theme support"  caption="GTK Apps with CSD and Theme support" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.18.0/night-color-applet.png" alt="Night Color System Tray Widget"  caption="Night Color System Tray Widget" width="600px" >}}

- Emoji Selector that can be opened through the application launcher or with the <kbd>Meta</kbd> + <kbd>.</kbd> keyboard shortcut
- New global edit mode which replaces the desktop toolbox button and lets you easily customize your desktop layout
- Improved touch-friendliness for the Kickoff application launcher and widget editing
- Support for GTK applications which use Client Side Decorations, adding proper shadows and resize areas for them
- GTK apps now also automatically inherit Plasma's settings for fonts, icons, cursors and more.
- There's a new System Tray widget for toggling the Night Color feature and by default it automatically appears when it's on
- More compact design to choose the default audio device in the Audio Volume System Tray widget
- Clickable volume indicator and tooltip item highlight indicators in the Task Manager
- Circular Application Launcher menu user icon
- Option to hide the lock screen clock
- It's now possible to configure keyboard shortcuts that turn Night Color and Do Not Disturb mode on or off
- Windy conditions shown in weather widget

## Notifications

{{<figure src="/announcements/plasma/5/5.18.0/draggable-download.png" alt="Draggable Download File Icon"  caption="Draggable Download File Icon" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.18.0/bluetooth-battery.png" alt="Low Bluetooth Battery"  caption="Bluetooth Device Battery Low Notification" width="600px" >}}

- The timeout indicator on notification popups has been made circular and surrounds the close button
- A draggable icon in the "file downloaded" notification has been added, so you can quickly drag it to places
- Plasma now shows you a notification warning when a connected Bluetooth device is about to run out of power

## System Settings

{{<figure src="/announcements/plasma/5/5.18.0/user-feedback.png" alt="User Feedback"  caption="User Feedback" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.18.0/application-style.png" alt="Application Style"  caption="Application Style" width="600px" >}}

- Plasma gained optional User Feedback settings (disabled by default), allowing you to give us detailed system information and statistics on how often individual features of Plasma you use
- Added a slider for the global animation speed
- Redesigned Application Style settings with a grid view
- Improved the search in the system settings sidebar
- An option to scroll to clicked location in the scrollbar track has been added
- The System Settings Night Color page has a clearer user interface now

## Discover

{{<figure src="/announcements/plasma/5/5.18.0/discover-comments.png" alt="Reading and Writing Review Comments"  caption="Reading and Writing Review Comments" width="600px" >}}

- Discover's default keyboard focus has been switched to the search field
- It's now possible to search for add-ons from the main page
- Added nested comments for addons
- Made design improvements to the sidebar header and reviews

## More

{{<figure src="/announcements/plasma/5/5.18.0/nvidia.png" alt="NVIDIA GPU stats"  caption="NVIDIA GPU stats" width="600px" >}}

- Decreased the amount of visual glitches in apps when using fractional scaling on X11
- Made it possible to show NVIDIA GPU stats in KSysGuard

## New Since 5.12 LTS

For those upgrading from our previous Long Term Support release here are some of the highlights from the last two years of development:

- Completely rewritten notification system
- Plasma Browser Integration
- Many redesigned system settings pages, either using a consistent grid view or just an overhauled interface
- Global menu support for GTK applications
- Display Management improvements including new OSD and widget
- Flatpak portal support
- Night Color feature
- Thunderbolt Device Management
