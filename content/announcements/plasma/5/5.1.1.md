---
aliases:
- ../../plasma-5.1.1
date: '2014-11-11'
layout: plasma
---

{{<figure src="/announcements/plasma/5/5.1.0/plasma-main.png" class="text-center float-right ml-3" caption="Plasma 5" width="600px" >}}

{{< i18n_date >}}

{{< i18n "annc-plasma-bugfix-intro" "5" "5.1.1" >}}

{{% i18n "annc-plasma-bugfix-minor-release-10" "5.1" "/announcements/plasma/5/5.1.0" "2014" %}}

{{< i18n "annc-plasma-bugfix-worth-5" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

{{% stop-translate %}}

- Limiting indexing word size in Baloo.
- Don't index dots in Baloo, it's a regular expression character
- Breeze: Do not takeout margins from toolbutton before rendering text
- Breeze: cleanup tab buttons positioning
- Breeze: Fix positioning of cornerwidgets
- Notes widget: Make text color white on black note
- Clock widget: Fix fuzzy clock saying half past seven when it's half past six
- khotkeys: fix loading configuration
- kinfocenter: Set the correct version
- kcm-effects: Use Loader for the Video Item
- Oxygen: margins and RTL fixes
- Plasma Desktop: Validate timezone name before setting
- Plasma Desktop: Backport settings made in the component chooser to kdelibs 4 applications
- Plasma Desktop: make kdelibs 4 apps react to icon theme change
- Plasma Desktop: Cleanup applet configuration scrollbar handling and fix glitching alternatives dialog
- Plasma Desktop: fix emptying the trash from the context menu
- Plasma Desktop: numberous bugfixes
- Plasma Workspace: Remove shutdown option from screen locker
