---
custom_about: true
custom_contact: true
title: Corel Computer and KDE Announce Technology Relationship
date: "1998-11-25"
description: Corel Computer and the K Desktop Environment (KDE) Project today announced a technology relationship that will bring the KDE graphical user interface (GUI) to desktop versions of the NetWinder Linux-based thin-client and thin-server computers.
---

<center>

<i>Linux desktop computing made easier and friendlier through collaborative
project</i></center>

Corel Computer Media Contact 

Cindy Scott 

cindys@corelcomputer.ca

613-788-6000 x6101

<br>

Ottawa, Canada--November 25, 1998--Corel Computer and the K Desktop
Environment (KDE) Project today announced a technology relationship that
will bring the KDE graphical user interface (GUI) to desktop versions of
the NetWinder� Linux-based thin-client and thin-server computers.

The alliance between Corel Computer and KDE, a non-commercial association
of Open Source&reg; programmers, provides NetWinder users a sophisticated
front-end to Linux, a stable and robust UNIX&reg;-like operating system.

"The KDE GUI is a key piece of our strategy to deliver highly compatible,
high-value, Linux-powered desktops to our corporate customers," said Ron
McNab, vice president and general manager of Corel Computer. "This is part
of our ongoing commitment to our customers by providing easy-to-use management
and productivity tools for the NetWinder."

Corel Computer intends to participate with the KDE Project to bring
new skills and technology to this phenomenal desktop environment. Corel
Computer has shipped a number of NetWinder DM, or development machines,
to KDE developers who are helping to port the desktop environment. Additionally,
NetWinder.Org developers, Raffaele Saena and John Olson, were responsible
for championing development of KDE on the NetWinder.

"Through the collaboration of KDE and Corel Computer, networking with
the NetWinder will now be as easy as toasting your bread in the morning,"
said Uwe Thiem, the African-region representative of the KDE Team. "This
collaboration and the port to the NetWinder gives us an opportunity to
deliver our free desktop to even more users. We are confident KDE will
gain widespread acceptance of users because of its outstanding functionality
and ease of use."

Corel Computer plans to make desktop versions of the NetWinder running
KDE available in early 1999. Early demonstrations of the port, such as
the one shown at the Open Systems fair in Wiesbaden, Germany, in September,
have been enthusiastically received by potential customers.

Based on the Open Source model, Corel Computer is devoting internal
development resources to the improvement of the KDE project including rigorous
testing of the environment on the NetWinder. As a developing partner, Corel
Computer will release its work back to the KDE development community.

KDE Information

The K Desktop Environment is a non-commercial, international, Internet-based
project which develops the freely available graphical desktop environment
for the UNIX&reg; and Linux&reg; platforms. The completely new Internet
enabled desktop, incorporating a large suite of applications for Unix workstations
currently supports more than 25 languages. The strength of this exceptional
environment lies in the interoperability of its components and the functionality
of its applications. For more information about KDE and its technology,
please visit the web site at www.kde.org or contact press@kde.org.

Corel Computer

Corel Computer, a global player in the design and manufacture of high
quality thin-client, thin-server and video conferencing products, markets
the NetWinder� and CorelVIDEO� product lines as part of a strategy to merge
communications and computing on the desktop. As a vigorous proponent of
open standards and vendor neutral technology, Corel Computer has incorporated
the StrongARM� microprocessor and the Linux operating system into its compact,
powerful NetWinder network computing products, which deliver more than
250 MIPS yet consumes a fraction of the power of an average desktop computer.
Its CorelVIDEO platform comprises reliable, scalable tools for face-to-face
video communications, whether across the hall or around the world. Corel
Computer is a division of Corel Corporation, an award-winning developer
of graphics software and productivity applications. Company information
is available on the World Wide Web at www.corelcomputer.com.

<center>-30-</center>

<font size=-2>Corel and CorelVIDEO are trademarks or registered trademarks
of Corel Corporation. Used with permission. NetWinder is a trademark of
Corel Computer Corp. StrongARM is a registered trademark of ARM Ltd. Linux
is a registered trademark of Linus Torvalds. Used with permission. Open
Source is a certification mark of Software in the Public Interest. Used
with permission.</font>
