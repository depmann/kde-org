---
title: Kirigami 1.1
release: kirigami-1.1
description: KDE Ships Kirigami 1.1.
date: 2016-09-26
---

After weeks of development and two small bugfix releases, we are happy to announce the first Kirigami minor release, version 1.1.

The Menu class features some changes and fixes which give greater control over the action triggered by submenus and leaf nodes in the menu tree. Submenus now know which entry is their parent, and allow the submenu's view to be reset when the application needs it to.

The OverlaySheet now allows to embed ListView and GridView instances in it as well.

The Drawer width now is standardized so all applications look coherent from one another and the title now elides if it doesn't fit. We also introduced the GlobalDrawer.bannerClicked signal to let applications react to banner interaction.

SwipeListItem has been polished to make sure its contents can fit to the space they have and we introduced the Separator component.

A nice fix for desktop Kirigami applications: The application window now has a default shortcut to close the application, depending on the system preferences, commonly Ctrl+Q.

Naturally this release also contains smaller fixes and general polishing.

As announced <a href="/announcements/plasma-5.7.95">recently</a>, Plasma 5.8 will ship a revamped version of Discover based on Kirigami. As such we expect that all major distributions will ship Kirigami packages by then if they are not already doing so.

## Source Download

The source code for Kirigami releases is available for download from <a href='http://download.kde.org/stable/kirigami/'>download.kde.org</a>.

More information is on the <a href='https://techbase.kde.org/Kirigami'>Kirigami wiki page</a>.

GPG signatures are available alongside the source code for
verification. They are signed by release manager <a
href='https://sks-keyservers.net/pks/lookup?op=vindex&search=0xeaaf29b42a678c20'>Marco Martin with 0xeaaf29b42a678c20</a>.

## {{< i18n "feedback" >}}

You can give us feedback and get updates on <a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='/announcements/buttons/facebook-logo.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a> or <a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='/announcements/buttons/twitter-logo.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a> or <a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='/announcements/buttons/googleplus-logo.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>.

{{% i18n "discuss-forum" %}}

{{% i18n "report-bug-plasma" %}}

{{% i18n "feedback-appreciated" %}}

## {{< i18n "supporting-kde" >}}

{{% i18n "whatiskde" %}}
