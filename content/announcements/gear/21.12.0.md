---
description: KDE brings you the gift of Free Software with Gear ⚙️ 21.12
authors:
  - SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
  - SPDX-FileCopyrightText: 2021 Aniqa Khokhar <aniqa.khokhar@kde.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2021-12-09
hero_image: hero.png
images:
  - /announcements/gear/21.12.0/hero.png
outro_img:
  link: all_apps.png
  alt: Screenshots of many applications
layout: gear
---

# Welcome to KDE Gear ⚙️ 21.12!

[Skip to _What's New_](#whatsnew)

KDE Gear 21.12 has landed and comes with a massive number of updates and new versions of applications and libraries. Literally, dozens of classic KDE everyday tools and the specialised sophisticated apps you use to work, be creative and play, are getting refreshers with design improvements, new features and performance and stability enhancements.

And the whole set of packages comes just in time for the season of giving. Hanukkah/the Winter Solstice/the Generic Mid-Winter Holiday/Christmas/whatever-you-celebrate is just around the corner, so why not share with those that are less fortunate, that is, those who do not use KDE software yet? 😜

Install Plasma for your friends and family and deck them out with the brand new versions of KDE's utilities and programs!

## What is KDE Gear

[Skip to _What's New_](#whatsnew)

KDE Gear is a set of apps and other software created and maintained by the KDE Community that release all new versions at the same time. KDE Gear may also include new apps that we deem have reached enough maturity to be included in the release.

[Here you can see the full list of apps and libraries](https://invent.kde.org/sysadmin/release-tools/-/blob/release/21.12/modules.git) that are releasing new versions in KDE Gear ⚙️ 21.12.

When we announce a new version of KDE Gear (like we are doing today), you can expect the new versions to appear in your Linux distros shortly afterwards. In some distributions, new versions will appear nearly immediately -- such as in the case of [Neon](https://neon.kde.org/). In others, it may take days or even weeks, as it depends on your distro's packagers.

New versions of KDE Gear apps also pop up in independent app stores, such as FlatHub or the Snap Store; and as updates on non-Linux platforms, such as on Windows.

# <a id="whatsnew"></a>What's New

## [Dolphin](https://apps.kde.org/dolphin/)

Dolphin is KDE's intuitive, but powerful folder and file manager that lets you copy, remove, duplicate, examine and move files and folders located on your internal disk drive, removable storage media and on remote hosts.

The new version of Dolphin includes some clever features that allow you to locate and identify files and folders easily and quickly. Take the _Filter_ feature for example: press <kbd>Ctrl</kbd> + <kbd>i</kbd> and a box will appear under the main panel. You can type anything you want to search for in the box, say "_.txt_", and Dolphin will hide everything that does not contain the letters "_.txt_" in its name. Very useful for finding what you are looking for. 

This is not a new feature, however. What is new is that, if you enter the _Detailed View_ (rightclick on an empty space in the main panel and select _View Mode_ > _Details_ from the pop up menu), you can avoid the list getting too long and confusing by using the  _Filter_ feature to hide open folders that do not contain files that match your search criteria.

![Dolphin's filter hides irrelevant folders](Dolphin_details_filter.png) 

Another way of finding stuff easily in the main panel is by knowing where things are. Although, as their name implies, hidden files are not usually shown, if you need to access them, you can make them show up in the files and folders panel by going to the hamburger menu and ticking the _Show Hidden Files_ or, if you have the Menu Bar, _View_ > _Show Hidden Files_.

It is quite normal to have dozens of hidden files and folders cluttering the view when displayed. To make things easier, Dolphin lets you push them under the regular files and folders. To do that, tick the _Hidden Files Last_ checkbox you will find under the _Sort by_ option in the Hamburger menu, or click _View_ > _Sort by_ to do the same thing.

Being able to preview the contents of a file and folder also makes managing your stuff simpler. Dolphin adds previews for comic book files (files with the _.cbz_ extension) containing WEBP-formatted images, and improves its icon zooming feature.

Other behind the scenes improvements include how Dolphin remembers its location and size on the desktop, corrections for bugs that could affect stability in exceptional edge cases, and tweaks to the interface and dialogues that help users do stuff quicker and easier.

## [Spectacle](https://apps.kde.org/spectacle/)

Spectacle is a seemingly simple application for capturing desktop screenshots, but that, following KDE's "simple by default, powerful when needed" mantra, comes with a long list of features to annotate, edit and share your shots. As such, the long list of settings you will find in the configuration dialog can be a bit intimidating. To make things clearer, in this new version, all the options are there, but they are collected into neat, clearly labelled comboboxes you can unfold and find the stuff you need and not be overwhelmed by the stuff you don't.

While you are fiddling with Spectacle's settings, one of the new things you can do is configure what Spectacle will do when it starts up. You can set it to automatically take a full-screen screenshot; make it remember the exact same parameters for the last screenshot you took before closing down (for example, highlight the exact same region of the desktop of your last rectangle screenshot), and then_ automatically take a screenshot; or you can configure Spectacle to not take a screenshot at all!

![Spectacle's cleaner and clearer configuration dialog](spectacle_config_after.png) 

Spectacle has also improved the look of the images as you drag and drop them from Spectacle's preview panel to Dolphin to save them or to an online image storage site to share them — yes, you can do that too. And, for those with more specialized set-ups, Spectacle can take correctly-colored screenshots on screens with 10-bit-per-channel color support enabled. Meanwhile, under Wayland, Spectacle continues to add features, like the capture "Active Window" which is now available both on the X11 and Wayland versions.

## [Kdenlive](https://kdenlive.org/)

As the demand for the support of more and more sophisticated video projects grow, so does the list of Kdenlive's features, adding new tools, improving the interface and working on the reliability and performance of the whole. Whether you are working on your latest movie or just want to edit your holiday clips and the videos of your kids building a snowman and opening presents, you will find that Kdenlive's new features add that extra polish that makes your films a cut above the rest.

Take for example the _Noise Suppressor for Voice_. This new audio effect removes background noise from your recordings like magic. Grab it from the audio effects tab and drop it onto the audio track you want to clean up. Even without any tweaking, you will notice that the noise from wind, traffic, or waves, will be diminished, while the voices of people will be enhanced. This is sound engineering done easy!

{{< video src="https://cdn.kde.org/promo/Announcements/Apps/21.12/noisereduction.mp4" src-webm="https://cdn.kde.org/promo/Announcements/Apps/21.12/noisereduction.webm" >}}

As for video effects, try out the improved motion tracking tools. Kdenlive 21.12 comes with several tracking methods and we are adding new and more efficient ones all the time. You use motion tracking if you want an element to follow something else that is moving around in the frame. Say you want to blur out a child's face, but they are moving around, or you want a label with text to follow a character as they walk across the frame. Locate _Motion Tracker_ in the list effects and drag and drop it into the clip you want to track. Use the box in the monitor to cover the area you want to track, click _Analyse_ in the effects panel, and off you go!

{{< video src="https://cdn.kde.org/promo/Announcements/Apps/21.12/DaSiam_x4_cc.mp4" src-webm="https://cdn.kde.org/promo/Announcements/Apps/21.12/DaSiam_x4_cc.webm" >}}

Another thing Kdenlive makes easy is transitions. In Kdenlive 21.12, you don't have to set up multiple tracks and add compositions. Put two clips on the same track, grab the end edge of the first one and shorten it by between a half and a whole second by dragging its edge to the left; shorten the second clip by the same amount by dragging its edge to the right. Then close the gap between both by dragging them together. Next, select both clips (hold down the <kbd>Shift</kbd> key and click on one clip, and then, still holding down <kbd>Shift</kbd>, click on the other); hit the <kbd>u</kbd> key and... Hey presto! A transition will magically appear between both. You can later adjust the kind of transition in the _Effects/Composition Stack_ if you don't like the default.

When it comes to placing your clips in the timeline, use the new [slip trimming mode or ripple trimming mode](https://kdenlive.org/en/video-editing-applications-handbook/#trim). Select a clip in the timeline and choose the _Tool_ > _Slip_ tool from the menu, click on a shortened version of your clip and drag it back and forth to place the exact portion of the clip between adjacent clips.

Ripple trimming mode (_Tool_ > _Ripple tool_) lets you lengthen or shorten a clip by dragging on the beginning of the clip. The beginning of the clip remains in the same position on the timeline, while the end moves as you drag. You would use this tool when you are sure about where you want the clip to start, but need to tweak where it ends.

As you can see, working with many moving parts is easy with Kdenlive. With that in mind, you will also appreciate the new feature that allows you to create multiple project bins. Split up all your bits and pieces into folders (say, all your title cards into one folder, all your sound snippets into another, etc.). Then, right-click on each folder and choose the _Open in new bin_ option from the pop-up menu. Kdenlive will open separate tabs ("bins") containing the stuff that was originally in each folder, and nothing else.

And, while you are working with several tracks, the multicam editing function will prove invaluable. Add your clips (up to four) to different tracks in the same position in the timeline and activate the multicam tool by selecting _Tool_ > _Multicam tool_ from the menu. You can skip from track to track for editing by pressing <kbd>1</kbd>, <kbd>2</kbd>, <kbd>3</kbd>, or <kbd>4</kbd> on your keyboard, or simply click on the desired track in the monitor.

![Kdenlive's multi-monitor](kdenlive_multicam.png) 

The point is you can use Kdenlive to make the simplest of movies, just by cutting and assembling clips on the timeline, or you can go full Brian DePalma with fancy transitions and effects. Either way, Kdenlive is the best video editor you can't buy (because it is free).

## [Konsole](https://konsole.kde.org/)

One of Linux's most powerful features is that it is very easy to drop down to a text terminal where you can type in instructions directly from the operating system to run. Although it may seem intimidating at first, even a passing knowledge of terminal commands makes working with Linux much more fun and, often, will improve your workflow considerably.

Konsole is an ideal tool for newcomers wanting to explore what the command line has to offer, because, apart from providing the basics (i.e. a terminal), it combines features and options that leverage the fact it is also a graphical application. You can split the panels in any way you want and open multiple tabs so you can run several tasks simultaneously, hover your cursor over the name of a file in a text listing and Konsole will show you a preview in a popup window. You can click on a file and Konsole will open it in its associated app (an image in an image viewer, or a document in a word processor, for example), ready for work.

So, Konsole combines all the power of a text terminal with the convenience and friendliness of a graphical application and the new version of Konsole being released today has taken a step further in the direction of visual convenience and drastically improves and simplifies the default tool bar by putting all of the layout and split-related items into a dropdown menu button. You can also control the visibility of the menubar visibility with one option.

![Konsole's menu for splitting panels](konsole_splitmenu_full.png) 

It is important to use applications that are visually appealing, but, as beauty is in the eyes of the beholder, Konsole, in classic KDE fashions gives you plenty of chances of customizing its look to your taste. Look under the _Settings_ menu, and you will find options that let you change the color schemes used in the terminal panels, and even the look of Konsole's interface — independently from the rest of your desktop theme if you wish.

On a side note, for people who need to access remote machines, the _SSH Manager_ is finally here! (It was promised for Konsole 21.08, but was removed last minute due to not being quite ready), thus further contributing to making Konsole simple, but powerful at the same time, and an overall convenient and attractive way into the world of the Linux terminal.

## ... And all this too:

* [Elisa](https://elisa.kde.org/), KDE's cool music player, becomes even more stylish with tweaks to the interface and more streamlined options that don't get in the way of rocking out to your beats.

![Elisa in party mode](Elisa_party_mode.png)

* While changing the size of an image in [Gwenview](https://apps.kde.org/gwenview/), KDE's versatile image viewer, the app tells you how much a picture will take up in your hard disk before even applying the changes.

* The latest version of [KDE Connect](https://kdeconnect.kde.org/), the app that links your phone to your desktop, lets you send messages just by pressing <kbd>Enter</kbd> — if you want to add a new line in the message, just press <kbd>Shift</kbd> + <kbd>Enter</kbd>.

* [KDE Itinerary](https://apps.kde.org/itinerary/) 21.12, your digital travel assistant, comes with a revamped user interface that takes into account COVID 19 health certificates and vaccination data; and shows the names of the countries you have visited as well as your travel dates.

* [Kate](https://kate-editor.org/), KDE's Advanced Text Editor, now lets you open multiple tabs in its embedded terminal views; the git integration plugin allows you to delete branches, and implements sessions by default, meaning that all of your session-specific data (opened documents, layouts, etc.) are automatically remembered by default.

* [KolourPaint](https://apps.kde.org/kolourpaint/), the simple painting program, got a visual makeover, giving it kool new looks.

* [Kontact](https://kontact.kde.org/) lets you manage all your calendars, contacts and emails. Configuring resources and collections (like your mail folders) is now easier, and Outlook users will appreciate the added stability when accessing their accounts.

* [Akregator](https://apps.kde.org/akregator/) is KDE's news feed reader and the new version lets you search through the articles you already read and makes it much easier to update your feeds.

* [Skanlite](https://apps.kde.org/skanlite/), an app that lets you scan documents and images, lets you scan directly to a single-page PDF and remembers the last-used scanner and image format when you save to a file.

* Talking of scanning and, in this case, your file system: [Filelight](https://apps.kde.org/filelight/), the app that allows you to see a clear graph of the state of your storage devices, uses a multi-threaded file system scanning algorithm in version 21.12, making it substantially faster when combing through all your files and folder.

* [Konqueror](https://apps.kde.org/konqueror/), KDE's classic file and web browser, now displays more information about certificate errors.

* [KCalc](https://apps.kde.org/kcalc/), KDE's calculator for Plasma desktop and mobile, comes with a new feature that shows a history view of all recently-run calculations.

---

{{< announcements/gear_major_outro >}}
