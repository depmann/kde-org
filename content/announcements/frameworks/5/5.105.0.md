---
qtversion: 5.15.2
date: 2023-04-08
layout: framework
libCount: 83
---


### Baloo

* Add conventional python virtual-env folder to exclude filters
* [TermGenerator] Skip all unprintable characters
* Define the translation domain of BalooEngine

### Breeze Icons

* Add icons for showing and hiding a virtual keyboard
* Redshift icons: Add missing semi-transparency, add new breeze-dark icons
* Update Redshift icons to off on & sun (bug 462215)
* Delete false Gparted and Kwikdisk icons (bug 467319)

### Extra CMake Modules

* KDE_INSTALL_TARGETS_DEFAULT_ARGS (KF6): drop KDE_INSTALL_INCLUDEDIR
* KF_INSTALL_TARGETS_DEFAULT_ARGS: drop KDE_INSTALL_INCLUDEDIR_KF

### Framework Integration

* Don't play sound for plain notification (bug 457672)

### KConfigWidgets

* KColorSchemeManager: don't override color scheme set by platform theme (bug 447029)
* CommandBar: Fix lastUsedActions not restored

### KContacts

* Add Address::geoUri getter and property

### KCoreAddons

* Prevent KSignalHandler leaking signalfd file descriptors

### KDeclarative

* API dox: add some minimal docs to namespace & classes to trigger coverage
* API dox: cover CalendarEvents in generated QCH file
* Overhaul configmodule docs

### KDESU

* Have KDE4 compat header emit compiler warnings about their use

### KDocTools

* kdoctools_install: fix doc detection in path with special chars

### KI18n

* Use compat headers with deprecation warnings for KuitMarkup/kuitmarkup.h
* cmake: Do not rebuild po and ts files if they did not change

### KIconThemes

* Add missing comma between enum values

### KImageFormats

* psd: Fix alpha blending (KF5)

### KIO

* CommandLauncher: call emitResult() as soon as process has started (bug 466359)
* Also handle copy_file_range failing with ENOENT
* widgets/renamefiledialog: set number limit again (bug 466636)

### Kirigami

* Fix quit action code
* GlobalDrawer: Fix header with invisible content taking up space
* ColorUtils: Handle cases where hue is -1 in linearInterpolate
* NavigationTabBar: Fix imports in doc example
* NavigationTabBar: Factor out minDelegateWidth part of expression
* Action Name Not required KF5
* Default page categorized settings
* Show back button when pushing a pagerow
* shadowed*rectangle: Don't use base class result if materials are different
* Set fallback theme path when a custom icon theme is used

### KNewStuff

* QtQuickDialogWrapper: Print out errors if component creation failed
* Deprecate knewstuffcore_version.h & knewstuffquick_version.h
* Add missing deps to KF5NewStuffCoreConfig

### KTextEditor

* try to improve test stability
* autoindent: fix indentation when "keep extra spaces" is enabled
* Julia indent: fix indentation when "keep extra spaces" is enabled,

### KWidgetsAddons

* Allow searching 2-character strings (bug 449003)
* Initialize KCharSelectTablePrivate::chr

### Plasma Framework

* DataEngines: Add forward compatibility as a porting aid
* containmentinterface: get applet position when menu key is pressed

### QQC2StyleBridge

* CheckIndicator: Allow exclusive buttons to be detected via their ButtonGroup (bug 467390)

### Solid

* Remove some obsolete and incorrect code from UPower and UDisks2 backend
* Deprecate "Recall" API for batteries
* Avoid synchronous DBus calls for devices list
* Initialize supported interfaces with member initializer list
* Replace generic UPower QDBusInterface with concrete implementation
* Remove support for UPower < 0.99
* Remove invalid Refresh DBus call from UPower backend

### Syntax Highlighting

* Highlight the QML "required" keyword, added in Qt 5.15

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
