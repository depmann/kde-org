---
aliases:
- ../../kde-frameworks-5.35.0
date: 2017-06-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### Attica

- Improve error notification

### BluezQt

- Pass an explicit list of arguments. This prevents QProcess from trying to handle our space containing path through a shell
- Fix property changes being missed immediately after an object is added (bug 377405)

### Breeze Icons

- update awk mime as it's a script language (bug 376211)

### Extra CMake Modules

- restore hidden-visibility testing with Xcode 6.2
- ecm_qt_declare_logging_category(): more unique include guard for header
- Add or improve "Generated. Don't edit" messages and make consistent
- Add a new FindGperf module
- Change default pkgconfig install path for FreeBSD

### KActivitiesStats

- Fix kactivities-stats into tier3

### KDE Doxygen Tools

- Do not consider keyword Q_REQUIRED_RESULT

### KAuth

- Verify that whoever is calling us is actually who he says he is

### KCodecs

- Generate gperf output at build time

### KCoreAddons

- Ensure proper per thread seeding in KRandom
- Do not watch QRC's paths (bug 374075)

### KDBusAddons

- Don't include the pid in the dbus path when on flatpak

### KDeclarative

- Consistently emit MouseEventListener::pressed signal
- Don't leak MimeData object (bug 380270)

### KDELibs 4 Support

- Handle having spaces in the path to CMAKE_SOURCE_DIR

### KEmoticons

- Fix: Qt5::DBus is only privately used

### KFileMetaData

- use /usr/bin/env to locate python2

### KHTML

- Generate kentities gperf output at build time
- Generate doctypes gperf output at build time

### KI18n

- Extend Programmer's Guide with notes about influence of setlocale()

### KIO

- Addressed an issue where certain elements in applications (e.g. Dolphin's file view) would become inaccessible in high-dpi multi-screen setup (bug 363548)
- [RenameDialog] Enforce plain text format
- Identify PIE binaries (application/x-sharedlib) as executable files (bug 350018)
- core: expose GETMNTINFO_USES_STATVFS in config header
- PreviewJob: skip remote directories. Too expensive to preview (bug 208625)
- PreviewJob: clean up empty temp file when get() fails (bug 208625)
- Speed up detail treeview display by avoiding too many column resizes

### KNewStuff

- Use a single QNAM (and a disk cache) for HTTP jobs
- Internal cache for provider data on initialisation

### KNotification

- Fix KSNIs being unable to register service under flatpak
- Use application name instead of pid when creating SNI dbus service

### KPeople

- Do not export symbols from private libraries
- Fix symbol exporting for KF5PeopleWidgets and KF5PeopleBackend
- limit #warning to GCC

### KWallet Framework

- Replace kwalletd4 after migration has finished
- Signal completion of migration agent
- Only start timer for migration agent if necessary
- Check for unique application instance as early as possible

### KWayland

- Add requestToggleKeepAbove/below
- Keep QIcon::fromTheme in main thread
- Remove pid changedSignal in Client::PlasmaWindow
- Add pid to plasma window management protocol

### KWidgetsAddons

- KViewStateSerializer: Fix crash when view is destroyed before state serializer (bug 353380)

### KWindowSystem

- Better fix for NetRootInfoTestWM in path with spaces

### KXMLGUI

- Set main window as parent of stand-alone popup menus
- When building menu hierarchies, parent menus to their containers

### Plasma Framework

- Add VLC tray icon
- Plasmoid templates: use the image which is part of the package (again)
- Add template for Plasma QML Applet with QML extension
- Recreate plasmashellsurf on exposed, destroy on hidden

### Syntax Highlighting

- Haskell: highlight "julius" quasiquoter using Normal##Javascript rules
- Haskell: enable hamlet highlighting for "shamlet" quasiquoter too

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
