---
aliases:
- ../announce-4.13.3
date: 2014-07-15
description: KDE Ships Applications and Platform 4.13.3.
title: KDE Ships July Updates to Applications, Platform and Plasma Workspaces
---

July 15, 2014. Today KDE released <a href="/announcements/announce-4.13.3">updates for its Applications and Development Platform</a>, the third in a series of monthly stabilization updates to the 4.13 series. This release also includes an <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2013-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.11.11&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=\">updated Plasma Workspaces 4.11.11</a>. Both releases contain only bugfixes and translation updates, providing a safe and pleasant update for everyone.

On the language front Farsi (Persian) reached the <a href="http://l10n.kde.org/stats/gui/stable-kde4/essential/">essential criteria</a> and thus will be again part of this release. And once again Kopete has some important fixes: fixed formatting both plain and html messages when OTR plugin is enabled, loaded but not used for encrypting messages in chat window and fixed generating of html messages in jabber protocol.

More than 40 recorded bugfixes include improvements to Personal Information Management suite Kontact, Umbrello UML Modeller, the Desktop search functionality, web browser Konqueror and the file manager Dolphin.

A more complete <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2014-01-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.13.3&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">list of changes</a> can be found in KDE's issue tracker.

To download source code or packages to install go to the <a href="/info/4/4.13.3">4.13.3 Info Page</a>. If you want to find out more about the 4.13 versions of KDE Applications and Development Platform, please refer to the <a href="/announcements/4.13/">4.13 release notes</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href="http://download.kde.org/stable/4.13.3/">download.kde.org</a> or from any of the <a href="/distributions">major GNU/Linux and UNIX systems</a> shipping today.

#### Supporting KDE

KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href="https://relate.kde.org/civicrm/contribute/transact?id=5">Join the Game</a> initiative.

#### Installing 4.13.3 Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.13.3 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href="/info/4/4.13.3#binary">4.13.3 Info Page</a>.

#### Compiling 4.13.3

The complete source code for 4.13.3 may be <a href="http://download.kde.org/stable/4.13.3/src/">freely downloaded</a>. Instructions on compiling and installing 4.13.3 are available from the <a href="/info/4/4.13.3">4.13.3 Info Page</a>.
