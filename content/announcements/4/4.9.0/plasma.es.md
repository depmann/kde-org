---
title: Espacios de trabajo Plasma 4.9 – Mejoras de los componentes base
date: "2012-12-05"
hidden: true
---

<p>
KDE se alegra de anunciar la inmediata disponibilidad de la versión 4.9 de los espacios de trabajo del Escritorio Plasma y de Plasma para Netbooks. Contienen mejoras en la funcionalidad existente del espacio de trabajo Plasma y también se han introducido un gran número de nuevas funcionalidades.
</p>
<h2>El gestor de archivos Dolphin</h2>
<p>
El potente gestor de archivos de KDE, Dolphin, incluye ahora los botones «Atrás» y «Adelante» y el regreso del cambio de nombre de archivos en línea. Dolphin puede mostrar metadatos, como puntuaciones, etiquetas, tamaños de imágenes y archivos, autor, fecha, entre otros, así como agrupar y ordenar por propiedades de los metadatos. El nuevo complemento para Mercurial permite manejar este sistema de control de versiones del mismo modo conveniente en el que se implementan git, SVN y CVS, de modo que los usuarios pueden ejecutar las órdenes «pull», «push» y «commit» desde el gestor de archivos. La interfaz de usuario de Dolphin contiene mejoras menores, como un panel de «Lugares» más avanzado, búsqueda mejorada y sincronización con la ubicación del terminal.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-dolphin_.png">
	<img src="/announcements/4/4.9.0/kde49-dolphin_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>El emulador de terminal X Konsole</h2>
<p>
La bestia de carga Konsole incorpora ahora la habilidad de buscar una selección de texto usando accesos rápidos Web de KDE. Ofrece la opción de contexto «Cambiar directorio a» cuando se suelta una carpeta en la ventana de Konsole. Los usuarios tienen más control para organizar las ventanas de la terminal <strong>desprendiendo pestañas</strong> y arrastrándolas para crear nuevas ventanas con ellas. Las pestañas existentes de pueden clonar en otras con el mismo perfil. La visibilidad del menú y de la barra de pestañas se puede controlar al iniciar Konsole. Para los que gustan de scripts, los títulos de las pestañas se pueden cambiar usando una secuencia de escape.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole1.png">
	<img src="/announcements/4/4.9.0/kde49-konsole1-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-konsole2.png">
	<img src="/announcements/4/4.9.0/kde49-konsole2-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>El gestor de ventanas KWin</h2>
<p>
El gestor de ventanas de KDE, KWin, ha recibido gran cantidad de trabajo. Las mejoras incluyen cambios sutiles como la elevación de ventanas durante la selección de ventanas y ayuda para preferencias específicas de las ventanas, así como cambios más visibles, como un mejorado módulo de control para el cambio de cajas y mejor rendimiento con las ventanas gelatinosas. Hay cambios para hacer que KWin funcione mejor con las Actividades, incluyendo la adición de reglas de ventanas relacionadas con las Actividades. El trabajo general se ha centrado en mejorar la calidad y el rendimiento de KWin.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-window-behaviour_settings.png">
	<img src="/announcements/4/4.9.0/kde49-window-behaviour_settings_thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<h2>Las Actividades</h2>
<p>
Las Actividades están ahora integradas más concienzudamente con los espacios de trabajo. Los archivos se pueden enlazar con las Actividades en Dolphin, Konqueror y la vista de carpetas. La vista de carpetas también puede mostrar solo aquellos archivos relacionados con una actividad en el escritorio o en un panel. Hay un nuevo esclavo KIO para Actividades y también es posible cifrar las Actividades privadas.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-link-files-to-activities.png">
	<img src="/announcements/4/4.9.0/kde49-link-files-to-activities-cropped.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<p>
Los espacios de trabajo introducen una implementación de MPRIS2, por lo que KMix tiene la posibilidad de manejar flujos de sonido y Plasma tiene un motor de datos para manejar este protocolo de reproducción de música. Estos cambios son acordes con la implementación de MPRIS2 en Juk y Dragon, los reproductores de música y de vídeo de KDE.
</p>
<p>
Existen más cambios menores en los espacios de trabajo, incluyendo varias adaptaciones a QML. El mejorado reproductor en miniatura de Plasma incluye un diálogo de propiedades de la pista y mejor filtrado. El menú de Kickoff se puede usar ahora con la única ayuda del teclado. El plasmoide de la gestión de redes contiene trabajos de usabilidad y de mejora visual. El elemento gráfico de Transporte público también contiene un buen número de cambios.
</p>

<h4>Instalación de Plasma</h4>

<p align="justify">
El software de KDE, incluidas todas sus bibliotecas y aplicaciones, está libremente disponible bajo licencias de Código Abierto. El software de KDE funciona sobre diversas configuraciones de hardware y arquitecturas de CPU, como ARM y x86, sistemas operativos y funciona con cualquier tipo de gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, puede encontrar versiones para Microsoft Windows de la mayoría de las aplicaciones de KDE en el sitio web <a href="http://windows.kde.org">KDE software on Windows</a> y versiones para Apple Mac OS X en el sitio web <a href="http://mac.kde.org/">KDE software on Mac</a>. En la web puede encontrar compilaciones experimentales de aplicaciones de KDE para diversas plataformas móviles como MeeGo, MS Windows Mobile y Symbian, aunque en la actualidad no tienen soporte. <a href="http://plasma-active.org">Plasma Active</a> es una experiencia de usuario para un amplio abanico de dispositivos, como tabletas y otro tipo de hardware 
móvil.
<br />
Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a
href="http://download.kde.org/stable/4.9.0/">download.kde.org</a>, 
y también en <a href="/download">CD-ROM</a>
o con cualquiera de los <a href="/distributions">principales
sistemas GNU/Linux y UNIX</a> de la actualidad.
</p>
<p align="justify">
  <a id="packages"><em>Paquetes</em></a>.
  Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de 4.9.0 
para algunas versiones de sus distribuciones, y en otros casos han sido voluntarios de la comunidad
los que lo han hecho. <br />
  Algunos de estos paquetes binarios están disponibles para su libre descarga en el sitio web de KDE <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">download.kde.org</a>.
  Paquetes binarios adicionales, así como actualizaciones de los paquetes ya disponibles,
estarán disponibles durante las próximas semanas.
<a id="package_locations"><em>Ubicación de los paquetes</em></a>.
Para una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE ha
sido notificado, visite la <a href="/info/4/4.9.0">Página de información sobre 4.9</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  La totalidad del código fuente de 4.9.0 se puede <a href="/info/4/4.9.0">descargar libremente</a>.
Dispone de instrucciones sobre cómo compilar e instalar el software de KDE 4.9.0
  en la <a href="/info/4/4.9.0#binary">Página de información sobre 4.9.0</a>.
</p>

<h4>
Requisitos del sistema
</h4>
<p align="justify">
Para obtener lo máximo de estos lanzamientos, le recomendamos que use una versión reciente de Qt, ya sea la 4.7.4 o la 4.8.0. Esto es necesario para asegurar una experiencia estable y con rendimiento, ya que algunas de las mejoras realizadas en el software de KDE están hechas realmente en la infraestructura subyacente Qt.<br />
Para hacer un uso completo de las funcionalidades del software de KDE, también le recomendamos que use en su sistema los últimos controladores gráficos, ya que pueden mejorar sustancialmente la experiencia del usuario, tanto en las funcionalidades opcionales como en el rendimiento y la estabilidad general.
</p>




<h2>También se han anunciado hoy:</h2>

<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Aplicaciones de KDE 4.9 nuevas y mejoradas</a></h2>
<p>
Hoy se han liberado aplicaciones de KDE nuevas y mejoradas que incluyen Okular, Kopete, KDE PIM y aplicaciones y juegos educativos. Lea el <a href="../applications">«Anuncio de aplicaciones de KDE»</a>.
</p>
<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>Plataforma de KDE 4.9</a></h2>
<p>
El lanzamiento de la plataforma de KDE de hoy incluye corrección de errores, otras mejoras de calidad, redes y preparación para Frameworks 5.
</p>
