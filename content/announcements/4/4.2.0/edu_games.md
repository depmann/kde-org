---
custom_about: true
custom_contact: true
hidden: true
title: Educational applications and Games
---

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../applications">
                <img src="/announcements/4/4.2.0/images/applications-32.png" />
                Previous page: Basic applications
                </a>        
        </td>
        <td align="right" width="50%">
                <a href="../platform">Next page: Development Platform
                <img src="/announcements/4/4.2.0/images/platform-32.png" /></a>
        </td>
    </tr>
</table>

<h2>Learn and Discover with the KDE Educational applications</h2>

<p>Desktop Planetarium, KStars, provides an accurate graphical simulation of the
night sky, from any location on Earth, at any date and time. It can control
telescopes and has many tools for the amateur or professional astronomer. This
new version now shows millions of stars, can predict conjunctions and has a Sky
Calendar. If you start it for the first time, a welcome wizard will show. After
choosing your location, you can click the "Download Extra Data..." to select
additional star and image data to install. Millions of stars are available in the
packages, as well as inline thumbnail images which show images of objects right
on the sky. There are several interesting educational applications of this
program as well. Pushing "CTRL-F" will bring up the find dialog. Type a name of
a heavenly object to find it, like the moon. You might want to turn off "Toggle
opaque ground" (most right button on the toolbar) to see the object if it is
below the horizon. If you now choose "Equatorial coordinates" by hitting the space
bar and set the time per second in the toolbar to 1 hour, watch. The moon moves
- but, seen from the equitorial, not in a horizontal line! Why is that? The
reason is that the earth spins, but not entirely as you might expect: there is a tilt and a
wobble to the spin of the earth, and thus the observer moves up and down...
Making science visible to children in such a way is a strong tool in the
classroom, and this is a truly educational application. And it is not just for
children, so be sure to have a look at this application if you are interested in
astronomy!</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/kstars.png">
	<img src="/announcements/4/4.2.0/kstars_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KStars is KDE's desktop planetarium, you can even control your telescope using it</em>
</div>
<br/>

<p><a href="http://edu.kde.org/kbruch/">KBruch</a> can be used to practice with
fractions. Kpercentage functions have been merged with KBruch so you can now
also practice with percentages. KAlgebra offers more complex mathematical
functionality, even going as far as supporting the MathML language used in
professional circles.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/kbruch.png">
	<img src="/announcements/4/4.2.0/kbruch_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KBruch makes learning fractals easy by visualizing equations</em>
</div>
<br/>

<p>Parley, the application which helps you learn other languages, has become easier
to use and can fetch translations automatically from online sources. KTurtle
became more fun as you can now export images and use a simple color picker to
change colors.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/parley.png">
	<img src="/announcements/4/4.2.0/parley.png" class="img-fluid">
	</a> <br/>
	<em>Parley makes learning a new language easier and more fun</em>
</div>
<br/>

<p>Viewing celestial bodies up close is now possible with Marble, the desktop
globe. Besides the Earth there are now maps for the Moon and Mars. You can view
the several Moon landing sites (including the impact location of the recent
Indian mission). On Mars you can see the spots where the Mars Exploration
Missions have taken place. If you want to have a look around these remote
places, start Marble either using "ALT-F2" (type "marble" and hit enter) or find
it in the menu. Marble might generate the maps first, and will then show you the
Earth. You can use the controls on the screen or the mousewheel to zoom in and
out, and by clicking and draging you can turn the globe around. Click "Map View"
on the bottom left to select another map. Besides the "Atlas" and "Satelite
View", there is an "OpenStreetMap" view which uses road data from the
volunteer-driven <a href="http://openstreetmap.org/">Open Street Map Project</a>.
It will download data from their servers to show you the maps up close. You can
also find a map of the world from 1689. To get the map of Mars, you have to
install a new map. This is easy using the build-in GNX interface. Go to the
"File" menu and choose "Maps". Look for the Mars map, either by using the search
function or scrolling the list, and click the "Install" button on the right
side. Marble will download and install the map. If you want, you can choose to
install other maps, or close the window. Now you will find the Mars map among
the selection of maps on the left side of the screen. Just click it, Marble will
load it and you can have a look around. To find out more about what's new in Marble,
have a look at <a href="http://edu.kde.org/marble/current_0.7.php">the visual
changelog to version 0.7</a>.</p>

<h2>Play with KDE Games</h2>

<p>The <a href="http://games.kde.org">KDE Games</a> provide you with a bit of
enjoyment during a day of hard work. The KDE Games team has been working on new
and improved artwork and introduced three new games: Killbots, Bomber and Kapman. In
Killbots, your goal is to make sure the robots don't get to you. You can move
away from them, but if they close in on you, there is the possibility of jumping
away. Be careful with that, as you need energy to jump safely to another
location. You can 'earn' energy by waiting out a round - do so if you're sure
the robots won't catch you even if you can't move anymore. 
</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/killbots.png">
	<img src="/announcements/4/4.2.0/killbots_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Escape from the Robots in Killbots</em>
</div>
<br/>

<p>In Bomber your goal is to destroy the buildings below you before you crash into
them. You use the spacebar to drop a bomb. The space ship will fly over the
buildings, getting lower each time. Once a level is cleared, a new, more
difficult one starts with taller buildings and a faster plane.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/bomber.png">
	<img src="/announcements/4/4.2.0/bomber_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Avoid crashing your aircraft into buildings in Bomber</em>
</div>
<br/>

<p></p>
Kapman is the very entertaining KDE version of the famous Pac-Man game. Kapman was
created by a group of French students as part of their courses in Computer Science
at the IUP ISI - University of Toulouse.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/kapman.png">
	<img src="/announcements/4/4.2.0/kapman.png" class="img-fluid">
	</a> <br/>
	<em>Kapman clones the retro game Pac-Man</em>
</div>
<br/>

<p><a href="http://games.kde.org/game.php?game=kdiamond">KDiamond</a> is a single
player puzzle game. The goal is to build lines of three similar gems by moving
them with the mouse. Once you've got three in a row, they disappear and new
jewels will appear on the board. You can now move with drag and drop, and turn
off the game timer. The sound effects are also new.</p>

<div class="text-center">
	<a href="/announcements/4/4.2.0/kdiamond.png">
	<img src="/announcements/4/4.2.0/kdiamond.png" class="img-fluid">
	</a> <br/>
	<em>KDiamond is a clone of the famous games "Bejeweled"</em>
</div>
<br/>

<p><a href="http://games.kde.org/game.php?game=ksirk">Ksirk</a> is a multi-player
game of world domination. You can place armies all over the world, and use them
to conquer your enemies. The new KSirk features improved graphics and network
play. You can use Jabber to find an online game. There is a new skin editor
which allows you to create new worlds to fight on. Using KDE-apps.org, you can
share these with the world and give others the opportunity to download your
creations using the KSirk build-in GNX installer.</p>

<p>In addition, KGoldRunner has seen the addition of a few new levels and themes,
and KBlocks joins the ranks of KDE games with sound support.</p>

<p>More information about these and other games can be found on the <a
href="http://games.kde.org">KDE Games website</a></p>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../applications">
                <img src="/announcements/4/4.2.0/images/applications-32.png" />
                Previous page: Basic applications
                </a>        
        </td>
        <td align="right" width="50%">
                <a href="../platform">Next page: Development Platform
                <img src="/announcements/4/4.2.0/images/platform-32.png" /></a>
        </td>
    </tr>
</table>