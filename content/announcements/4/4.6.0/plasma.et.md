---
title: Plasma töötsoonid annavad juhtohjad kasutaja kätte
date: "2011-01-26"
hidden: true
---

<h2>KDE laskis välja töötsoonid 4.6.0</h2>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w10.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w10.png" class="img-fluid" alt="Plasma töölaud 4.6.0-s">
	</a> <br/>
	<em>Plasma töölaud 4.6.0-s</em>
</div>
<br/>


<p>
 KDE-l on rõõm teatada Plasma töölaua ja Plasma Netbooki uuest väljalaskest. Plasma töötsoonid on saanud nii seniste funktsioonide korralikku lihvi kui ka mitmeid olulisi uuendusi, mis süvendavad veelgi Plasma suunda semantilisema ja ülesandepõhisema töökorralduse poole.
</p>
<p>
<b>Tegevuste</b> süsteem on ümber kujundatud, et seda oleks lihtsam kasutada. Paremklõpsuga akna tiitliribale saab nüüd muuta rakendused ja failid vajaliku tegevuse osaks. Tegevust valides näitab Plasma töötsoon just seda, mida kasutaja seal näha tahab. Tegevuste lisamine, eemaldamine ja nende nime muutmine on samuti senisest mugavam.</li>
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w01.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w01.png" class="img-fluid" alt="Rakendusi on hõlpus tegevustega seostada">
	</a> <br/>
	<em>Rakendusi on hõlpus tegevustega seostada</em>
</div>
<br/>

<p>
<b>Toitehaldus</b> on põhjalikult ümber töötatud, selle kood on nüüd kümme korda väiksem ja väga modulaarne, mis muudab hooldamise lihtsamaks ning võimaldab kergesti kirjutada pluginaid toitehaldusele uute võimaluste lisamiseks. Toitehalduse seadistustedialoog on nüüd paremini mõistetav ja kasutatav. Uus "reegliagent" võimaldab mitmesuguseid monitoriga seotud toiminguid, näiteks märguandeid või ekraani väljalülitamist filmi vaatamise ajal.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w02.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w02.png" class="img-fluid" alt="Uus lihtsam toitehalduse liides">
	</a> <br/>
	<em>Uus lihtsam toitehalduse liides</em>
</div>
<br/>


Aknahaldur <b>KWin</b> on varasemast oluliselt kiirem ja töötab paremini senisest rohkemate graafikadraiveritega tänu olulisele optimeerimisele, mis puhverdab akende seadistused ja värskendab ainult neid ekraani osi, mida on vaja värskendada.Sellega kaasneb:
<ul>
    <li>märkimisväärselt <a href="http://blog.martin-graesslin.com/blog/2010/10/optimization-in-kwin-4-6/">paranenud jõudlus</a>.</li>
    <li>graafikadraiverite omaduste parem tuvastamine.</li>
    <li>aktiivsete akende efekt – aknaid saab nüüd sulgeda otse ülevaatest.</li>
</ul>

<p>
Muud täiustused:
<ul>
    <li>rakenduste käivitamisaegsete märguannete tõhusam käitlemine.</li>
    <li>ümbertöötatud märguannete süsteem. Nüüd võib lasta märguannete hüpikakendel ilmuda just selles ekraaniosas, kus kasutaja seda soovib - piisab vaid, kui märguanne vajalikku kohta lohistada. Märguannete ajaloo ja tööde hüpikdialoogi välimus on klaarim ning allalaadimiste korral on võimalik edenemismärguannet laiendades näha allalaadimise kiirust ka graafiliselt.</li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w04.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w04.png" class="img-fluid" alt="Märguandeid võib nüüd lohistada ükspuha kuhu töölaual">
	</a> <br/>
	<em>Märguandeid võib nüüd lohistada ükspuha kuhu töölaual</em>
</div>
<br/>

<p>
<ul>
    <li><a href="http://i158.photobucket.com/albums/t120/wdawn/sjelf-1.png">sahtlividin</a> - paneelil olles omandab automaatselt vajaliku suuruse ning näitab lugemata kirjade arvu ja võrgus olevaid kontakte.</li>
    <li>paneeli <b>digitaalkell</b> on oluliselt ümbertöötatud ning näitab nüüd varjutatud teksti, mis mitme tausta ja teema korral on tunduvalt loetavam.</li>
    <li><b>tegumiriba</b> toetab nüüd tähtsamaid käivitajaid: lohistage vaid programm tegumiribale ja sellest saab kiirkäivituskirje või ka võimalus panna rakendus tööle otse tegumiribal, milleks tuleb sellel teha paremklõps ja valida vastav valik menüüst.</li>
    <li><b>Plasma Netbooki</b> on jätkuvalt optimeeritud, nii et "otsimine ja käivitamine" peaks olema tunduvalt kiirem, samuti peaks ajalehetegevuse "esimene lehekülg" olema palju mugavam kasutada ka puuteekraaniga seadmetes.</li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w07.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w07.png" class="img-fluid" alt="Plasma Netbooki otsing ja käivitamine on kiirem">
	</a> <br/>
	<em>Plasma Netbooki otsing ja käivitamine on kiirem</em>
</div>
<br/>

<p>
Plasma töötsoonid on loetletule lisaks saanud veel palju täiustusi, mille hulgas võib ära mainida muudatused Oxygeni ikooniteemas ning KDE platvormi väliselt loodud rakenduste parema lõimimise tänu Oxygeni GTK teemale.

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w06.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w06.png" class="img-fluid" alt="Uus Oxygeni GTK teema aitab sujuvalt ühendada KDE-välised rakendused Plasma töötsooni">
	</a> <br/>
	<em>Uus Oxygeni GTK teema aitab sujuvalt ühendada KDE-välised rakendused Plasma töötsooni</em>
</div>
<br/>
</p>

<h4>Plasma paigaldamine</h4>

<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral, operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/4.6.0/">download.kde.org</a>, samuti
<a href="/download">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.6.0 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.6.0/">download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4/4.6.0">4.6infoleheküljel</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Täieliku 4.6.0 lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/4.6.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.6.0 kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/4/4.6.0#binary">4.6.0infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.2. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
Graafikadraiverid võivad teatud olukorras komposiitvõimaluste kasutamisel eelistada OpenGL-ile hoopis XRenderit. Kui täheldate tõsiseid graafikaprobleeme, võib usutavasti aidata töölauaefektide väljalülitamine, sõltuvalt küll konkreetsest graafikadraiverist ja selle seadistustest. KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes. 
</p>

<h4>
Autorid
</h4>
<p>
Käesoleva väljalasketeate koostasid Vivek Prakash, Stefan Majewsky, Guillaume De Bure, Nikhil Marathe, Markus Slopianka, Stuart Jarvis, Jos Poortvliet, Nuno Pinheiro, Carl Symons, Marco Martin, Sebastian Kügler, Nick P ja veel paljud KDE propageerimise meeskonna liikmed. Eesti keelde tõlkis väljalasketeate Marek Laane.
</p>





<h2>Täna ilmusid veel:</h2>

<h3>
<a href="../applications">
KDE Dolphinile lisandus täpsustav otsing
</a>
</h3>

<p>

<a href="../applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="KDE rakendused 4.6.0"/>
</a>
</a>
Paljude <b>KDE rakenduste</b> meeskonnad lasksid samuti välja uued versioonid. Eriti tasub tähele panna KDE virtuaalse gloobuse Marble tunduvalt parandatud teekondade väljaarvutamise ja näitamise võimalusi ning KDE failihalduri Dolphin täiustatud filtreerimist ja otsimist metaandmete abil, niinimetatud fassettsirvimist. KDE mängud said hulganisti parandusi ning pildinäitaja Gwenview ja ekraanipiltide tegemise rakendus KSnapshot võimaluse saata pilte aega viitmata mitmesse levinud sotsiaalvõrgustikku. Täpsemalt kõneleb kõigest <a href="../applications">KDE rakenduste 4.6 teadaanne</a>.<br /><br />
</p>

<h3>
<a href="../platform">
Suund mobiilsusele kahandab KDE platvormi kaalu
</a>
</h3>

<p>

<a href="../platform">
<img src="/announcements/4/4.6.0/images/platform.png" class="app-icon float-left m-3" alt="KDE arendusplatvorm 4.6.0"/>
</a>

<b>KDE platvorm</b>, millele tuginevad Plasma töötsoonid ja KDE rakendused, sai uusi omadusi, millest võidavad kõik KDE rakendused. Uus suund mobiilsusele muudab hõlpsamaks mobiilsetele seadmetele mõeldud rakenduste arendamise. Plasma raamistik toetab nüüd töölauavidinate kirjutamist Qt deklaratiivses programmeerimiskeeles QML ning pakub uusi JavaScripti liideseid andmete kasutamiseks. KDE rakendustele metaandmete ja otsinguvõimalusi pakkuv Nepomuki tehnoloogia sai graafilise liidese, mille abil andmeid varundada ja taastada. Iganenud HAL-i asemel on nüüd võimalik kasutada UPowerit, UDevi ja UDisksi. Paranes Bluetoothi toetus. Oxygeni vidina- ja stiilikomplekt sai mitmeid täiustusi ning uus GTK rakendustele mõeldud Oxygeni teema võimaldab neid sujuvalt ühendada Plasma töötsoonidega, nii et nad näevad välja nagu KDE rakendused. Täpsemalt kõneleb kõigest <a href="../platform">KDE platvormi 4.6 teadaanne</a>.

</p>
